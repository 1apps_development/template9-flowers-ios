//
//  AIGlobals.swift
//  Swift3CodeStructure
//
//  Created by Vrushik on 25/11/2016.
//  Copyright © 2016 Vrushik. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import Photos

//MARK: - GENERAL
let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

//MARK: - MANAGERS
let ServiceManager = AIServiceManager.sharedManager
let iPhoneStoryBoard = UIStoryboard(name: "Main", bundle: nil)
let ipad_storyboard = UIStoryboard(name: "StoryboardiPad", bundle: nil)


//let UserManager = AIUser.sharedManager
//let CurrentUserAuthID = Auth.auth().currentUser

//MARK: - APP SPECIFIC
let APP_NAME = "garden"
let App_device_type = "ios"
let APP_THEME_ID = "garden"
let IS_DEMO_MODE = false


//New Modification FONT
//Font Names

/**FiraSans-Regular
 FiraSans-Medium
 FiraSans-SemiBold
 */
/*
 --Font Name: ProximaNova-Regular
 --Font Name: ProximaNova-Semibold
 --Font Name: ProximaNova-Bold
 --Font Name: ProximaNova-Extrabld
 --Font Name: ProximaNova-Black
 Family Name: Proxima Nova Alt
 --Font Name: ProximaNovaA-Light

 */
let FONT_ProximaNova_Bold = "ProximaNova-Bold"
let FONT_ProximaNova_ExtraBold = "ProximaNova-ExtraBold"
let FONT_ProximaNova_Black = "ProximaNova-Black"
let FONT_ProximaNova_alt_Light = "ProximaNovaAlt-Light"
let FONT_ProximaNova_Regular = "ProximaNova-Regular"
let FONT_ProximaNova_Light = "ProximaNovaA-Light"
let FONT_ProximaNova_Semibold = "ProximaNova-Semibold"

let FONT_Outfit_Black = "Outfit-Black"
let FONT_Outfit_Bold = "Outfit-Bold"
let FONT_Outfit_ExtraBold = "Outfit-ExtraBold"
let FONT_Outfit_ExtraLight = "Outfit-ExtraLight"
let FONT_Outfit_Light = "Outfit-Light"
let FONT_Outfit_Medium = "Outfit-Medium"
let FONT_Outfit_Regular = "Outfit-Regular"
let FONT_Outfit_Semibold = "Outfit-Semibold"
let FONT_Outfit_Thin = "Outfit-Thin"




//MARK: - Help

let HELP_URL = "http://www.google.com"

let PRIVACY_POLICY_URL = "http://www.google.com"
let TERMS_CONDITION_POLICY_URL = "http://www.google.com"

//MARK: - KEYS
let AIUSER_CURRENTUSER_STORE = "AIUSER_CURRENTUSER_STORE"
let WITHOUT_LOGIN = "KEY_IS_WITHOUT_LOGIN"

//Langauge
let LOCALIZE_ENGLISH    = "en"
let LOCALIZE_ARBIC      = "ar"
let LOCALIZE_JAPANESE   = "ja"
let LOCALIZE_HINDI      = "hi"


//MARK: - IMAGE
func ImageNamed(_ name:String) -> UIImage?{
    return UIImage(named: name)
}


var Normal_font_size: CGFloat = 14.0
var Textfiled_font_size: CGFloat = 17.0
var AppButton_font_size: CGFloat = 17.0


// MARK: - KEYS FOR USERDEFAULTS
struct userDefaultsKeys{
    static let KEY_APP_TOKEN: String = "userRegistrationToken"
    static let KEY_APP_FCM_TOKEN: String = "fcmToken"
    static let KEY_USER_REG_DATA : String = "userRegistrationData"
    static let KEY_MY_ACCOUNT_DATA : String = "myAccountData"
    static let KEY_REMEMBER_ME: String = "KEY_REMEMBER_ME"
    static let KEY_USERNAME: String = "usernameOrEmail"
    static let KEY_PASSWORD: String = "password"
    static let KEY_SAVED_CART: String = "cartArrSaved"
    static let KEY_COUPON_APPLIED: String = "CouponApplied"
    static let KEY_IS_USER_LOGGED_IN: String = "KEY_IS_USER_LOGGED_IN"
    static let KEY_CURRENCY: String = "currency"
    static let KEY_CURRENCY_NAME: String = "currencyName"
    static let KEY_TERMS: String = "terms"
    static let KEY_CONTECTUS: String = "contectUS"
    static let KEY_RETURNPOLICY: String = "returnPolicy"
    static let KEY_YOUTUVBE: String = "youtube"
    static let KEY_INSTA: String = "Instagram"
    static let KEY_MASSANGER: String = "massanger"
    static let KEY_TWITTER: String = "twitter"
    static let KEY_GESTUSEROBJ: String = "Gestuserobject"
    static let KEY_GUESTQTY: String = "KEY_QTY"
    static let KEY_GESTUSERPRODUCTARRAY: String = "KEY_GESTUSERPRODUCTARRAY"
    static let KEY_GESTTEXTARRAY: String = "textArray"
    static let KEY_USERID: String = "userid"
    static let KEY_PRODUCTID: String = "Productid"
    static let KEY_COVERIMAGE: String = "Coverimage"
    static let KEY_PRODUCTNAME: String = "PRODUCTNAME"
    static let KEY_ORIGNALPRICE: String = "ORIGNALPRICE"
    static let KEY_DISCOUNTPRICE: String = "DISCOUNTPRICE"
    static let KEY_FINALPRICE: String = "FINALPRICE"
    static let KEY_QTY: String = "QTY"
    static let KEY_VARIENTID: String = "VARIENTID"
    static let KEY_VARIENTNAME: String = "VARIENTNAME"
    static let KEY_GUESTSUBTOTAL: String = "keygestsubtotal"
    static let KEY_GUESTFINALTOTAL: String = "keuguestFinalTotal"
    static let KEY_FIRSTNAME: String = "firstname"
    static let KEY_LASTNAME: String = "lastname"
    static let KEY_EMAIL: String = "keyemail"
    static let KEY_PHONE: String = "phonenumber"
    static let KEY_FULLNAME: String = "keyfullname"
    static let KEY_SELECTEDCOUNTRY: String = "selectedCountry"
    static let KEY_SELECTEDSTATE: String = "selectedSate"
    static let KEY_SELECTEDCITY: String = "selectedCity"
    static let KEY_SELECTEDCOUNTRYDELIVARY: String = "selectedCountryDELIVARY"
    static let KEY_SELECTEDSTATEDELIVARY: String = "selectedSateDELIVARY"
    static let KEY_BILLINGOBJ: String = "Billingobject"
    static let KEY_DELIVARYID: String = "keydelivaryid"
    static let KEY_DELIVARYIMAGE: String = "keydelivaryimage"
    static let KEY_DELIVARYDESCRIPTION: String = "keydelivarydescripation"
    static let KEY_PAYMENTTYPE: String = "keypaymenType"
    static let KEY_PAYMENTIMAGETYPE: String = "keypaymentImageType"
    static let KEY_PAYMENTDESCRIPTION: String = "keyPaymentdescripation"
    static let KEY_CUPENOBJECT: String = "keycupenobject"
    static let KEY_USERPROFILE: String = "keyuserprofile"
    static let KEY_TOTALPRICE: String = "KEY_TOTALPRICE"

}

//MARK: - COLORS

public func RGBCOLOR(_ r: CGFloat, g: CGFloat , b: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
}

public func RGBCOLOR(_ r: CGFloat, g: CGFloat , b: CGFloat, alpha: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alpha)
}

let APP_PRIMARY_GREEN_COLOR = hexStringToUIColor(hex: "#5F766A")
let APP_PRIMARY_DARK_TEXT_ICON_COLOR = hexStringToUIColor(hex: "#162C4E")
let APP_PRIMARY_DEEP_BLUE_COLOR = hexStringToUIColor(hex: "#172C4E")
let APP_YELLO_COLOR = hexStringToUIColor(hex: "#FFC700")
let APP_HEADER_TEXT_COLOR = hexStringToUIColor(hex: "#172C4E")
let APP_PRIMARY_ORANGE_COLOR = hexStringToUIColor(hex: "#FF754C")
let APP_SHADOWVIEW_BORDER_COLOR = hexStringToUIColor(hex: "#DCE2ED")
let APP_GREY_TEXT_COLOR = hexStringToUIColor(hex: "#717887")

struct DeviceSize {
    static let width:CGFloat = UIScreen.main.bounds.width
    static let height:CGFloat = UIScreen.main.bounds.height
}


//MARK: - SCREEN SIZE

let NAVIGATION_BAR_HEIGHT:CGFloat = 64
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH = UIScreen.main.bounds.size.width

func GET_PROPORTIONAL_WIDTH (_ width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/375)
}
func GET_PROPORTIONAL_HEIGHT (_ height:CGFloat) -> CGFloat {
    return ((SCREEN_HEIGHT * height)/667)
}

func GET_PROPORTIONAL_WIDTH_CELL (_ width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/375)
}
func GET_PROPORTIONAL_HEIGHT_CELL (_ height:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * height)/667)
}


// MARK: - USERDEFAULTS


struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    
    static let IS_IPHONE_X_SERISE   = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    
}


func View_to_image(with view: UIView) -> UIImage? {
    view.isOpaque = false
       UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
       defer { UIGraphicsEndImageContext() }
       if let context = UIGraphicsGetCurrentContext() {
           view.layer.render(in: context)
           let image = UIGraphicsGetImageFromCurrentImageContext()
           return image
       }
       return nil
   }

func tempURL_Video() -> URL? {
       let directory = NSTemporaryDirectory() as NSString
       
       let tempPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
       let tempDocumentsDirectory = tempPath[0] as NSString
       
       let uniqueVideoID = "\(randomString(length: 5)).mp4"
       //        let tempDataPath = tempDocumentsDirectory.appendingPathComponent(uniqueVideoID)
       
       if directory != "" {
           let path = tempDocumentsDirectory.appendingPathComponent(uniqueVideoID)
           return URL(fileURLWithPath: path)
       }
       
       return nil
   }
   func randomString(length: Int) -> String {
       let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
       return String((0..<length).map{ _ in letters.randomElement()! })
   }
   

func getStringFromDictionary(_ dict:AnyObject) -> String{
    var strJson = ""
    do {
        let data = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        strJson = String(data: data, encoding: String.Encoding.utf8)!
    } catch let error as NSError {
        print("json error: \(error.localizedDescription)")
    }
    
    return strJson
}

func Save_image_to_document(img : UIImage) -> String {
      

    let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    // create a name for your image
        let fileName = "\(randomString(length: 5)).png" // name of the image to be saved
    let fileURL = documentsDirectoryURL.appendingPathComponent(fileName)

    if !FileManager.default.fileExists(atPath: fileURL.path) {
        do {
            try img.pngData()!.write(to: fileURL)
            return fileURL.path
            } catch {
                print(error)
                   return ""
            }
        } else {
            print("Image Not Added")
           return ""
    }
     
  }
  
func getMediaDuration(url: URL!) -> Float{
     let asset : AVURLAsset = AVURLAsset(url: url) as AVURLAsset
     let duration : CMTime = asset.duration
     return Float(CMTimeGetSeconds(duration))
 }

func Save_File_Photos(_ url : URL ){
      PHPhotoLibrary.shared().performChanges({
          PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
      }) { saved, error in
          if saved {
             print("Your video was successfully saved")
            
          }else{
              print("Video Save Error:-\(error.debugDescription)")
          }
      }
  }
func getURLPathForFilterVideo() -> URL? {
    let tool = "Filter"
    let directory = NSTemporaryDirectory() as NSString
    let tempPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    let tempDocumentsDirectory = tempPath[0] as NSString
    let uniqueVideoID = "Filtereddvideo.mov"
    //        let tempDataPath = tempDocumentsDirectory.appendingPathComponent(uniqueVideoID)
    if directory != "" {
        let path = tempDocumentsDirectory.appendingPathComponent(uniqueVideoID)
        if tool != "Rotated" {
            CheckAndRemoveFileAtPath(path: path)
        }
        return URL(fileURLWithPath: path)
    }
    return nil
}
func getURLPathFor(tool:String) -> URL? {
    let directory = NSTemporaryDirectory() as NSString
    let tempPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    let tempDocumentsDirectory = tempPath[0] as NSString
    let uniqueVideoID = "\(tool)video.mp4"
    //        let tempDataPath = tempDocumentsDirectory.appendingPathComponent(uniqueVideoID)
    if directory != "" {
        let path = tempDocumentsDirectory.appendingPathComponent(uniqueVideoID)
        if tool != "Rotated" {
            CheckAndRemoveFileAtPath(path: path)
        }
        return URL(fileURLWithPath: path)
    }
    return nil
}
func CheckAndRemoveFileAtPath(path:String){
    print("Path:\(path)")
    do{
        if FileManager.default.fileExists(atPath: path) {
            try FileManager.default.removeItem(atPath: path)
        }
    }catch let e{
        print("removePath Error:\(e.localizedDescription)")
    }
}
func remove_file(itemName:String, fileExtension: String) {
    let fileManager = FileManager.default
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    guard let dirPath = paths.first else {
        return
    }
    let filePath = "\(dirPath)/\(itemName).\(fileExtension)"
    do {
        try fileManager.removeItem(atPath: filePath)
    } catch let error as NSError {
        print(error.debugDescription)
    }}

 func resolutionForLocalVideo(url: URL) -> CGSize? {
    if FileManager.default.fileExists(atPath: url.path) {
        let alltracks = AVURLAsset(url: url).tracks
        print("alltracks:\(alltracks)")
         guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
         return CGSize(width: abs(size.width), height: abs(size.height))
    }
    return nil
}
 func makeCircleWith(size: CGSize, backgroundColor: UIColor) -> UIImage? {
          UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
          let context = UIGraphicsGetCurrentContext()
          context?.setFillColor(backgroundColor.cgColor)
          context?.setStrokeColor(UIColor.clear.cgColor)
          let bounds = CGRect(origin: .zero, size: size)
          context?.addEllipse(in: bounds)
          context?.drawPath(using: .fill)
          let image = UIGraphicsGetImageFromCurrentImageContext()
          UIGraphicsEndImageContext()
          return image
      }
func Get_Files_BasePath() -> URL {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    let docURL = URL(string: documentsDirectory)!
    let dataPath = docURL.appendingPathComponent("Taskly")
    if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
        do {
            try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error.localizedDescription);
        }
    }
    return dataPath
}

