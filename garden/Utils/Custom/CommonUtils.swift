//
//  CommonUtils.swift
//  UbeatUser
//
//  Created by MAC PC-9 on 5/22/18.
//  Copyright © 2018 MAC PC-9. All rights reserved.
//

import UIKit

class CommonUtils: NSObject {

    static func shared() -> CommonUtils
    {
        return CommonUtils()
    }
    
    func setDashboardViewController()
    {
//        let VC = HomeViewController(nibName: "HomeViewController", bundle: nil)
//        appDelegate.navigationController = UINavigationController(rootViewController: VC)
//        appDelegate.navigationController?.navigationBar.isHidden = true
//        appDelegate.window?.rootViewController = appDelegate.navigationController
//        appDelegate.window?.makeKeyAndVisible()
    }
    
    func setDashboardViewControllerViaSplash()
    {
      /*  let VC = storyBoard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        VC.index = 1
        appDelegate.navigationController = UINavigationController(rootViewController: VC)
        appDelegate.navigationController?.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = appDelegate.navigationController
        appDelegate.window?.makeKeyAndVisible()*/
    }
    
    func setIntroViewController()
    {
//        appDelegate.openLoginScreen()
        /*
        let VC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        appDelegate.navigationController = UINavigationController(rootViewController: VC)
        appDelegate.navigationController?.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = appDelegate.navigationController
        appDelegate.window?.makeKeyAndVisible()*/
    }
    
    func setIntroViewControllerViaSplash()
    {/*
        let VC = storyBoard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        VC.index = 0
        appDelegate.navigationController = UINavigationController(rootViewController: VC)
        appDelegate.navigationController?.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = appDelegate.navigationController
        appDelegate.window?.makeKeyAndVisible()*/
    }
    
    func setSignInViewController()
    {/*
        let VC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        appDelegate.navigationController = UINavigationController(rootViewController: VC)
        appDelegate.navigationController?.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = appDelegate.navigationController
        appDelegate.window?.makeKeyAndVisible()*/
    }
    
    static func getDateFromString(strDate : String!, format : String) -> Date!
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = format
        
        let date: Date! = dateFormatterGet.date(from: strDate)
//        Dlog(message: date as AnyObject)
        
        return date
    }
    
    static func secondBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let difference = Calendar.current.dateComponents([.second], from: endDate, to: startDate)
        return difference.second!
    }
    
    static func getStringFromDate(date : Date!, format : String!) -> String!
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = format!
        
//        Dlog(message: dateFormatterGet.string(from: date) as AnyObject)
        
        return dateFormatterGet.string(from: date)
    }
    
    static func getCurrentStringFromUTCString(strDate : String!, oldFormat : String!, newFormat : String!) -> String!
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = oldFormat!
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: strDate!)!
        dateFormatter.dateFormat = newFormat!
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date)
        
    }
    
    static func getUTCDateFromUTCString(strDate : String!, format : String!) -> Date!
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format!
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: strDate!)!
        
    }
    
    static func getCurrentDateFromUTCString(strDate : String!, format : String!) -> Date!
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format!
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.date(from: strDate!)!
        
    }
    
    func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
    
    static func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    static func toJSONString(dic : [String : AnyObject]!,options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = dic,
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
    
    static func convertToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func arrayToJSONString(dic : [[String : AnyObject]]!,options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = dic,
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
    
    static func convertToArray(text: String) -> [[String: AnyObject]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    static func windowErrorAlert(title : String!, message:String!, buttonTitle : String!){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIViewController()
        let okAction = UIAlertAction(title: buttonTitle, style: .default) { (action) -> Void in
            alert.dismiss(animated: true, completion: nil)
            window.resignKey()
            window.isHidden = true
            window.removeFromSuperview()
            window.windowLevel = UIWindow.Level.alert - 1
            window.setNeedsLayout()
        }
        
        alert.addAction(okAction)
        window.windowLevel = UIWindow.Level.alert + 1
        window.makeKeyAndVisible()
        
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    static func setImageColor(img : UIImageView!, color : UIColor!)
    {
        img.image = img.image!.withRenderingMode(.alwaysTemplate)
        img.tintColor = color
    }
    
    static func setImageColorWithCollection(imgCollection : [UIImageView]!, color : UIColor!)
    {
        for img in imgCollection {
            img.image = img.image!.withRenderingMode(.alwaysTemplate)
            img.tintColor = color
        }
    }
    
    static func getUrlAppendingDocumentDirectory(filename : String) -> URL
    {
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        return URL(fileURLWithPath: "\(documentsDirectory)\(filename)")
    }
    
    static func getPathComponent(url : URL!, noOfcomponent : Int!) -> String!
    {
        var strPath = ""
        
        for i in 1 ..< noOfcomponent + 1
        {
            strPath = "/\(url.pathComponents[url.pathComponents.count-i])" + strPath
        }
        
        return strPath
    }
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}

extension Data {
    
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}
