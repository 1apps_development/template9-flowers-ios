//
//  AIServiceConstants.swift
//
//  Created by Vrushik on 08/02/2021.
//  Copyright © 2021 Vrushik. All rights reserved.
//
import Foundation

//MARK:- BASE URL

//Live URL
//Live URL
var URL_BASE                              = "https://apps.rajodiya.com/ecommercego-mobileapp/api/garden"
var URL_BASEIMAGE                         = "https://apps.rajodiya.com/ecommerce"
var URL_PaymentImage                      = "payment_url"

//Test URL
//let URL_BASE                            = "http://192.168.0.200:4932/"
//let URL_Local_Server                    = "http://192.168.0.114:8000/"

let URL_Signup                              = getFullUrl("/register")
let URL_Login                               = getFullUrl("/login")
let URL_SendOTP                             = getFullUrl("/fargot-password-send-otp")
let URL_VerifyOTP                           = getFullUrl("/fargot-password-verify-otp")
let URL_PasswordSAVE                        = getFullUrl("/fargot-password-save")
let URL_LandingPage                         = getFullUrl("/landingpage")
let URL_HomeCategories                      = getFullUrl("/home-categoty?page=")
let URL_ProductSearch                       = getFullUrl("/categorys-product?page=1")
let URL_Currency                            = getFullUrl("/currency")
let URL_CategoryProduct                    = getFullUrl("/categorys-product?page=")
let URL_CategoryProductGuest              = getFullUrl("/categorys-product-guest?page=")
let URL_ProductDetail                   = getFullUrl("/product-detail")
let URL_ProductDetailsGuest             = getFullUrl("/product-detail-guest")
let URL_SideMenu                        = getFullUrl("/navigation")
let URL_Bestseller                      = getFullUrl("/bestseller?page=")
let URL_Bestseller_guest                = getFullUrl("/bestseller-guest?page=")
let URL_TrendingProducts                = getFullUrl("/tranding-category-product?page=")
let URL_TrendingProductsGuest          = getFullUrl("/tranding-category-product-guest?page=")
let URL_Addtocart                        = getFullUrl("/addtocart")
let URL_Extra                            = getFullUrl("/extra-url")
let URL_Wishlist                         = getFullUrl("/wishlist")
let URL_cartQty                          = getFullUrl("/cart-qty")
let URL_ProductRateing                   = getFullUrl("/product-rating")
let URL_taxGuest                          = getFullUrl("/tax-guest")
let URL_VarientStock                     = getFullUrl("/check-variant-stock")
let URL_ProductsSearch                    = getFullUrl("/search?page=")
let URL_productsSearchGuest               = getFullUrl("/search-guest?page=")
let URL_cartList                          = getFullUrl("/cart-list")
let URL_CupenCode                         = getFullUrl("/apply-coupon")
let URL_Country                            = getFullUrl("/country-list")
let URL_State                              = getFullUrl("/state-list")
let URL_City                               = getFullUrl("/city-list")
let URL_Address                            = getFullUrl("/address-list")
let URL_Addaddress                         = getFullUrl("/add-address")
let URL_UpdatedAddress                      = getFullUrl("/update-address")
let URL_DelivaryList                        = getFullUrl("/delivery-list")
let URL_PaymentList                         = getFullUrl("/payment-list")
let URL_ConfimOrder                         = getFullUrl("/confirm-order")
let URL_OrderPlaced                         = getFullUrl("/place-order")
let URL_UserDetails                         = getFullUrl("/user-detail")
let URL_OrderPlacedGuest                   = getFullUrl("/place-order-guest")
let URL_Wishlistes                         = getFullUrl("/wishlist-list")
let URL_Logout                             = getFullUrl("/logout")
let URL_Updateuserimage                   = getFullUrl("/update-user-image")
let URL_ProfileUpdate                     = getFullUrl("/profile-update")
let URL_DeleteAccount                       = getFullUrl("/user-delete")
let URL_ChangePassword                    = getFullUrl("/change-password")
let URL_OrderHistory                      = getFullUrl("/order-list?page=")
let URL_OrderDetails                       = getFullUrl("/order-detail")
let URL_StatusChange                       = getFullUrl("/order-status-change")
let URL_deleteAddress                      = getFullUrl("/delete-address")
let URL_ReturnHistory                      = getFullUrl("/return-order-list?page=")
let URL_LoyalityProgram                    = getFullUrl("/loyality-program-json")
let URL_LoyalityRewords                    = getFullUrl("/loyality-reward")
let URL_CartCheak                           = getFullUrl("/cart-check")
let URL_CATEGORYLIST                        = getFullUrl("/category-list")
let URL_notifyUser                          = getFullUrl("/notify_user")
//MARK:- FULL URL

func getFullUrl(_ urlEndPoint : String) -> String {
    return URL_BASE + urlEndPoint
}

func getImageFullURL(_ urlendpoint: String) -> String {
    return URL_BASEIMAGE + urlendpoint
}

extension String
{

static let str_please_enable_camera_permission_in_setting = "Please enable camera Permission in Settings > Taskly > enable camera permission"
    static let str_settings = "Settings"
}
