//
//  ChangePasswordVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class ChangePasswordVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var text_password: UITextField!
    @IBOutlet weak var text_confimPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickSaveChanges(_ sender: UIButton) {
        if IS_DEMO_MODE == true {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
        }else{
            if self.text_password.text == ""{
                showAlertMessage(titleStr: "", messageStr: "Please enter password.")
            }else if self.text_confimPassword.text! == "" {
                showAlertMessage(titleStr: "", messageStr: "Please enter confim password")
            }else if self.text_password.text! != self.text_confimPassword.text! {
                showAlertMessage(titleStr: "", messageStr: "Password & confirm password must be same.")
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "password": self.text_password.text!,
                                           "theme_id": APP_THEME_ID]
                getupdatedPassword(param)
            }
        }
    }
    
    //MARK: - APIfunction
    func getupdatedPassword(_ param: [String:Any]) {
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_ChangePassword, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        print(massage!)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
}
