//
//  BillingAddressVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit
import iOSDropDown
import Alamofire

class BillingAddressVC: UIViewController {
    
    @IBOutlet weak var text_fname: UITextField!
    @IBOutlet weak var text_lname: UITextField!
    @IBOutlet weak var text_email: UITextField!
    @IBOutlet weak var text_telephone: UITextField!
    @IBOutlet weak var btn_Cheak: UIButton!
    
    @IBOutlet weak var adderssListTV: UITableView!
    @IBOutlet weak var heightCVadderssList: NSLayoutConstraint!
    
    @IBOutlet weak var DelivaryView: UIView!
    @IBOutlet weak var heightDelivaryView: NSLayoutConstraint!
    
    @IBOutlet weak var text_company: UITextField!
    @IBOutlet weak var text_address: UITextField!
    @IBOutlet weak var text_city: DropDown!
    @IBOutlet weak var text_postCode: UITextField!
    @IBOutlet weak var text_state: DropDown!
    @IBOutlet weak var text_Country: DropDown!

    //MARK: - Variables
    
    var pageIndex = 1
    var lastIndex = 0
    var selectedindex = 0
    var selectedCountryid = Int()
    var selectedStateid = Int()
    var selectedCityid = String()
    var ArrayAddressList: [AddressLists] = []
    var ArrayCountry: [CountryLists] = []
    var ArraySatate: [StateLists] = []
    var ArrayCity: [CityLists] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DelivaryView.isHidden = true
        self.heightDelivaryView.constant = 0.0
        text_postCode.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.text_fname.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_FIRSTNAME)
        self.text_lname.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_LASTNAME)
        self.text_email.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_EMAIL)
        self.text_telephone.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PHONE)
        
        self.btn_Cheak.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        self.adderssListTV.delegate = self
        self.adderssListTV.dataSource = self
        self.adderssListTV.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        self.heightCVadderssList.constant = 0.0
        self.adderssListTV.estimatedRowHeight = UITableView.automaticDimension
        self.heightCVadderssList.constant = UITableView.automaticDimension
        let addressparam: [String:Any] = ["user_id": getID(),
                                          "theme_id": APP_THEME_ID
        ]
        getAddressList(addressparam)
        self.adderssListTV.reloadData()
        
    }
    
    //MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddAddress(_ sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickContinues(_ sender: UIButton){
        if self.text_fname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter first name.")
        }
        else if self.text_lname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter last name.")
        }
        if self.text_email.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the email address.")
        }
        else if self.text_telephone.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter phone number.")
        }
//        else if isValidEmail(self.txt_email.text!)
//        {
//            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
//        }
        else{
            if self.ArrayAddressList.count != 0 {
                let data = self.ArrayAddressList[selectedindex]
                print(data)
                if self.btn_Cheak.imageView?.image == UIImage(named: "checkbox_selected"){
                    let data = self.ArrayAddressList[selectedindex]
                    let billingObject = ["firstname":self.text_fname.text!,"lastname":self.text_lname.text!,"email":self.text_email.text!,"billing_user_telephone":self.text_telephone.text!,"billing_address":data.address!,"billing_postecode":data.postcode!,"billing_country":data.countryid!,"billing_state":data.stateid!,"billing_city":data.cityname!,"delivery_address":data.address!,"delivery_postcode":data.postcode!,"delivery_country":data.countryid!,"delivery_state":data.stateid!,"delivery_city":data.cityname!] as [String : Any]
                    
                    UserDefaults.standard.set(billingObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let adddata = self.ArrayAddressList[selectedindex]
                    if self.text_address.text! == ""
                    {
                        showAlertMessage(titleStr: "", messageStr: "ENTER_ADDRESS_MESAAGE")
                    }
                    else if self.text_Country.text == ""
                    {
                        showAlertMessage(titleStr: "", messageStr: "ENTER_COUNTRY_MESAAGE")
                    }
                    else if self.text_state.text == ""
                    {
                        showAlertMessage(titleStr: "", messageStr: "ENTER_STATE_MESAAGE")
                    }
                    else if self.text_city.text == ""
                    {
                        showAlertMessage(titleStr: "", messageStr: "ENTER_CITY_MESAAGE")
                    }
                    else if self.text_postCode.text! == ""
                    {
                        showAlertMessage(titleStr: "", messageStr: "ENTER_POSTCODE_MESAAGE")
                    }else{
                        let data = self.ArrayAddressList[selectedindex]
                        let billingObject = ["firstname":self.text_fname.text!,"lastname":self.text_lname.text!,"email":self.text_email.text!,"billing_user_telephone":self.text_telephone.text!,"billing_address":adddata.address!,"billing_postecode":data.postcode!,"billing_country":adddata.countryid!,"billing_state":adddata.stateid!,"billing_city":adddata.city!,"delivery_address":self.text_address.text!,"delivery_postcode":Int(self.text_postCode.text!)!,"delivery_country":self.selectedCountryid,"delivery_state":self.selectedStateid,"delivery_city":self.text_city.text!] as [String : Any]
                        UserDefaults.standard.set(billingObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    @IBAction func onClickContry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.text_Country.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_Country.checkMarkEnabled = false
        self.text_Country.optionArray = countryName
        self.text_Country.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_Country.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_Country.didSelect(completion: { selected, index, id in
            self.text_Country.text = selected
            self.selectedCountryid = self.ArrayCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedCountryid,
                                       "theme_id": APP_THEME_ID]
            self.getState(param)
               })
    }
    @IBAction func onClickState(_ sender: DropDown){
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.text_state.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_state.checkMarkEnabled = false
        self.text_state.optionArray = statename
        self.text_state.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_state.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_state.didSelect(completion: { selected, index, id in
            self.text_state.text = selected
            self.selectedStateid = self.ArraySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedStateid,
                                       "theme_id": APP_THEME_ID]
            self.getCity(param)
               })
    }
    @IBAction func onClickCity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.text_city.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_city.checkMarkEnabled = false
        self.text_city.optionArray = cityname
        self.text_city.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_city.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_city.didSelect(completion: { selected, index, id in
            self.text_city.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    @IBAction func onClickCheakbtn(_ sender: Any){
        if self.btn_Cheak.imageView?.image == UIImage.init(named: "checkbox")
        {
            self.btn_Cheak.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.DelivaryView.isHidden = true
            self.heightDelivaryView.constant = 0.0
        }
        else{
            self.btn_Cheak.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.DelivaryView.isHidden = false
            self.heightDelivaryView.constant = 500.0
        }
    }
    
    //MARK: - API Functions
    func getAddressList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_Address, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let addressdata = data["data"] as? [[String:Any]]{
                                    self.ArrayAddressList = Address.init(addressdata).addresslist
                                    self.adderssListTV.reloadData()
                                    self.heightCVadderssList.constant = CGFloat(100 * self.ArrayAddressList.count)
                                    let param: [String:Any] = ["theme_id": APP_THEME_ID]
                                    self.getCountries(param)
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    func getCountries(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Country, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                               self.ArrayCountry = Country.init(data).countrylists
                           }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    func getState(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_State, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArraySatate = State.init(data).statelists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
   func getCity(_ param: [String:Any]){
       AIServiceManager.sharedManager.callPostApi(URL_City, params: param, nil) { response in
           switch response.result{
           case let .success(result):
               HIDE_CUSTOM_LOADER()
               if let json = result as? [String: Any]{
                   if let status = json["status"] as? Int{
                       if status == 1{
                           if let data = json["data"] as? [[String:Any]]{
                               self.ArrayCity = City.init(data).citylists
                           }
                       }else if status == 9 {
                           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                           let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                           let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                           nav.navigationBar.isHidden = true
                           keyWindow?.rootViewController = nav
                       }else {
                           let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                       }
                   }
//                    print(json)
               }
           case let .failure(error):
               print(error.localizedDescription)
               HIDE_CUSTOM_LOADER()
               break
           }
       }
    }
}
extension BillingAddressVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayAddressList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
        if indexPath.row == self.selectedindex{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#B5C547")
            cell.img_selected.image = UIImage(named: "radio_checked")
        }else{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = .white
            cell.img_selected.image = UIImage(named: "radio_uncheck")
        }
        cell.configCell(ArrayAddressList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedindex = indexPath.item
        self.adderssListTV.reloadData()
    }
}
extension BillingAddressVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = text_postCode.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
