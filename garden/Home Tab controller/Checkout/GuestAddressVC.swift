//
//  GuestAddressVC.swift
//  plants
//
//  Created by mac on 05/12/22.
//

import UIKit
import iOSDropDown

class GuestAddressVC: UIViewController {

    @IBOutlet weak var text_fname: UITextField!
    @IBOutlet weak var text_lname: UITextField!
    @IBOutlet weak var text_email: UITextField!
    @IBOutlet weak var text_telephone: UITextField!
    
    @IBOutlet weak var text_billCompany: UITextField!
    @IBOutlet weak var text_billaddress: UITextField!
    @IBOutlet weak var text_billcountry: DropDown!
    @IBOutlet weak var text_billstate: DropDown!
    @IBOutlet weak var text_billCity: DropDown!
    @IBOutlet weak var text_billpostCode: UITextField!
    
    @IBOutlet weak var text_dillCompany: UITextField!
    @IBOutlet weak var text_dilladdress: UITextField!
    @IBOutlet weak var text_dillcountry: DropDown!
    @IBOutlet weak var text_dillstate: DropDown!
    @IBOutlet weak var text_dillCity: DropDown!
    @IBOutlet weak var text_dillpostCode: UITextField!
    @IBOutlet weak var btn_cheak: UIButton!
    
    @IBOutlet weak var dilivaryView: UIView!
    @IBOutlet weak var heightDelvary: NSLayoutConstraint!
    
    //MARK: - Variables
    
    var selectedCountryid = Int()
    var selectedStateid = Int()
    var selectedCityid = String()
    var ArrayCountry: [CountryLists] = []
    var ArraySatate: [StateLists] = []
    var ArrayCity: [CityLists] = []
    
    var selectedDelivaryCountryid = Int()
    var selectedDelivaryStateid = Int()
    var selectedDelivaryCityid = String()
    var ArrayDelivaryCountry: [CountryLists] = []
    var ArrayDelivarySatate: [StateLists] = []
    var ArrayDelivaryCity: [CityLists] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dilivaryView.isHidden = true
        self.heightDelvary.constant = 0.0
        self.text_billpostCode.delegate = self
        self.text_dillpostCode.delegate = self
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getCountries(param)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btn_cheak.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
    }
    
    @IBAction func onclickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickCheak(_ sender: UIButton) {
        if self.btn_cheak.imageView?.image == UIImage.init(named: "checkbox_selected")
        {
            self.btn_cheak.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.dilivaryView.isHidden = false
            self.heightDelvary.constant = 500.0
        }
        else{
            self.btn_cheak.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.dilivaryView.isHidden = true
            self.heightDelvary.constant = 0.0
        }
    }
    @IBAction func onclickBillCountry(_ sender: DropDown) {
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.text_billcountry.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_billcountry.checkMarkEnabled = false
        self.text_billcountry.optionArray = countryName
        self.text_billcountry.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_billcountry.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_billcountry.didSelect(completion: { selected, index, id in
            self.text_billcountry.text = selected
            self.selectedCountryid = self.ArrayCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedCountryid,
                                       "theme_id": APP_THEME_ID]
            self.getState(param)
               })
    }
    @IBAction func onclickBillState(_ sender: DropDown) {
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.text_billstate.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_billstate.checkMarkEnabled = false
        self.text_billstate.optionArray = statename
        self.text_billstate.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_billstate.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_billstate.didSelect(completion: { selected, index, id in
            self.text_billstate.text = selected
            self.selectedStateid = self.ArraySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedStateid,
                                       "theme_id": APP_THEME_ID]
            self.getCity(param)
               })
    }
    @IBAction func onclickBillCity(_ sender: DropDown) {
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.text_billCity.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_billCity.checkMarkEnabled = false
        self.text_billCity.optionArray = cityname
        self.text_billCity.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_billCity.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_billCity.didSelect(completion: { selected, index, id in
            self.text_billCity.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    @IBAction func onclickdillCountry(_ sender: DropDown) {
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.text_dillcountry.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_dillcountry.checkMarkEnabled = false
        self.text_dillcountry.optionArray = countryName
        self.text_dillcountry.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_dillcountry.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_dillcountry.didSelect(completion: { selected, index, id in
            self.text_dillcountry.text = selected
            self.selectedDelivaryCountryid = self.ArrayDelivaryCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedDelivaryCountryid,
                                       "theme_id": APP_THEME_ID]
            self.getState(param)
               })
    }
    @IBAction func onclickdillState(_ sender: DropDown) {
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.text_dillstate.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_dillstate.checkMarkEnabled = false
        self.text_dillstate.optionArray = statename
        self.text_dillstate.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_dillstate.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_dillstate.didSelect(completion: { selected, index, id in
            self.text_dillstate.text = selected
            self.selectedDelivaryStateid = self.ArrayDelivarySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedDelivaryStateid,
                                       "theme_id": APP_THEME_ID]
            self.getCity(param)
               })
    }
    @IBAction func onclickdillCity(_ sender: DropDown) {
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.text_dillCity.textColor = hexStringToUIColor(hex: "#E6E6E6")
        self.text_dillCity.checkMarkEnabled = false
        self.text_dillCity.optionArray = cityname
        self.text_dillCity.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
        self.text_dillCity.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
        self.text_dillCity.didSelect(completion: { selected, index, id in
            self.text_dillCity.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    
    @IBAction func onclickContinue(_ sender: UIButton) {
        if self.text_fname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter first name")
        }
        else if self.text_lname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter last name.")
        }
        if self.text_email.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the email address.")
        }
        else if self.text_telephone.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter phone number.")
        }
        else if isValidEmail(self.text_email.text!) == false
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
        }
        else if isValidEmail(self.text_email.text!) == false
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
        }
        else if self.text_billaddress.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter address.")
        }
       
        else if self.text_billcountry.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter country.")
        }
        else if self.text_billstate.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter region / state.")
        }
        else if self.text_billCity.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter city.")
        }
        else if self.text_billpostCode.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter postcode.")
        }
        else{
            if self.btn_cheak.imageView?.image == UIImage(named: "checkbox_selected"){
                UserDefaults.standard.set(self.text_billcountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)
                UserDefaults.standard.set(self.text_billstate.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATE)
                
                UserDefaults.standard.set(self.text_dillcountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)
                UserDefaults.standard.set(self.text_dillstate.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)
                
                let billingObj = ["firstname":self.text_fname.text!,"lastname":self.text_lname.text!,"email":self.text_email.text!,"billing_user_telephone":self.text_telephone.text!,"billing_address":self.text_billaddress.text!,"billing_postecode":self.text_billpostCode.text!,"billing_country":self.selectedCountryid,"billing_state":self.selectedStateid,"billing_city":self.text_billCity.text!,"delivery_address":self.text_billaddress.text!,"delivery_postcode":self.text_billpostCode.text!,"delivery_country":self.selectedCountryid,"delivery_state":self.selectedStateid,"delivery_city":self.text_billCity.text!] as [String : Any]
                UserDefaults.standard.set(billingObj, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                if self.text_dillCompany.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery address.")
                }
                else if self.text_dillcountry.text == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery country.")
                }
                else if self.text_dillstate.text == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery region / state.")
                }
                else if self.text_dillCity.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery city.")
                }
                else if self.text_dillpostCode.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery postcode.")
                }
                else{
                    UserDefaults.standard.set(self.text_billcountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)
                    UserDefaults.standard.set(self.text_billstate.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATE)
                    
                    UserDefaults.standard.set(self.text_dillcountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)
                    UserDefaults.standard.set(self.text_dillstate.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)
                    
                    let delivaryObject = ["firstname":self.text_fname.text!,"lastname":self.text_lname.text!,"email":self.text_email.text!,"billing_user_telephone":self.text_telephone.text!,"billing_address":self.text_billaddress.text!,"billing_postecode":self.text_billpostCode.text!,"billing_country":self.selectedCountryid,"billing_state":self.selectedStateid,"billing_city":self.text_billCity.text!,"delivery_address":self.text_dilladdress.text!,"delivery_postcode":self.text_dillpostCode.text!,"delivery_country":self.selectedDelivaryCountryid,"delivery_state":self.selectedDelivaryStateid,"delivery_city":self.text_dillCity.text!] as [String : Any]
                    
                    UserDefaults.standard.set(delivaryObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    //MARK: - ApiFuctions
    func getCountries(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Country, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayCountry = Country.init(data).countrylists
                                self.ArrayDelivaryCountry = Country.init(data).countrylists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    func getState(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_State, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArraySatate = State.init(data).statelists
                                self.ArrayDelivarySatate = State.init(data).statelists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
   func getCity(_ param: [String:Any]){
       AIServiceManager.sharedManager.callPostApi(URL_City, params: param, nil) { response in
           switch response.result{
           case let .success(result):
               HIDE_CUSTOM_LOADER()
               if let json = result as? [String: Any]{
                   if let status = json["status"] as? Int{
                       if status == 1{
                           if let data = json["data"] as? [[String:Any]]{
                               self.ArrayCity = City.init(data).citylists
                               self.ArrayDelivaryCity = City.init(data).citylists
                           }
                       }else if status == 9 {
                           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                           let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                           let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                           nav.navigationBar.isHidden = true
                           keyWindow?.rootViewController = nav
                       }else{
                           let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                       }
                   }
//                    print(json)
               }
           case let .failure(error):
               print(error.localizedDescription)
               HIDE_CUSTOM_LOADER()
               break
           }
       }
    }
    
}
extension GuestAddressVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        if textField == text_dillpostCode {
            let currentString: NSString = text_dillpostCode.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        let currentString: NSString = text_billpostCode.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
