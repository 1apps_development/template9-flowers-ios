//
//  CheckoutVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit
import Alamofire
import StripeCore
import Stripe

class CheckoutVC: UIViewController {
    
    @IBOutlet weak var tableShopping: UITableView!
    @IBOutlet weak var image_payment: UIImageView!
    @IBOutlet weak var lbl_bilInfo: UILabel!
    @IBOutlet weak var lbl_dillInfo: UILabel!
    @IBOutlet weak var img_delevary: UIImageView!
    @IBOutlet weak var height_CheakOutView: NSLayoutConstraint!
    @IBOutlet weak var heightTVShopping: NSLayoutConstraint!
    @IBOutlet weak var view_cupenCode: UIView!
    @IBOutlet weak var lbl_cupenCodeAmount: UILabel!
    @IBOutlet weak var btn_confims: UIButton!
    @IBOutlet weak var lbl_totalAmmount: UILabel!
    @IBOutlet weak var lbl_cuoencode: UILabel!
    @IBOutlet weak var lbl_SubTotal: UILabel!
    @IBOutlet weak var btn_CheakOut: UIButton!
    @IBOutlet weak var CVtextView: UICollectionView!
    
    @IBOutlet weak var cupencodeTotal: UILabel!
    @IBOutlet weak var lbl_subTotalTop: NSLayoutConstraint!
    @IBOutlet weak var lbl_cupenCodeTitle: UILabel!
    @IBOutlet weak var cheakOutView1: UIView!
    
    @IBOutlet weak var lbl_discount: UILabel!
    
    // MARK: - Variables
    var Guest_productArray: [CartsList] = []
    var Guest_txinfoarray: [TextdataList] = []
    var cartListArray: [CartsList] = []
    var textinfoArray: [TextdataList] = []

    private static let backendURL = URL(string: "https://apps.rajodiya.com/ecommerce/api/payment-sheet")!
    private var paymentIntentClientSecret: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_subTotalTop.constant = 8
        height_CheakOutView.constant = 190
        cheakOutView1.isHidden = true
        lbl_cupenCodeTitle.isHidden = true
        view_cupenCode.isHidden = true
        tableShopping.register(UINib.init(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
        CVtextView.register(UINib(nibName: "TextDetailsCell", bundle: nil), forCellWithReuseIdentifier: "TextDetailsCell")
        tableShopping.tableFooterView = UIView()
        StripeAPI.defaultPublishableKey = "pk_test_aLoicAw16w12wxLlmI9uluaf008XMnn2VJ"
        self.fetchPaymentIntent()
        
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            
            if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                
                let guest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                self.Guest_productArray = ProductsList.init(guest_array).cartsList
                
                let Guest_textArray = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTTEXTARRAY) as! [[String:Any]]
                self.Guest_txinfoarray = TextData.init(Guest_textArray).textdatalists
                
                let billinginformation = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_BILLINGOBJ) as! [String:Any]
                
                self.lbl_bilInfo.text = "\(billinginformation["firstname"]!) \(billinginformation["lastname"]!) \n \(billinginformation["billing_address"]!), \(billinginformation["billing_city"]!), \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SELECTEDSTATE)!), \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)!) - \(billinginformation["billing_postecode"]!), \n Phone:\(billinginformation["billing_user_telephone"]!) \n Email:\(billinginformation["email"]!)"
                
                if billinginformation["delivery_address"] as! String == "" {
                    self.lbl_dillInfo.text = "_"
                }else{
                    self.lbl_dillInfo.text = "\(billinginformation["firstname"]!) \(billinginformation["lastname"]!) \n \(billinginformation["delivery_address"]!), \(billinginformation["delivery_city"]!), \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)!), \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)!) - \(billinginformation["delivery_postcode"]!), \n Phone:\(billinginformation["billing_user_telephone"]!) \n Email:\(billinginformation["email"]!)"
                }
                let GUest_cupenobj = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CUPENOBJECT) as! [String:Any]
                if GUest_cupenobj.count != 0 {
                    self.lbl_cupenCodeAmount.text = "-\(GUest_cupenobj["coupon_final_amount"]!) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) for all products "
                    self.lbl_discount.text = "(-\(GUest_cupenobj["coupon_final_amount"]!)\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)!))"
                    self.lbl_cuoencode.text = "\(GUest_cupenobj["coupon_code"]!)"
                    self.lbl_subTotalTop.constant = 103.5
                    self.lbl_cupenCodeTitle.isHidden = false
                    self.cheakOutView1.isHidden = false
                    self.view_cupenCode.isHidden = false
                    self.height_CheakOutView.constant = 267
                }
                else{
                    self.height_CheakOutView.constant = 190
                }
                self.tableShopping.reloadData()
                self.heightTVShopping.constant = CGFloat(self.Guest_productArray.count * 90)
                
                if self.Guest_txinfoarray.count == 2{
                    self.CVtextView.isScrollEnabled = false
                }else{
                    self.CVtextView.isScrollEnabled = true
                }
                self.image_payment.sd_setImage(with: URL(string: "\(URL_PaymentImage)/\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)!)"), placeholderImage: UIImage(named: ""))
                self.img_delevary.sd_setImage(with: URL(string: "\(URL_BASEIMAGE)\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYIMAGE)!)"), placeholderImage: UIImage(named: ""))
        
                self.lbl_SubTotal.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_GUESTSUBTOTAL)
                self.lbl_totalAmmount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_GUESTFINALTOTAL)
                self.CVtextView.reloadData()
//                self.viewempty.isHidden = false
            }
        }else{
            let param: [String:Any] = ["payment_type": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE)!,
                                       "billing_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_BILLINGOBJ)!,
                                       "coupon_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CUPENOBJECT)!,
                                       "delivery_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)!,
                                       "user_id": getID(),
                                       "payment_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)!,
                                       "delivery_id": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYID)!,
                                       "theme_id": APP_THEME_ID]
            getConfimOrder(param)
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCheckout(_ sender: Any){
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE) == "stripe"{
            pay()
        }else if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let gestparam: [String:Any] = ["product": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSERPRODUCTARRAY)!,
                                       "tax_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)!,
                                       "coupon_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CUPENOBJECT)!,
                                       "billing_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_BILLINGOBJ)!,
                                       "payment_type": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE)!,
                                       "payment_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)!,
                                       "delivery_id": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYID)!,
                                       "delivery_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)!,
                                        "theme_id": APP_THEME_ID]
            getPlaceOrderGuest(gestparam)
        }else if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE) == "stripe"{
            pay()
        }else{
            let param: [String:Any] = ["user_id": getID(),
                                       "coupon_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CUPENOBJECT)!,
                                       "billing_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_BILLINGOBJ)!,
                                       "payment_type": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE)!,
                                       "payment_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)!,
                                       "delivery_id": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYID)!,
                                       "delivery_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)!,
                                       "theme_id": APP_THEME_ID]
          getOrderPlaced(param)
        }
    }
    // MARK: - CommenFunction
    
    func fetchPaymentIntent() {
        let url = Self.backendURL

        let shoppingCartContent: [String: Any] = ["price": UserDefaults.standard.integer(forKey: userDefaultsKeys.KEY_GUESTFINALTOTAL),
                                                  "currency": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)!,
                                                  "theme_id": APP_THEME_ID]

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONSerialization.data(withJSONObject: shoppingCartContent)

        let task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                let clientSecret = json["clientSecret"] as? String
            else {
                let message = error?.localizedDescription ?? "Failed to decode response from server."
                self?.displayAlert(title: "Error loading page", message: message)
                return
            }

            print("Created PaymentIntent")
            self?.paymentIntentClientSecret = clientSecret

            DispatchQueue.main.async {
                self?.btn_CheakOut.isEnabled = true
            }
        })

        task.resume()
    }

    func displayAlert(title: String, message: String? = nil) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController, animated: true)
        }
    }
    
    func pay() {
        guard let paymentIntentClientSecret = self.paymentIntentClientSecret else {
            return
        }

        var configuration = PaymentSheet.Configuration()
        configuration.merchantDisplayName = "Example, Inc."

        let paymentSheet = PaymentSheet(
            paymentIntentClientSecret: paymentIntentClientSecret,
            configuration: configuration)

        paymentSheet.present(from: self) { [weak self] (paymentResult) in
            switch paymentResult {
            case .completed:
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    let gestparam: [String:Any] = ["product": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSERPRODUCTARRAY)!,
                                               "tax_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)!,
                                               "coupon_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CUPENOBJECT)!,
                                               "billing_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_BILLINGOBJ)!,
                                               "payment_type": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE)!,
                                               "payment_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)!,
                                               "delivery_id": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYID)!,
                                               "delivery_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)!,
                                                "theme_id": APP_THEME_ID]
                    self!.getPlaceOrderGuest(gestparam)
                }else{
                    let param: [String:Any] = ["user_id": getID(),
                                               "coupon_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CUPENOBJECT)!,
                                               "billing_info": UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_BILLINGOBJ)!,
                                               "payment_type": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTTYPE)!,
                                               "payment_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)!,
                                               "delivery_id": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYID)!,
                                               "delivery_comment": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)!,
                                               "theme_id": APP_THEME_ID]
                    self!.getOrderPlaced(param)
                }
            case .canceled:
                print("Payment canceled!")
            case .failed(let error):
                self?.displayAlert(title: "Payment failed", message: error.localizedDescription)
            }
        }
    }
    // MARK: - APIFunctions
    
    func getConfimOrder(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_ConfimOrder, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let cupeninfo = data["coupon_info"] as? [String:Any] {
                                    let cupencode = cupeninfo["code"] as? String
                                    self.lbl_cuoencode.text = cupencode
                                    let discountString = cupeninfo["discount_string"] as? String
                                    let discountStringtwo = cupeninfo["discount_string2"] as? String
                                    self.lbl_cupenCodeAmount.text = "\(discountString!)"
                                    self.lbl_discount.text = "\(discountStringtwo!)"
                                    self.lbl_subTotalTop.constant = 100
                                    self.lbl_cupenCodeTitle.isHidden = false
                                    self.cheakOutView1.isHidden = false
                                    self.view_cupenCode.isHidden = false
                                    self.height_CheakOutView.constant = 267
                                }
                                if let text = data["tax"] as? [[String:Any]]{
                                    self.textinfoArray = TextData.init(text).textdatalists
                                    self.CVtextView.reloadData()
                                }
                                if let product = data["product"] as? [[String:Any]]{
                                    self.cartListArray = ProductsList.init(product).cartsList
                                    self.tableShopping.reloadData()
                                    self.heightTVShopping.constant = CGFloat(self.cartListArray.count * 89)
                                }
                                if let billinginfo = data["billing_information"] as? [String:Any] {
                                    let name  = billinginfo["name"] as? String
                                    let address = billinginfo["address"] as? String
                                    let email = billinginfo["email"] as? String
                                    let phone = billinginfo["phone"] as? String
                                    let country = billinginfo["country"] as? String
                                    let state = billinginfo["state"] as? String
                                    let city = billinginfo["city"] as? String
                                    let postcode = billinginfo["postecode"] as? Int
                                    
                                    self.lbl_bilInfo.text = "\(name!) \n \(address!) \(city!) \(state!) \(country!) - \(postcode!) \n Phone: \(phone!) \n Email: \(email!)"
                                }
                                if let delivaryInfo = data["delivery_information"] as? [String:Any] {
                                    let name  = delivaryInfo["name"] as! String
                                    let email = delivaryInfo["email"] as! String
                                    let phone = delivaryInfo["phone"] as! String
                                    let country = delivaryInfo["country"] as! String
                                    let state = delivaryInfo["state"] as! String
                                    let city = delivaryInfo["city"] as! String
                                    let postcode = delivaryInfo["postecode"] as! Int
                                    let address = delivaryInfo["address"] as! String
                                    if address == "" {
                                        self.lbl_dillInfo.text = "-"
                                    }else{
                                        self.lbl_dillInfo.text = "\(name) \n \(address) \(city) \(state) \(country) - \(postcode) \n Phone: \(phone) \n Email: \(email)"
                                    }
                                }
                                let payment = data["paymnet"] as? String
                                self.image_payment.sd_setImage(with: URL(string: "\(URL_PaymentImage)/\(payment!)"), placeholderImage: UIImage(named: ""))
                                let delivary = data["delivery"] as? String
                                self.img_delevary.sd_setImage(with: URL(string: "\(URL_BASEIMAGE)\(delivary!)"), placeholderImage: UIImage(named: ""))
                                
                                let sub_total = data["subtotal"] as? String
                                self.lbl_SubTotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(sub_total!)"
                                let finalPrice = data["final_price"] as? String
                                self.lbl_totalAmmount.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(finalPrice!)"
        //                        self.viewempty.isHidden = false
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func getOrderPlaced(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_OrderPlaced, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let compelteorder = data["complete_order"] as? [String:Any]{
                                    if let ordercomplete = compelteorder["order-complate"] as? [String:Any]{
                                        let ordercomplatetitle = ordercomplete["order-complate-title"] as! String
                                        let orderComplateDiscription = ordercomplete["order-complate-description"] as! String
                                        let vc = OrderPlacedVC(nibName: "OrderPlacedVC", bundle: nil)
                                        vc.modalPresentationStyle = .overCurrentContext
                                        vc.parentVC = self
                                        vc.ordercomTitle = ordercomplatetitle
                                        vc.ordercomDescription = orderComplateDiscription
                                        self.navigationController?.present(vc, animated: true, completion: nil)
                                    }
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func getPlaceOrderGuest(_ gestparam: [String:Any]){
        let headers : HTTPHeaders = ["Content-Type": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_OrderPlacedGuest, params: gestparam, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let compelteorder = data["complete_order"] as? [String:Any]{
                                    if let ordercomplete = compelteorder["order-complate"] as? [String:Any]{
                                        let ordercomplatetitle = ordercomplete["order-complate-title"] as! String
                                        let orderComplateDiscription = ordercomplete["order-complate-description"] as! String
                                        
                                        UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                        UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSERPRODUCTARRAY)
                                        UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)

                                        UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)
                                        UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_CUPENOBJECT)
                                        UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_BILLINGOBJ)

                                        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                                        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)
                                        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_DELIVARYID)
                                        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)
                                        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        
                                        let vc = OrderPlacedVC(nibName: "OrderPlacedVC", bundle: nil)
                                        vc.modalPresentationStyle = .overCurrentContext
                                        vc.parentVC = self
                                        vc.ordercomTitle = ordercomplatetitle
                                        vc.ordercomDescription = orderComplateDiscription
                                        self.navigationController?.present(vc, animated: true, completion: nil)
                                    }
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
}
extension CheckoutVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            return self.Guest_productArray.count
        }else{
            return cartListArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
            cell.lbl_proName.text = Guest_productArray[indexPath.row].name
            let productimgURL = "\(Guest_productArray[indexPath.row].image!)"
            cell.img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
                cell.img_product.image = image
            }
            let price = Double(Guest_productArray[indexPath.row].finalprice!)! * Double(Guest_productArray[indexPath.row].qty!)
            cell.lbl_price.text = "QTY : \(Guest_productArray[indexPath.row].qty!)"
            cell.lbl_proPrice.text = "\(price)"
            cell.lbl_qty.text = "\(Guest_productArray[indexPath.row].qty!)"
            cell.qtyProView.isHidden = true
            cell.lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
            cell.lbl_proName.text = cartListArray[indexPath.row].name
            let productimgURL = getImageFullURL("\(cartListArray[indexPath.row].image!)")
            cell.img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
                cell.img_product.image = image
            }
            cell.lbl_price.text = "Qty \(cartListArray[indexPath.row].qty!)"
            cell.lbl_proPrice.text = "\(cartListArray[indexPath.row].finalprice!)"
            cell.lbl_qty.text = "\(cartListArray[indexPath.row].qty!)"
            cell.qtyProView.isHidden = true
            cell.lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension CheckoutVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            return Guest_txinfoarray.count
        }else{
            return textinfoArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let cell = self.CVtextView.dequeueReusableCell(withReuseIdentifier: "TextDetailsCell", for: indexPath) as! TextDetailsCell
            let data = self.Guest_txinfoarray[indexPath.item]
            cell.lbl_textTitle.text = data.taxString
            cell.lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(data.taxPrice!)"
            return cell
        }else{
            let cell = self.CVtextView.dequeueReusableCell(withReuseIdentifier: "TextDetailsCell", for: indexPath) as! TextDetailsCell
            cell.configureCell(textinfoArray[indexPath.item])
            return cell
        }
    }
}
