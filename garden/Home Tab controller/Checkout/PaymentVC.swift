//
//  PaymentVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit

class PaymentVC: UIViewController {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var paymentList: UITableView!
    @IBOutlet weak var heightPaymentList: NSLayoutConstraint!
    @IBOutlet weak var btnSelected: UIButton!
    @IBOutlet weak var btnTermsCondition: UIButton!
    
    // MARK: - Variables
    var selectedindex = 0
    var ArrayPayment: [PaymentList] = []
    var ArrayOnStatus: [PaymentList] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.paymentList.isHidden = true
        self.paymentList.register(UINib(nibName: "DelivaryCell", bundle: nil), forCellReuseIdentifier: "DelivaryCell")
        self.btnSelected.setImage(UIImage(named: "checkbox"), for: .normal)
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getPaymentList(param)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnSelected(_ sender: UIButton){
        if self.btnSelected.imageView?.image == UIImage.init(named: "checkbox")
        {
            self.btnSelected.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        }
        else{
            self.btnSelected.setImage(UIImage.init(named: "checkbox"), for: .normal)
        }
    }
    @IBAction func onClickContinue(_ sender: UIButton){
        
        if self.btnSelected.imageView?.image == UIImage(named: "checkbox"){
            showAlertMessage(titleStr: "", messageStr: "Please agree to the Terms & Conditions.")
        }else{
            if self.descriptionTextView.text == "Description"{
                self.descriptionTextView.text = ""
            }
            let data = ArrayOnStatus[selectedindex]
            if data.nameString == "COD"{
                UserDefaults.standard.set("cod", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }else if data.nameString == "Bank Transfer"{
                UserDefaults.standard.set("bank_transfer", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }else if data.nameString == "other_payment"{
                UserDefaults.standard.set("Other Payment", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }else if data.nameString == "Stripe"{
                UserDefaults.standard.set("stripe", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }
            UserDefaults.standard.set(self.descriptionTextView.text, forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func onClickTermsCondition(_ sender: UIButton){
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_TERMS)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    // MARK: - APIFunctions
    func getPaymentList(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_PaymentList, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayPayment = Payment.init(data).paymentlist
                                self.ArrayOnStatus = self.ArrayPayment.filter({$0.status == "on"})
                                self.paymentList.estimatedRowHeight = 120
                                self.paymentList.rowHeight = UITableView.automaticDimension
                                self.paymentList.reloadData()
                                self.heightPaymentList.constant = CGFloat(120 * self.ArrayOnStatus.count)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    self.heightPaymentList.constant = self.paymentList.contentSize.height
                                    self.paymentList.isHidden = false
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
}
extension PaymentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayOnStatus.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DelivaryCell", for: indexPath) as! DelivaryCell
        let datas = ArrayOnStatus[indexPath.row]
        if indexPath.row == self.selectedindex{
            cell.viewCell.borderWidth = 1
            cell.viewCell.borderColor = hexStringToUIColor(hex: "#B5C547")
            cell.img_selected.image = UIImage(named: "radio_checked")
        }else{
            cell.viewCell.borderWidth = 1
            cell.viewCell.borderColor = .white
            cell.img_selected.image = UIImage(named: "radio_uncheck")
        }
        cell.lbl_dillName.text = datas.nameString
        cell.lbl_dillDis.text = datas.detail
        cell.lbl_price.isHidden = true
        cell.lbl_aditionalPrice.isHidden = true
        let imageURL = "\(URL_PaymentImage)/\(datas.image!)"
        cell.img_delevary.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
            cell.img_delevary.image = image
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.ArrayOnStatus[indexPath.row]
        if data.nameString == "COD"
        {
            UserDefaults.standard.set("cod", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
        }
        else if data.nameString == "Bank Transfer"
        {
            UserDefaults.standard.set("bank_transfer", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
        }
        else if data.nameString == "Stripe"{
            UserDefaults.standard.set("stripe", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
        }
        self.selectedindex = indexPath.item
        self.paymentList.reloadData()
    }
    
}
