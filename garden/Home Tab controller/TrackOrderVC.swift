//
//  TrackOrderVC.swift
//  plants
//
//  Created by mac on 06/12/22.
//

import UIKit

class TrackOrderVC: UIViewController {
    
    @IBOutlet weak var image_circle_one: UIImageView!
    @IBOutlet weak var lbl_titleOne: UILabel!
    @IBOutlet weak var lbl_lineOne: UILabel!
    @IBOutlet weak var image_circle_two: UIImageView!
    @IBOutlet weak var lbl_titletwo: UILabel!
    @IBOutlet weak var lbl_linetwo: UILabel!
    
    var Status = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.Status == "Pending"
        {
            self.image_circle_one.image = UIImage(systemName: "circle.fill")
            self.image_circle_one.tintColor = hexStringToUIColor(hex: "#B5C547")
            self.lbl_lineOne.backgroundColor = hexStringToUIColor(hex: "#B5C547")
            
            self.image_circle_two.image = UIImage(systemName: "circle")
            self.image_circle_two.tintColor = .white
            self.lbl_linetwo.backgroundColor = .white
        }
        else if self.Status == "Delivered" {
            
            self.image_circle_one.image = UIImage(systemName: "circle.fill")
            self.image_circle_one.tintColor = hexStringToUIColor(hex: "#B5C547")
            self.lbl_lineOne.backgroundColor = hexStringToUIColor(hex: "#B5C547")
            
            self.image_circle_two.image = UIImage(systemName: "circle.fill")
            self.image_circle_two.tintColor = hexStringToUIColor(hex: "#B5C547")
            self.lbl_linetwo.backgroundColor = hexStringToUIColor(hex: "#B5C547")
        }else{
            print("cancel or return")
        }
    }
    
    @IBAction func onclickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
