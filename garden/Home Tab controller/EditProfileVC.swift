//
//  EditProfileVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class EditProfileVC: UIViewController {

    @IBOutlet weak var text_name: UITextField!
    @IBOutlet weak var text_lname: UITextField!
    @IBOutlet weak var text_email: UITextField!
    @IBOutlet weak var text_telephone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            
        }else{
            self.text_name.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_FIRSTNAME)
            self.text_lname.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_LASTNAME)
            self.text_telephone.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PHONE)
            self.text_email.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_EMAIL)
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclikDeleteMyAccount(_ sender: Any) {
        if IS_DEMO_MODE == true {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
        }else{
            let param: [String:Any] = ["user_id": getID(),
                                       "theme_id": APP_THEME_ID]
            deleteMyAccount(param)
        }
    }
    
    @IBAction func onclickSaveChanges(_ sender: UIButton) {
        if IS_DEMO_MODE == true {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
        }else{
            if self.text_email.text == ""
            {
                showAlertMessage(titleStr: "", messageStr: "Please enter the email address.")
            }
            else if self.text_telephone.text == ""
            {
                showAlertMessage(titleStr: "", messageStr: "Please enter phone number.")
            }
            else if self.text_name.text == ""
            {
                showAlertMessage(titleStr: "", messageStr: "Please enter first name.")
            }
            else if self.text_lname.text == ""
            {
                showAlertMessage(titleStr: "", messageStr: "Please enter last name.")
            }else{
                UserDefaults.standard.set(self.text_name.text, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                UserDefaults.standard.set(self.text_lname.text, forKey: userDefaultsKeys.KEY_LASTNAME)
                UserDefaults.standard.set(self.text_telephone.text, forKey: userDefaultsKeys.KEY_PHONE)
                UserDefaults.standard.set(self.text_email.text, forKey: userDefaultsKeys.KEY_EMAIL)
                let param: [String:Any] = ["user_id": getID(),
                                           "first_name": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_FIRSTNAME)!,
                                           "last_name": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_LASTNAME)!,
                                            "email": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_EMAIL)!,
                                           "telephone": UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PHONE)!]
                getProfileUpdate(param)
            }
        }
    }
    
    // MARK: - API functions
    func getProfileUpdate(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_ProfileUpdate, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        if let dicdata = data["data"] as? [String:Any]{
                                            let firstname = dicdata["first_name"] as? String
                                            UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                                            let lastname = dicdata["last_name"] as? String
                                            UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                                            let email = dicdata["email"] as? String
                                            UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
        //                                    let image = dicdata["image"] as? String
        //                                    UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                                            let phone = dicdata["mobile"] as? String
                                            UserDefaults.standard.set(phone, forKey: userDefaultsKeys.KEY_PHONE)
                                            let name = dicdata["name"] as? String
                                            UserDefaults.standard.set(name, forKey: userDefaultsKeys.KEY_FULLNAME)
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    func deleteMyAccount(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_DeleteAccount, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                       let massage = msg["message"] as! String
                                       showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                            print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }

}
