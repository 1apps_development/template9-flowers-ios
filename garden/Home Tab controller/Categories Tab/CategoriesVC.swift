//
//  CategoriesVC.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit

class CategoriesVC: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var bottomCircleMenu: UIView!
    @IBOutlet weak var tableCategories: UITableView!
    @IBOutlet var heightOfTable: NSLayoutConstraint!
    @IBOutlet weak var viewInsideCircle1: UIView!
    @IBOutlet weak var viewInsideCircle2: UIView!
    @IBOutlet weak var viewInsideCircle3: UIView!
    @IBOutlet weak var viewInsideCircle4: UIView!
    @IBOutlet weak var ic1of4: UIImageView!
    @IBOutlet weak var ic2of4: UIImageView!
    @IBOutlet weak var ic3of4: UIImageView!
    @IBOutlet weak var ic4of4: UIImageView!
    @IBOutlet weak var ic1of3: UIImageView!
    @IBOutlet weak var ic2of3: UIImageView!
    @IBOutlet weak var ic3of3: UIImageView!
    @IBOutlet weak var ic1of2: UIImageView!
    @IBOutlet weak var ic2of2: UIImageView!
    @IBOutlet weak var ic1of1: UIImageView!
    @IBOutlet weak var lbl1Of4: UILabel!
    @IBOutlet weak var lbl2Of4: UILabel!
    @IBOutlet weak var lbl3Of4: UILabel!
    @IBOutlet weak var lbl4Of4: UILabel!
    @IBOutlet weak var lbl1Of3: UILabel!
    @IBOutlet weak var lbl2Of3: UILabel!
    @IBOutlet weak var lbl3Of3: UILabel!
    @IBOutlet weak var lbl1Of2: UILabel!
    @IBOutlet weak var lbl2Of2: UILabel!
    @IBOutlet weak var lbl1Of1: UILabel!
    @IBOutlet weak var lbl_cartCount: UILabel!

    var arrCategories : [categoryModel] = []
    var isSelectedAgain:Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: Notification.Name(rawValue: "NOTIFICATION_CENTER_TAB"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
    }
    
    //MARK: - Custom/Other Functions
    
    func setupInitialUI(){
        tableCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellReuseIdentifier: "CategoriesCell")
        tableCategories.tableFooterView = UIView()

        MakeAPICallforCategoriesList(["theme_id":APP_THEME_ID])
    }
    
    @objc func notificationReceived(_ noti: Notification){
        isSelectedAgain = true
        if isSelectedAgain{
            UIView.animate(withDuration: 0.3) {
                self.bottomCircleMenu.isHidden = false
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.onClickBottomClose(scrollView)
    }
    
    func setDataInBottomCircleMenu(){
        if arrCategories.count == 1{
            
            self.lbl1Of1.text = self.arrCategories[0].name
            let endpoint = self.arrCategories[0].iconPath
            let urlIcon = getImageFullURL("\(endpoint!)")
            self.ic1of1.sd_setImage(with: URL(string: urlIcon), placeholderImage: UIImage(named: "placeholder"))
            
            self.viewInsideCircle4.isHidden = true
            self.viewInsideCircle1.isHidden = false
            self.viewInsideCircle3.isHidden = true
            self.viewInsideCircle2.isHidden = true
            
        }else if arrCategories.count == 2{
            
            self.lbl1Of2.text = self.arrCategories[0].name
            let endpoint = self.arrCategories[0].iconPath
            let urlIcon = getImageFullURL("\(endpoint!)")
            self.ic1of2.sd_setImage(with: URL(string: urlIcon), placeholderImage: UIImage(named: "placeholder"))

            self.lbl1Of2.text = self.arrCategories[1].name
            let endpoint1 = self.arrCategories[1].iconPath
            let urlIcon1 = getImageFullURL("\(endpoint1!)")
            self.ic2of2.sd_setImage(with: URL(string: urlIcon1), placeholderImage: UIImage(named: "placeholder"))

            self.viewInsideCircle4.isHidden = true
            self.viewInsideCircle1.isHidden = true
            self.viewInsideCircle3.isHidden = true
            self.viewInsideCircle2.isHidden = false
            
        }else if arrCategories.count == 3{
           
            self.lbl1Of3.text = self.arrCategories[0].name
            let endpoint = self.arrCategories[0].iconPath
            let urlIcon = getImageFullURL("\(endpoint!)")
            self.ic1of3.sd_setImage(with: URL(string: urlIcon), placeholderImage: UIImage(named: "placeholder"))

            self.lbl1Of3.text = self.arrCategories[1].name
            let endpoint1 = self.arrCategories[1].iconPath
            let urlIcon1 = getImageFullURL("\(endpoint1!)")
            self.ic2of3.sd_setImage(with: URL(string: urlIcon1), placeholderImage: UIImage(named: "placeholder"))

            self.lbl3Of3.text = self.arrCategories[2].name
            let endpoint2 = self.arrCategories[2].iconPath
            let urlIcon2 = getImageFullURL("\(endpoint2!)")
            self.ic3of3.sd_setImage(with: URL(string: urlIcon2), placeholderImage: UIImage(named: "placeholder"))

            self.viewInsideCircle4.isHidden = true
            self.viewInsideCircle1.isHidden = true
            self.viewInsideCircle3.isHidden = false
            self.viewInsideCircle2.isHidden = true
            
        }else{
            
            self.lbl1Of4.text = self.arrCategories[0].name
            let endpoint = self.arrCategories[0].iconPath
            let urlIcon = getImageFullURL("\(endpoint!)")
            self.ic1of4.sd_setImage(with: URL(string: urlIcon), placeholderImage: UIImage(named: "placeholder"))

            self.lbl2Of4.text = self.arrCategories[1].name
            let endpoint1 = self.arrCategories[1].iconPath
            let urlIcon1 = getImageFullURL("\(endpoint1!)")
            self.ic2of4.sd_setImage(with: URL(string: urlIcon1), placeholderImage: UIImage(named: "placeholder"))
           
            self.lbl3Of4.text = self.arrCategories[2].name
            let endpoint2 = self.arrCategories[2].iconPath
            let urlIcon2 = getImageFullURL("\(endpoint2!)")
            self.ic3of4.sd_setImage(with: URL(string: urlIcon2), placeholderImage: UIImage(named: "placeholder"))

            self.lbl4Of4.text = self.arrCategories[3].name
            let endpoint3 = self.arrCategories[3].iconPath
            let urlIcon3 = getImageFullURL("\(endpoint3!)")
            self.ic4of4.sd_setImage(with: URL(string: urlIcon3), placeholderImage: UIImage(named: "placeholder"))

            
            self.viewInsideCircle4.isHidden = false
            self.viewInsideCircle1.isHidden = true
            self.viewInsideCircle3.isHidden = true
            self.viewInsideCircle2.isHidden = true
        }
        
    }

    
    //MARK: - IBActions
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Invoices"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }

    @IBAction func onClickFilter(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickBottomClose(_ sender: Any){
        isSelectedAgain = false
        UIView.animate(withDuration: 0.3) {
            self.bottomCircleMenu.isHidden = true
        }
    }
    
    @IBAction func onClickBottomCircleMenuItem(_ sender: UIButton){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
       
        //view1
        if sender.tag == 11{
            vc.catid = self.arrCategories[0].id
        }
        
        //view2
        else if sender.tag == 12{
            vc.catid = self.arrCategories[0].id

        }else if sender.tag == 22{
            vc.catid = self.arrCategories[1].id
        }
        
        //view3
        else if sender.tag == 13{
            vc.catid = self.arrCategories[0].id

        }else if sender.tag == 23{
            vc.catid = self.arrCategories[1].id

        }else if sender.tag == 33{
            vc.catid = self.arrCategories[2].id

        }
        
        //view4
        else if sender.tag == 14{
            vc.catid = self.arrCategories[0].id

        }else if sender.tag == 24{
            vc.catid = self.arrCategories[1].id

        }else if sender.tag == 34{
            vc.catid = self.arrCategories[2].id

        }else if sender.tag == 44{
            vc.catid = self.arrCategories[3].id
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - API
    
    func MakeAPICallforCategoriesList(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let dataArr = data["data"] as? [[String:Any]]{
                            self.arrCategories = HomeCatSuperModel.init(dataArr).categories
                            self.heightOfTable.constant = CGFloat(247 * self.arrCategories.count)
                            self.view.layoutSubviews()
                            self.setDataInBottomCircleMenu()
                            self.tableCategories.reloadData()
                        }

                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
}


extension CategoriesVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell") as! CategoriesCell
        cell.selectionStyle = .none
        cell.configureCell(arrCategories[indexPath.row])
        cell.btnShowmore.tag = indexPath.row
        cell.btnShowmore.addTarget(self, action: #selector(onclickShowMore), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
        vc.catid = arrCategories[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 247
    }
    @objc func onclickShowMore(_ sender: UIButton){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
        vc.catid = arrCategories[sender.tag].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
