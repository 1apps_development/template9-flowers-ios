//
//  ProductListingVC.swift
//  jewellery
//
//  Created by mac on 04/11/22.
//

import UIKit
import Alamofire


class ProductListingVC: UIViewController {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblProducts: UILabel!
    @IBOutlet weak var categoriesCV: UICollectionView!
    @IBOutlet weak var productsCV: UICollectionView!
    @IBOutlet weak var productCVHeigh: NSLayoutConstraint!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var lbl_cartCount: UILabel!

    
    var arrCategories:[categoryModel] = []
    var arrProducts:[Products] = []
    var categoryId: Int!
    var productid = Int()
    var varientId = Int()
    
    
    var pageindex = 1
    var lastindex = 0
    
    var productpageindex = 1
    var productlastindex = 0
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        self.categoriesCV.register(UINib.init(nibName: "CategoryDummyCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDummyCell")
        self.categoriesCV.register(UINib.init(nibName: "categoryCVCell", bundle: nil), forCellWithReuseIdentifier: "categoryCVCell")
        self.productsCV.register(UINib.init(nibName: "PlantCell", bundle: nil), forCellWithReuseIdentifier: "PlantCell")
        self.ScrollView.delegate = self
        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        let catParam: [String: Any] = ["theme_id": APP_THEME_ID]
        getCategoriesData(catParam)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
    }
     
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickCart(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(self.ScrollView.contentOffset.y) >=  Int(self.ScrollView.contentSize.height - self.ScrollView.frame.size.height)) {
            if self.productpageindex != self.productlastindex {
                self.productpageindex = self.productpageindex + 1
                if self.arrProducts.count != 0 {
                    if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
                    {
                        let param: [String: Any] = ["theme_id": APP_THEME_ID]
                        getProductsGuestData(param)
                    }
                    else{
                        let proParam: [String:Any] = ["maincategory_id": self.categoryId!,
                                                      "theme_id": APP_THEME_ID]
                        getbestSellersData(proParam)
                    }
                }
            }
        }
    }
    
    //MARK: - API
    func getCategoriesData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories + "\(pageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.pageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.lastindex = lastpage
                                    self.arrCategories.removeAll()
                                }
                                if let categories = data["data"] as? [[String:Any]]{
                                    self.arrCategories.append(contentsOf: HomeCatSuperModel.init(categories).categories)
                                    self.categoriesCV.reloadData()
                                }
                                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                                    self.categoryId = 0
                                    let param: [String:Any] = ["maincategory_id": self.categoryId!,
                                                               "theme_id": APP_THEME_ID]
                                    self.getProductsGuestData(param)
                                }else{
                                    self.categoryId = 0
                                    let param: [String:Any] = ["maincategory_id": self.categoryId!,
                                                               "theme_id": APP_THEME_ID]
                                    self.getbestSellersData(param)
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
  }
    func getbestSellersData(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                    "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_CategoryProduct + "\(productpageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.productpageindex == 1 {
                                    let productLastpage = data["last_page"] as! Int
                                    self.productlastindex = productLastpage
                                    self.arrProducts.removeAll()
                                }
                                if let products = data["data"] as? [[String:Any]]{
                                    self.arrProducts.append(contentsOf: ProductsSuperModel.init(products).product)
                                    self.productsCV.reloadData()
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.productCVHeigh.constant = self.productsCV.contentSize.height
                                    }
                                }
                                let productTotal = data["total"] as! Int
                                self.lblProducts.text = "\(productTotal) products"
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }

                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
       }
    }
}
    func getProductsGuestData(_ param: [String:Any]){
                AIServiceManager.sharedManager.callPostApi(URL_CategoryProductGuest + "\(productpageindex)", params: param, nil) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                            if self.productpageindex == 1 {
                                                let lastpage = data["last_page"] as! Int
                                                self.productlastindex = lastpage
                                                self.arrProducts.removeAll()
                                            }
                                            if let catData = data["data"] as? [[String:Any]]{
                                                self.arrProducts.append(contentsOf: ProductsSuperModel.init(catData).product)
                                                self.productsCV.reloadData()
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                                    self.productCVHeigh.constant = self.productsCV.contentSize.height
                                                }
                                            }
                                        let productTotal = data["total"] as! Int
                                        self.lblProducts.text = "\(productTotal) products"
                                    }
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                            
                            //                    print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
    }
    func addTocart(_ param: [String:Any]){
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String:Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let stats = json["data"] as? [String:Any]{
                                        let massage = stats["message"] as? String
                                        let status = json["status"] as? Int
                                        let count = stats["count"] as? Int
                                        UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        if status == 1 {
                                            let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                                self.dismiss(animated: true)
                                            }
                                            
                                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                            
                                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                            alert.addAction(photoLibraryAction)
                                            alert.addAction(cameraAction)
                                            alert.addAction(cancelAction)
                                            self.present(alert, animated: true, completion: nil)
                                        }else if status == 0 {
                                            if massage == "Product has out of stock."{
                                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                            }
                                        }
                                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    
                                        let alertVC = UIAlertController(title: "Garden", message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                            let param: [String:Any] = ["user_id": getID(),
                                                                       "product_id": self.productid,
                                                                       "variant_id": self.varientId,
                                                                       "quantity_type": "increase",
                                                                       "theme_id": APP_THEME_ID]
                                            self.getQtyOfProduct(param)
                                        }
                                        let noAction = UIAlertAction(title: "No", style: .destructive)
                                        alertVC.addAction(noAction)
                                        alertVC.addAction(yesAction)
                                        self.present(alertVC,animated: true,completion: nil)
                                }
                            }
                        }
                    case let .failure(error):
                        printD("error:\(error.localizedDescription)")
                        HIDE_CUSTOM_LOADER()
                    }
                }
            }
        }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        self.productsCV.reloadData()
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }

}

extension ProductListingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == self.categoriesCV ? self.arrCategories.count + 1 : self.arrProducts.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.categoriesCV{
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDummyCell", for: indexPath) as! CategoryDummyCell
                if categoryId == 0 {
                    cell.viewOuter.backgroundColor = hexStringToUIColor(hex: "#5C8973")
                    cell.lblCategoryName.textColor = hexStringToUIColor(hex: "#010F09")
                }else{
                    cell.viewOuter.backgroundColor = .clear
                    cell.lblCategoryName.textColor =  .white
                }
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCVCell", for: indexPath) as! categoryCVCell
            cell.configureCell(arrCategories[indexPath.item - 1])

            if categoryId == arrCategories[indexPath.item - 1].id{
                cell.viewOuter.backgroundColor = hexStringToUIColor(hex: "#5C8973")
                cell.lblCategoryName.textColor = hexStringToUIColor(hex: "#010F09")
                cell.imgCategory.image = cell.imgCategory.image?.withRenderingMode(.alwaysTemplate)
                cell.imgCategory.tintColor = hexStringToUIColor(hex: "#010F09")
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    cell.imgCategory.image = cell.imgCategory.image?.withRenderingMode(.alwaysTemplate)
                    cell.imgCategory.tintColor =  .white
                })
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlantCell", for: indexPath) as! PlantCell
            cell.configureCell(self.arrProducts[indexPath.row])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProducts[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let imagepath = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": imagepath,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProducts[indexPath.row].id!
                    self.varientId = self.arrProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btnFav.isHidden = true
            }else{
                cell.btnFav.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btnFav.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else{
                cell.btnFav.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btnFav.tag = indexPath.row
            cell.btnFav.addTarget(self, action: #selector(onclickFevBtn), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.categoriesCV{
            if indexPath.item == 0 {
                categoryId = 0
            }else{
                categoryId = arrCategories[indexPath.item - 1].id
             }
            self.productpageindex = 1
            self.productlastindex = 0
            let param: [String:Any] = ["maincategory_id": categoryId!,
                                       "theme_id": APP_THEME_ID]
            UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" ? getProductsGuestData(param) :  getbestSellersData(param)
            categoriesCV.reloadData()
        }else{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductDescriptionVC") as! ProductDescriptionVC
            vc.productId = self.arrProducts[indexPath.row].id!
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
        {
            if collectionView == self.categoriesCV
            {
                if indexPath.item == self.arrCategories.count - 1 {
                    if self.pageindex != self.lastindex{
                        self.pageindex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            getProductsGuestData(param)
                        }
                    }
                }
            }
        }else{
            if collectionView == self.categoriesCV
            {
                if indexPath.item == self.arrCategories.count - 1 {
                    if self.pageindex != self.lastindex {
                        self.pageindex += 1
                        if self.arrCategories.count != 0 {
                            let proParam: [String:Any] = ["theme_id": APP_THEME_ID]
                            getCategoriesData(proParam)
                        }
                    }
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView == self.categoriesCV ? CGSize(width: 90.0, height: 109.0) : CGSize(width: (self.productsCV.frame.width - 40) / 2, height: 350)
    }
    @objc func onclickFevBtn(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
