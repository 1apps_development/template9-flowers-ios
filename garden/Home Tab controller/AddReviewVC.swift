//
//  AddReviewVC.swift
//  plants
//
//  Created by mac on 06/12/22.
//

import UIKit

protocol FeedbackDelegate {
    func refreshData(id:Int,rating_no:String,title:String,description:String)
}

class AddReviewVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var viewAddreview: CosmosView!
    @IBOutlet weak var textAboutRate: UITextView!
    @IBOutlet weak var text_addrevird: UITextField!
    
    var delegate: FeedbackDelegate?
    var product_id = Int()
    var rattingscount = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textAboutRate.text = "Leave a message, if you want"
        self.textAboutRate.textColor = UIColor.lightGray
        self.textAboutRate.delegate = self
        viewAddreview.didFinishTouchingCosmos = { rating in
            print(rating)
            self.rattingscount = "\(rating)"
        }
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.init(named: "App_bg_Color")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Leave a message, if you want"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func onclickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickRateNoew(_ sender: UIButton) {
        if self.textAboutRate.text == "Leave a message, if you want" || self.text_addrevird.text == "" || self.rattingscount == ""
        {
            let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Please enter message", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "okay", style: .default)
            alertVC.addAction(yesAction)
            self.present(alertVC,animated: true,completion: nil)
        }
        else{
            if self.textAboutRate.text == "Leave a message, if you want"
            {
                self.textAboutRate.text = ""
            }
            self.delegate?.refreshData(id: self.product_id, rating_no: self.rattingscount, title: self.text_addrevird.text!, description: self.textAboutRate.text!)

            self.navigationController?.popViewController(animated: false)
            
        }
    }

}
