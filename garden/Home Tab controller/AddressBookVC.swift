//
//  AddressBookVC.swift
//  plants
//
//  Created by mac on 06/12/22.
//

import UIKit
import Alamofire

class AddressBookVC: UIViewController {

    @IBOutlet weak var TVAddressList: UITableView!
    @IBOutlet weak var View_empty: UIView!
    @IBOutlet weak var lbl_nodataFound: UILabel!
    @IBOutlet weak var heightaddressList: NSLayoutConstraint!

    //MARK: - Variables
    var selectedindex = 0
    var addressList = [AddressLists]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TVAddressList.register(UINib(nibName: "AddressBookCell", bundle: nil), forCellReuseIdentifier: "AddressBookCell")
        View_empty.isHidden = true
        lbl_nodataFound.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["user_id": getID(),
                                   "theme_id": APP_THEME_ID]
        getAddressList(param)
    }
    
    @IBAction func onclickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickAddAddress(_ sender: UIButton) {
        let vc = MainstoryBoard.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getAddressList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_Address, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let addressdata = data["data"] as? [[String:Any]]{
                                    self.addressList = Address.init(addressdata).addresslist
                                    self.TVAddressList.reloadData()
                                    self.heightaddressList.constant = CGFloat(100 * self.addressList.count)
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func deleteAddressList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_deleteAddress, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let param: [String:Any] = ["user_id": getID(),
                                                           "theme_id": APP_THEME_ID]
                                self.getAddressList(param)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
}
extension AddressBookVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.addressList.count == 0 {
            self.View_empty.isHidden = false
            self.lbl_nodataFound.isHidden = false
        }else{
            self.View_empty.isHidden = true
            self.lbl_nodataFound.isHidden = true
            return self.addressList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TVAddressList.dequeueReusableCell(withIdentifier: "AddressBookCell") as! AddressBookCell
        let data = self.addressList[indexPath.row]
        cell.lbl_addressTitle.text = data.title!
        cell.lbl_addressDis.text = "\(data.address!), \(data.cityname!) , \(data.statename!), \(data.countryname!) - \(data.postcode!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedindex = indexPath.item
        self.TVAddressList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                let data = self.addressList[indexPath.row]
                let param: [String:Any] = ["address_id": data.id!,
                                           "theme_id": APP_THEME_ID]
                self.deleteAddressList(param)
            }
        )
        let EditAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                let data = self.addressList[indexPath.row]
                let vc = MainstoryBoard.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
                vc.isedit = 1
                vc.EditedData = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
        )
        
        EditAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "password eye"),
            title: "Edit", textColor: UIColor(red: 194/255, green: 205/255, blue: 88/255, alpha: 1))
        EditAction.backgroundColor = UIColor(red: 194/255, green: 205/255, blue: 88/255, alpha: 1)
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_delete"),
            title: "Delete", textColor: UIColor(red: 194/255, green: 205/255, blue: 88/255, alpha: 1))
        deleteAction.backgroundColor = UIColor(red: 194/255, green: 205/255, blue: 88/255, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [deleteAction,EditAction])
    }
    
}
