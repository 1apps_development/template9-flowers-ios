//
//  LoyaltyProgramVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class LoyaltyProgramVC: UIViewController {
    
    @IBOutlet weak var tableLoyalty: UITableView!
    @IBOutlet weak var lbl_getcashes: UILabel!
    @IBOutlet weak var lbl_getcashesDis: UILabel!
    @IBOutlet weak var lbl_getYourCash: UILabel!
    @IBOutlet weak var lbl_links: UILabel!
    @IBOutlet weak var lbl_pointCash: UILabel!
    @IBOutlet weak var lbl_linkCopy: UILabel!
    @IBOutlet weak var height_loyality: NSLayoutConstraint!
    @IBOutlet weak var scroll_view: UIScrollView!
    
    var historyArray: [OrderList] = []
    var pageindex = 1
    var lastindex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll_view.delegate = self
        self.pageindex = 1
        self.lastindex = 0
        tableLoyalty.register(UINib.init(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        tableLoyalty.tableFooterView = UIView()
        lbl_linkCopy.text = URL_BASE
        let loyalityParam: [String:Any] = ["theme_id": APP_THEME_ID]
        getloyalityData(loyalityParam)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let param: [String:Any] = ["user_id": getID(),
                                       "theme_id": APP_THEME_ID]
            getOrderLists(param)
           
        }
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(self.scroll_view.contentOffset.y) >=  Int(self.scroll_view.contentSize.height - self.scroll_view.frame.size.height)) {
            if self.pageindex != self.lastindex {
                self.pageindex = self.pageindex + 1
                if self.historyArray.count != 0 {
                    let param: [String:Any] = ["user_id": getID(),
                                               "theme_id": APP_THEME_ID]
                    getOrderLists(param)
                }
            }
        }
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickCopyLink(_ sender: UIButton) {
        UIPasteboard.general.string = lbl_linkCopy.text
        showAlertMessage(titleStr: "Kidos", messageStr: "Link Copied!")
    }
    
    func getloyalityData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_LoyalityProgram, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let loyalityProgram = data["loyality-program"] as? [String:Any]{
                                    let loyality = loyalityProgram["loyality-program-title"] as? String
                                    self.lbl_getcashes.text = loyality
                                    let loyalitydis = loyalityProgram["loyality-program-description"] as? String
                                    self.lbl_getcashesDis.text = loyalitydis
                                    let yourcash = loyalityProgram["loyality-program-your-cash"] as? String
                                    self.lbl_getYourCash.text = yourcash
                                    let link = loyalityProgram["loyality-program-copy-this-link-and-send-to-your-friends"] as? String
                                    self.lbl_links.text = link
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    
    func loyalityReword(_ param: [String:Any]) {
        AIServiceManager.sharedManager.callPostApi(URL_LoyalityRewords, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let point = data["point"] as? String
                                self.lbl_pointCash.text = "+\(point ?? "0.00") \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)!)"
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    
    func getOrderLists(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_OrderHistory + "\(pageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.pageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.lastindex = lastpage
                                    self.historyArray.removeAll()
                                }
                                if let orderdata = data["data"] as? [[String:Any]]{
                                    self.historyArray.append(contentsOf: Order.init(orderdata).orderList)
                                    self.tableLoyalty.reloadData()
                                    self.height_loyality.constant = CGFloat(80 * self.historyArray.count)
                                }
                               
                                let parameters: [String:Any] = ["user_id": getID(),
                                                                "theme_id": APP_THEME_ID]
                                self.loyalityReword(parameters)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else {
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
}
extension LoyaltyProgramVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            print("no data")
        }else{
           return historyArray.count
            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        cell.isLoyalty = true
        let orderDetails = historyArray[indexPath.row]
        cell.order_id.text = orderDetails.order_id_string
        cell.lbl_Price.text = "\(orderDetails.amount!)"
        
        let dates = DateFormater.getFullDateStringFromString(givenDate: orderDetails.date!)
        cell.order_date.text = "Date:\(dates)"
        
        cell.lblStatus.isHidden = true
        cell.lblStatusTitle.isHidden = true
        cell.lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
        vc.order_id = historyArray[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
