//
//  SettingsVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import SDWebImage
import Alamofire

class SettingsVC: UIViewController {
    
    
    @IBOutlet weak var tableSettings: UITableView!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_cartCount: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    
    //MARK: - Variables
    var arrSettings = [SettingItem(title: "Edit your account information",subtit: "edit your account", image: "set_user"),
                       SettingItem(title: "Change your password",subtit: "Change Your Passowrd", image: "set_password"),
                       SettingItem(title: "Modify your address book entries",subtit: "Edit your address", image: "set_modify"),
                       SettingItem(title: "View your order history",subtit: "See your order history", image: "set_order"),
                       SettingItem(title: "Loyality Program",subtit: "Get cash by following people", image: "set_loyalty"),
                       SettingItem(title: "My Returns",subtit: "My return orders", image: "set_truck"),
                       SettingItem(title: "Logout", subtit: "Logout app", image: "ic_logout")]

    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableSettings.register(UINib.init(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        tableSettings.tableFooterView = UIView()
        heightTable.constant = CGFloat(60 * self.arrSettings.count)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let param: [String:Any] = ["user_id": getID(),
                                        "theme_id": APP_THEME_ID]
            self.getUserDetails(param)
        }
    }
    func openViewControllerWithName(name: String){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.parentVC = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickCart(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func onClickEditProfile(_ sender: Any){
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onclickUpdateProfile(_ sender: UIButton) {
        if IS_DEMO_MODE == true {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
        }else{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }else{
                self.imagePicker.delegate = self
                //        self.imagePicker.allowsEditing = true
                let alert = UIAlertController(title: "Select Your Profile", message: "Select image", preferredStyle: .actionSheet)
                let photoLibraryAction = UIAlertAction(title: "Photo library", style: .default) { (action) in
                    self.imagePicker.sourceType = .photoLibrary
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
                    self.imagePicker.sourceType = .camera
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                alert.addAction(photoLibraryAction)
                alert.addAction(cameraAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - API Functions
    
    
    func getUserDetails(_ param: [String:Any]) {
            
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_UserDetails, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String: Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                   let name = data["name"] as? String
                                    self.lbl_name.text = name
                                    let email = data["email"] as? String
                                    self.lbl_email.text = email
                                    let image = data["image"] as? String
                                    let endPoint = "\(URL_PaymentImage)/\(image!)"
                                    self.img_profile.sd_setImage(with: URL(string: endPoint))
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    print(error.localizedDescription)
                    HIDE_CUSTOM_LOADER()
                    break
                }
            }
          }
    }
    
    
    func getlogout(_ param: [String:Any]){
        
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_Logout, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let massage = data["message"] as? String
                                UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_USERID)
                                UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_APP_TOKEN)
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }

}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.selectionStyle = .none
        cell.configureCellWith(arrSettings[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = arrSettings[indexPath.row].title
        var vcName : String = ""
        if title == "Change your password"{
            vcName = "ChangePasswordVC"
        }else if title == "Modify your address book entries"{
            vcName = "AddressBookVC"
        }else if title == "View your order history"{
            vcName = "OrderHistoryVC"
        }else if title == "My Returns"{
            vcName = "MyReturnsVC"
        }else if title == "Loyality Program"{
            vcName = "LoyaltyProgramVC"
        }else if title == "Edit your account information"{
            vcName = "EditProfileVC"
        }else if title == "Logout"{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }
            else{
                let alertVC = UIAlertController(title: Bundle.main.displayName, message: "Are you sure to logout from this app?", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                    let param: [String:Any] = ["user_id": getID(),
                                               "theme_id": APP_THEME_ID]
                    self.getlogout(param)
                }
                let noAction = UIAlertAction(title: "No", style: .destructive)
                alertVC.addAction(noAction)
                alertVC.addAction(yesAction)
                self.present(alertVC,animated: true,completion: nil)
            }
        }
        if vcName != ""{
            self.openViewControllerWithName(name: vcName)
        }

    }
}
extension SettingsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.img_profile.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
        SHOW_CUSTOM_LOADER()
        let imageData = self.img_profile.image?.jpegData(compressionQuality: 0.0)!
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
            multipartFormData.append("\(getID())".data(using: String.Encoding.utf8)!, withName: "user_id")
            multipartFormData.append(APP_THEME_ID.data(using: String.Encoding.utf8)!, withName: "theme_id")
        }, to: URL_Updateuserimage, headers: headers).response(completionHandler: { response in
            switch response.result{
            case .success(_):
                HIDE_CUSTOM_LOADER()
                
                    if response.response?.statusCode == 200 {
                        print("OK. Done")
                        do {
                            if let jsonArray = try JSONSerialization.jsonObject(with: response.data!, options : .allowFragments) as? Dictionary<String,Any>
                            {
                               print(jsonArray) // use the json here
                                if let msg = jsonArray["message"] as? String{
                                    self.view.makeToast(msg, point: CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height/2), title: "Ticket Created Successfully!", image: nil) { didTap in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                                if let imgdata = jsonArray["data"] as? [String:Any]{
                                    let imgmassage = imgdata["message"] as? String
                                    UserDefaults.standard.set("\(URL_BASE)/\(imgmassage!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                                }
                            } else {
                                print("bad json")
                            }
                        } catch let error as NSError {
                            print(error)
                        }

                    }else if response.response?.statusCode == 406{
                        print("error")
                        DispatchQueue.main.sync{
                            let alert = UIAlertController(title: "Error", message:
                    "Person has not been set up.", preferredStyle: .alert)

                            alert.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
                                 DispatchQueue.main.async{
                                     HIDE_CUSTOM_LOADER()

    //                                self.progressUiView.isHidden = true
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }))

                            self.present(alert, animated: true)
                        }
                    }else{
                        print(response.response?.statusCode)
                    }


                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            })
        }
}

