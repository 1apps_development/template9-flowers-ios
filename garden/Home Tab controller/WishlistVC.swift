//
//  WishlistVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class WishlistVC: UIViewController {
    
    @IBOutlet weak var tableWish: UITableView!
    @IBOutlet weak var CartCount: UILabel!
    @IBOutlet weak var view_empty: UIView!
    @IBOutlet weak var lbl_nodatFound: UILabel!
    
    
    var pageindex = 1
    var lastindex = 0
    var product_id = Int()
    var varient_id = Int()
    var arrWishlist: [WishlistData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_empty.isHidden = true
        lbl_nodatFound.isHidden = true
        tableWish.register(UINib.init(nibName: "WishlistCell", bundle: nil), forCellReuseIdentifier: "WishlistCell")
        tableWish.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.CartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            pageindex = 1
            lastindex = 0
            let param: [String:Any] = ["user_id": getID(),
                                       "theme_id": APP_THEME_ID]
            getwihslistData(param)
        }
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickAddTocart(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - API Functions
    
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.CartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                
                                    let alertVC = UIAlertController(title: "Garden", message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                        let param: [String:Any] = ["user_id": getID(),
                                                                   "product_id": self.product_id,
                                                                   "variant_id": self.varient_id,
                                                                   "quantity_type": "increase",
                                                                   "theme_id": APP_THEME_ID]
                                        self.getQtyOfProduct(param)
                                    }
                                    let noAction = UIAlertAction(title: "No", style: .destructive)
                                    alertVC.addAction(noAction)
                                    alertVC.addAction(yesAction)
                                    self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func getwihslistData(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlistes, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        if let wishlistdata = data["data"] as? [[String:Any]]{
                                            self.arrWishlist = WishList.init(wishlistdata).wishlists
                                            self.tableWish.reloadData()
        //                                    self.wishlistTableheight.constant = CGFloat(self.arrWishlist.count * 80)
                                        }
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String:Any] = ["user_id": getID(),
                                                                   "theme_id": APP_THEME_ID]
                                        self.getwihslistData(param)
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
}

extension WishlistVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrWishlist.count == 0 {
            self.view_empty.isHidden = false
            self.lbl_nodatFound.isHidden = false
        }else{
            self.view_empty.isHidden = true
            self.lbl_nodatFound.isHidden = true
            return arrWishlist.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistCell") as! WishlistCell
        cell.configCell(arrWishlist[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductDescriptionVC") as! ProductDescriptionVC
        vc.productid = arrWishlist[indexPath.row].productid!
        vc.varientId = arrWishlist[indexPath.row].varientid!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cartaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
            success(true)
            let data = self.arrWishlist[indexPath.row]
            self.product_id = data.productid!
            self.varient_id = data.varientid!
            
            let param: [String:Any] = ["user_id": getID(),
                                       "product_id": data.productid!,
                                       "variant_id": data.varientid!,
                                       "qty": 1,
                                       "theme_id": APP_THEME_ID]
            self.addTocart(param)
        }
        cartaction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_shopping"),
            title: "", textColor: UIColor(red: 183/255, green: 212/255, blue: 197/255))
        cartaction.backgroundColor = UIColor.white
        
        return UISwipeActionsConfiguration(actions: [cartaction])
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
            success(true)
            let data = self.arrWishlist[indexPath.row]
            self.product_id = data.productid!
            self.varient_id = data.varientid!
            
            let param: [String:Any] = ["user_id": getID(),
                                       "product_id": data.productid!,
                                        "wishlist_type": "remove",
                                       "theme_id": APP_THEME_ID]
            self.getWishList(param)
        }
        deleteaction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_delete"),
            title: "", textColor: UIColor(red: 183/255, green: 212/255, blue: 197/255))
        deleteaction.backgroundColor = hexStringToUIColor(hex: "#B5C547")
        
        return UISwipeActionsConfiguration(actions: [deleteaction])
    }
}
