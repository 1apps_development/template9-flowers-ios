//
//  ProductDescriptionVC.swift
//  Plants
//
//  Created by mac on 29/04/22.
//

import UIKit
import ImageSlideshow
import iOSDropDown
import Alamofire

class DropDownCell: UITableViewCell {
    
    var onclickButtonClosure: (()->Void)?
    
    @IBOutlet weak var lbl_varititle: UILabel!
    @IBOutlet weak var lbl_colorName: UILabel!
    @IBOutlet weak var txt_colorSelect: DropDown!
    @IBOutlet weak var selectionColorView: UIView!
    
    @IBAction func onclickOnClick(_ sender: DropDown) {
        self.onclickButtonClosure?()
    }
}
class SizeCell: UITableViewCell {
    @IBOutlet weak var lbl_sizeSelect: UILabel!
    @IBOutlet weak var sizesCollectionView: UICollectionView!
}
class ProductDescriptionVC: UIViewController, ImageSlideshowDelegate {
    
    @IBOutlet weak var lbl_productname: UILabel!
    @IBOutlet weak var addToWishList: UILabel!
    @IBOutlet weak var TVvarient: UITableView!
    @IBOutlet weak var productImage: ImageSlideshow!
    @IBOutlet weak var cvImage: UICollectionView!
    @IBOutlet weak var TVdescription: UITableView!
    @IBOutlet weak var heightTvDescription: NSLayoutConstraint!
    @IBOutlet weak var cvReview: UICollectionView!
    @IBOutlet weak var CVproduct: UICollectionView!
    @IBOutlet weak var TVInstaction: UITableView!
    @IBOutlet weak var heightTVInsaction: NSLayoutConstraint!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_outofStock: UILabel!
    @IBOutlet weak var btn_addreview: UIButton!
    @IBOutlet weak var heightreview: NSLayoutConstraint!
    @IBOutlet weak var heightTVvarient: NSLayoutConstraint!
    @IBOutlet weak var cartLabel: UILabel!
    @IBOutlet weak var btn_wishlist: UIButton!
    @IBOutlet weak var segmentControl: HBSegmentedControl!
    @IBOutlet weak var btn_addTocart: UIButton!
    
    var productId: Int = 0
    var arrProductsDetails: [Products] = []
    var arrProducts:[Products] = []
    var arrSubProduct: [ProductImages] = []
    var arrProductImages: [SDWebImageSource] = []
    var arrVarients: [Varient] = []
    var disciptions: [OtherDis] = []
    var arrReviews: [Review] = []
    var arrInstraction: [ProductInstraction] = []
    var arrCatgeory: [categoryModel] = []
    var valueArray: [String] = []
    var SelectVarientArray: [String] = []
    var varientColor: [String] = []
    var cellHeight = Double()
    var isstock: String?
    var itemid: String?
    var catid: Int?
    var proCatid: Int?
    var Selected_Variant_Name = String()
    var stock: Int?
    var productid = Int()
    var varientId = Int()
    var selectedIndex: Int?
    var gestUserdata: [String:Any] = [:]
    var catgotyes: [String] = []
    var pageIndex = 1
    var lastIndex = 0
    var productpageindex = 1
    var productlastindex = 0
    var isinWishlist = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvImage.register(UINib(nibName: "MoreImageCell", bundle: nil), forCellWithReuseIdentifier: "MoreImageCell")
        TVdescription.register(UINib(nibName: "DescriptionCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        CVproduct.register(UINib(nibName: "PlantCell", bundle: nil), forCellWithReuseIdentifier: "PlantCell")
        TVInstaction.register(UINib(nibName: "InstractionCell", bundle: nil), forCellReuseIdentifier: "InstractionCell")
        cvReview.register(UINib(nibName: "ReviewCell", bundle: nil), forCellWithReuseIdentifier: "ReviewCell")
        self.lbl_outofStock.isHidden = true
        self.btn_addreview.isHidden = true
        self.cartLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            self.MakeAPICallforProductDetailGuest(["id":self.productId,"theme_id":APP_THEME_ID])
            self.addToWishList.isHidden = true
            self.btn_wishlist.isHidden = true
        }else{
            self.MakeAPICallforProductDetail(["id":self.productId,"theme_id":APP_THEME_ID])
            self.addToWishList.isHidden = false
            self.btn_wishlist.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
    }
    
    func setupSegment(){
        segmentControl.items = catgotyes
        segmentControl.font = UIFont.init(name: FONT_Outfit_Medium, size: 9)
        segmentControl.unselectedLabelColor = hexStringToUIColor(hex: "B4B4B4")
        segmentControl.selectedLabelColor = hexStringToUIColor(hex: "00160C")
        segmentControl.backgroundColor = .clear
        segmentControl.thumbColor = hexStringToUIColor(hex: "B5C547")
        segmentControl.selectedIndex = 0
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            let param: [String:Any] = ["theme_id": APP_THEME_ID,
                                       "maincategory_id": arrCatgeory[segmentControl.selectedIndex].categoryId!]
            getProductsGuestData(param)
        }else{
            let param: [String:Any] = ["theme_id": APP_THEME_ID,
                                       "maincategory_id": arrCatgeory[segmentControl.selectedIndex].categoryId!]
            getbestSellersData(param)
        }
        
        segmentControl.addTarget(self, action: #selector(segmentValueChanged(_:)), for: .valueChanged)
    }
    
    @IBAction func btnMore(_ sender: Any){
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_RETURNPOLICY)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func segmentValueChanged(_ sender: AnyObject) {
        self.selectedIndex = segmentControl.selectedIndex
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            let param: [String:Any] = ["theme_id": APP_THEME_ID,
                                       "maincategory_id": arrCatgeory[self.selectedIndex!].categoryId!]
            getProductsGuestData(param)
        }else{
            let param: [String:Any] = ["theme_id": APP_THEME_ID,
                                       "maincategory_id": arrCatgeory[self.selectedIndex!].categoryId!]
            getbestSellersData(param)
        }
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCartClick(_ sender : Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onclickAddReview(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "AddReviewVC") as! AddReviewVC
        vc.product_id = productId
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickAddtoCartClick(_ sender : Any){
        if self.stock == 0{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) != nil{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }else{
                let param: [String: Any] = ["product_id": productId,
                                            "user_id": getID(),
                                            "theme_id": APP_THEME_ID]
                self.notifyUser(param)
            }
        }else{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                    
                    var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                    var iscart = false
                    var cartindex = Int()
                    for i in 0..<Gest_array.count{
                        if Gest_array[i]["product_id"]! as! Int == gestUserdata["id"] as! Int && Gest_array[i]["variant_id"]! as! Int == self.varientId{
                            iscart = true
                            cartindex = i
                        }
                    }
                    if iscart == false{
                        let cartobj = ["product_id": gestUserdata["id"] as! Int,
                                       "image": gestUserdata["cover_image_url"] as! String,
                                       "name": gestUserdata["name"] as! String,
                                       "orignal_price": gestUserdata["original_price"] as! String,
                                       "discount_price": gestUserdata["discount_price"] as! String,
                                       "final_price": gestUserdata["final_price"] as! String,
                                       "qty": 1,
                                       "variant_id": varientId ] as [String : Any]
                        Gest_array.append(cartobj)
                        UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                        UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                        UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                        
                        let alert = UIAlertController(title: nil, message: "\(gestUserdata["name"] as! String) add successfully", preferredStyle: .actionSheet)
                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                            self.dismiss(animated: true)
                        }
                        
                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                        alert.addAction(photoLibraryAction)
                        alert.addAction(cameraAction)
                        alert.addAction(cancelAction)
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                        let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                            var cartsList = Gest_array[cartindex]
                            cartsList["qty"] = cartsList["qty"] as! Int + 1
                            Gest_array.remove(at: cartindex)
                            Gest_array.insert(cartsList, at: cartindex)
                            
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                        }
                        let noaction = UIAlertAction(title: "No", style: .destructive)
                        alert.addAction(yesaction)
                        alert.addAction(noaction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                self.cartLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": productId,
                                           "variant_id": varientId,
                                           "qty": 1,
                                           "theme_id": APP_THEME_ID]
                MakeApiCallForAddtoCart(param)
            }
        }
    }
    
    @IBAction func onclickWishList(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            if isinWishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": productId,
                                           "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                isinWishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": productId,
                                           "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                isinWishlist = false
                self.getWishList(param)
            }
        }
        if self.isinWishlist == false{
            self.btn_wishlist.setImage(UIImage(named: "ic_heart"), for: .normal)
        }else{
            self.btn_wishlist.setImage(UIImage(named: "ic_white_heart"), for: .normal)
        }
    }
    //MARK: - CustomFunction
    
    func imageSliderData() {
        self.productImage.slideshowInterval = 3.0
        self.productImage.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.productImage.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.productImage.pageIndicator = pageControl
        self.productImage.setImageInputs(self.arrProductImages)
        self.productImage.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.productImage.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.productImage.presentFullScreenController(from: self)
    }
    //MARK: - API
    func MakeAPICallforProductDetail(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_ProductDetail, params: param, headers) { response in
            switch response.result {
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["product_info"] as? [String:Any]{
                            self.gestUserdata = aData
                            let proTitle = aData["name"] as? String
                            self.lbl_productname.text = proTitle
                            let proPrice = aData["final_price"] as? String
                            self.productPrice.text = proPrice
                            let catgoryId = aData["category_id"] as? Int
                            self.catid = catgoryId
                            let wishlist = aData["in_whishlist"] as? Bool
                            self.isinWishlist = wishlist!
                            if let discription = aData["other_description_array"] as? [[String:Any]]{
                                self.disciptions = Discription.init(discription).discription
                                self.TVdescription.reloadData()
                                self.heightTvDescription.constant = CGFloat(self.disciptions.count * 220)
                            }
                            if let insractions = data["product_instruction"] as? [[String:Any]] {
                                self.arrInstraction = Instruction.init(insractions).instraction
                                self.TVInstaction.reloadData()
                                self.heightTVInsaction.constant = CGFloat(self.arrInstraction.count * 90)
                            }
                            let stock = aData["product_stock"] as? Int
                            self.stock = stock
                            let defaultvarientid = aData["default_variant_id"] as? Int
                            self.varientId = defaultvarientid!
                            if defaultvarientid == 0 {
                                if stock! <= 0 {
                                    self.lbl_outofStock.isHidden = false
                                    self.btn_addTocart.setTitle("Notify me when available", for: .normal)
                                }else{
                                    self.lbl_outofStock.isHidden = true
                                    self.btn_addTocart.setTitle("Add to cart", for: .normal)
                                }
                            }else{
                                if self.arrVarients.count != 0 {
                                    self.heightTVvarient.constant = CGFloat(self.arrVarients.count * 100)
                                    for varient in self.arrVarients {
                                        if varient.value?.count != 0 {
                                            self.SelectVarientArray.append(varient.value![0])
                                        }
                                    }
                                    let param: [String:Any] = ["product_id": self.productId,
                                                               "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                               "theme_id": APP_THEME_ID
                                    ]
                                    self.MakeAPICallforVarientStock(param)
                                }
                            }
                            let review = aData["is_review"] as? Int
                            if review == 1 {
                                self.btn_addreview.isHidden = true
                            }else if review == 0{
                                self.btn_addreview.isHidden = false
                            }else{
                                self.btn_addreview.isHidden = false
                            }
                        }
                        self.MakeAPICallforCategoriesList(["theme_id": APP_THEME_ID])
                        if let productReview = data["product_Review"] as? [[String:Any]]{
                            self.arrReviews = ProductReview.init(productReview).productreview
                            self.cvReview.reloadData()
                        }
                        if self.arrReviews.count != 0 {
                            self.heightreview.constant = 270.0
                        }else{
                            self.heightreview.constant = 0.0
                        }
                        self.arrProductImages.removeAll()
                        if let varient = data["product_image"] as? [[String:Any]]{
                            self.arrSubProduct = ProductSubImage.init(varient).subproductdata
                            let arrImage = self.arrSubProduct.map({$0.imagePath})
                            for producturl in arrImage{
                                let getproductURL = getImageFullURL("\(producturl!)")
                                let sdwebimage = SDWebImageSource(url: URL(string: "\(getproductURL)")!, placeholder: UIImage(named: ""))
                                self.arrProductImages.append(sdwebimage)
                            }
                            self.imageSliderData()
                            self.cvImage.reloadData()
                            if let typeVarients = data["variant"] as? [[String:Any]]{
                                self.arrVarients = ProductVarient.init(typeVarients).varients
                                self.TVvarient.reloadData()
                                self.heightTVvarient.constant = CGFloat(self.arrVarients.count * 120)
                            }
                            if self.arrVarients.count != 0 {
                                self.heightTVvarient.constant = CGFloat(self.arrVarients.count * 120)
                                for varient in self.arrVarients {
                                    if varient.value?.count != 0 {
                                        self.SelectVarientArray.append(varient.value![0])
                                    }
                                }
                                let param: [String:Any] = ["product_id": self.productId,
                                                           "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                           "theme_id": APP_THEME_ID]
                                self.MakeAPICallforVarientStock(param)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    
    func MakeAPICallforProductDetailGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_ProductDetailsGuest, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["product_info"] as? [String:Any]{
                            self.gestUserdata = aData
                            let proTitle = aData["name"] as? String
                            self.lbl_productname.text = proTitle
                            let proPrice = aData["final_price"] as? String
                            self.productPrice.text = proPrice
                            let catgoryId = aData["category_id"] as? Int
                            self.catid = catgoryId
                            if let discription = aData["other_description_array"] as? [[String:Any]]{
                                self.disciptions = Discription.init(discription).discription
                                self.TVdescription.reloadData()
                                self.heightTvDescription.constant = CGFloat(self.disciptions.count * 220)
                            }
                            if let insractions = data["product_instruction"] as? [[String:Any]] {
                                self.arrInstraction = Instruction.init(insractions).instraction
                                self.TVInstaction.reloadData()
                                self.heightTVInsaction.constant = CGFloat(self.arrInstraction.count * 60)
                            }
                            let stock = aData["product_stock"] as? Int
                            self.stock = stock
                            let defaultvarientid = aData["default_variant_id"] as? Int
                            if defaultvarientid == 0 {
                                if stock! <= 0 {
                                    self.lbl_outofStock.isHidden = false
                                    self.btn_addTocart.setTitle("Notify me when available", for: .normal)
                                }else{
                                    self.lbl_outofStock.isHidden = true
                                    self.btn_addTocart.setTitle("Add to cart", for: .normal)
                                }
                            }else{
                                if self.arrVarients.count != 0 {
                                    self.heightTVvarient.constant = CGFloat(self.arrVarients.count * 100)
                                    for varient in self.arrVarients {
                                        if varient.value?.count != 0 {
                                            self.SelectVarientArray.append(varient.value![0])
                                        }
                                    }
                                    let param: [String:Any] = ["product_id": self.productId,
                                                               "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                               "theme_id": APP_THEME_ID
                                    ]
                                    self.MakeAPICallforVarientStock(param)
                                }
                            }
                            let review = aData["is_review"] as? Int
                            if review == 1 {
                                self.btn_addreview.isHidden = true
                            }else if review == 0{
                                self.btn_addreview.isHidden = true
                            }else{
                                self.btn_addreview.isHidden = true
                            }
                        }
                        self.MakeAPICallforCategoriesList(["theme_id": APP_THEME_ID])
                        if let productReview = data["product_Review"] as? [[String:Any]]{
                            self.arrReviews = ProductReview.init(productReview).productreview
                            self.cvReview.reloadData()
                        }
                        if self.arrReviews.count != 0 {
                            //                            self.reteView.isHidden = false
                            self.heightreview.constant = 270.0
                        }else{
                            //                            self.reteView.isHidden = true
                            self.heightreview.constant = 0.0
                        }
                        self.arrProductImages.removeAll()
                        if let varient = data["product_image"] as? [[String:Any]]{
                            self.arrSubProduct = ProductSubImage.init(varient).subproductdata
                            let arrImage = self.arrSubProduct.map({$0.imagePath})
                            for producturl in arrImage{
                                let getproductURL = getImageFullURL("\(producturl!)")
                                let sdwebimage = SDWebImageSource(url: URL(string: "\(getproductURL)")!, placeholder: UIImage(named: ""))
                                self.arrProductImages.append(sdwebimage)
                            }
                            self.imageSliderData()
                            self.cvImage.reloadData()
                            if let typeVarients = data["variant"] as? [[String:Any]]{
                                self.arrVarients = ProductVarient.init(typeVarients).varients
                                self.TVvarient.reloadData()
                                self.heightTVvarient.constant = CGFloat(self.arrVarients.count * 120)
                            }
                            if self.arrVarients.count != 0 {
                                self.heightTVvarient.constant = CGFloat(self.arrVarients.count * 120)
                                for varient in self.arrVarients {
                                    if varient.value?.count != 0 {
                                        self.SelectVarientArray.append(varient.value![0])
                                    }
                                }
                                let param: [String:Any] = ["product_id": self.productId,
                                                           "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                           "theme_id": APP_THEME_ID
                                ]
                                self.MakeAPICallforVarientStock(param)
                            }
                            //                            self.CVProduct.reloadData()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforVarientStock(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_VarientStock, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }
                        let id = data["id"] as! Int
                        self.varientId = id
                        let varient = data["variant"] as! String
                        self.Selected_Variant_Name = varient
                        let stock = data["stock"] as! Int
                        self.stock = stock
                        if stock <= 0 {
                            self.lbl_outofStock.isHidden = false
                            self.btn_addTocart.setTitle("Notify me when available", for: .normal)
                        }else{
                            self.lbl_outofStock.isHidden = true
                            self.btn_addTocart.setTitle("Add to cart", for: .normal)
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func notifyUser(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_notifyUser, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeApiCallForAddtoCart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }                                }
                                    self.cartLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: "Garden", message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productId,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEME_ID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                            
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPIcallForAddreview(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func addrateing(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPICallforCategoriesList(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let dataArr = data["data"] as? [[String:Any]]{
                            
                            self.arrCatgeory = HomeCatSuperModel.init(dataArr).categories
                            
                            for i in 0..<self.arrCatgeory.count {
                                let object = self.arrCatgeory[i]
                                self.proCatid = object.categoryId!
                                if self.catid != self.proCatid {
                                    self.catgotyes.append(object.name!)
                                }
                            }
                            self.setupSegment()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func getProductsGuestData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_CategoryProductGuest + "\(productpageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.productpageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.productlastindex = lastpage
                                    self.arrProducts.removeAll()
                                }
                                if let catData = data["data"] as? [[String:Any]]{
                                    self.arrProducts.append(contentsOf: ProductsSuperModel.init(catData).product)
                                    self.CVproduct.reloadData()
                                }
                            }
                            
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    //                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func getbestSellersData(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_CategoryProduct + "\(pageIndex)", params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String: Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if self.pageIndex == 1 {
                                        let productLastpage = data["last_page"] as! Int
                                        self.pageIndex = productLastpage
                                        self.arrProducts.removeAll()
                                    }
                                    if let products = data["data"] as? [[String:Any]]{
                                        self.arrProducts.append(contentsOf: ProductsSuperModel.init(products).product)
                                        self.CVproduct.reloadData()
                                    }
                                    //                        let productTotal = data["total"] as! Int
                                    //                        self.lblProducts.text = "\(productTotal) products"
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        
                    }
                case let .failure(error):
                    print(error.localizedDescription)
                    HIDE_CUSTOM_LOADER()
                    break
                }
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.cartLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                
                                let alertVC = UIAlertController(title: "Garden", message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productId,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEME_ID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String: Any] = ["theme_id": APP_THEME_ID]
                                        self.getbestSellersData(param)
                                        self.CVproduct.reloadData()
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
}
extension ProductDescriptionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvImage{
            return arrSubProduct.count
        }else if collectionView == cvReview{
            return arrReviews.count
        }else if collectionView == CVproduct{
            return arrProducts.count
        }else{
            return self.valueArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cvImage{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreImageCell", for: indexPath) as! MoreImageCell
            cell.configureCell(arrSubProduct[indexPath.item])
            return cell
        }else if collectionView == cvReview{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            cell.lbl_reviewText.text = arrReviews[indexPath.item].reviewProduct
            cell.rateing.rating = arrReviews[indexPath.item].reteing!
            cell.lbl_rate.text = "\(arrReviews[indexPath.item].reteing!)"
            cell.lbl_clientName.text = arrReviews[indexPath.item].username
            return cell
        }else if collectionView == CVproduct{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlantCell", for: indexPath) as! PlantCell
            cell.configureCell(arrProducts[indexPath.item])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProducts[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let imagepath = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": imagepath,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.cartLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProducts[indexPath.row].id!
                    self.varientId = self.arrProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btnFav.isHidden = true
            }else{
                cell.btnFav.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btnFav.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else{
                cell.btnFav.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btnFav.tag = indexPath.row
            cell.btnFav.addTarget(self, action: #selector(onclickBtnWishlistFev), for: .touchUpInside)
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
            let data = self.valueArray[indexPath.item]
            cell.lbl_sizes.text = "\(data)"
            
            if data == self.SelectVarientArray[collectionView.tag]
            {
                cell.sizeView.backgroundColor = hexStringToUIColor(hex: "#0D1E1C")
                cell.sizeView.applyCornerRadius(8)
                cell.lbl_sizes.textColor = UIColor.white
            }
            else{
                cell.sizeView.backgroundColor = .clear
                cell.sizeView.applyBorder(.black, width: 1)
                cell.sizeView.applyCornerRadius(8)
                cell.lbl_sizes.textColor = UIColor.black
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvImage{
            return CGSize(width: 60, height: 60)
        }else if collectionView == CVproduct{
            return CGSize(width: collectionView.frame.width / 2, height: 350)
        }else if collectionView == cvReview{
            return CGSize(width: cvReview.frame.width, height: 269)
        }else{
            return CGSize(width: 90, height: 45)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvImage{
            self.productImage.setCurrentPage(indexPath.row, animated: true)
        }else if collectionView == CVproduct {
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductDescriptionVC") as! ProductDescriptionVC
                vc.productId = arrProducts[indexPath.row].id!
                navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductDescriptionVC") as! ProductDescriptionVC
                vc.productId = arrProducts[indexPath.row].id!
                navigationController?.pushViewController(vc, animated: true)
            }
        }else if collectionView == cvReview {
            printD("selected")
        }else{
            let data = arrVarients[collectionView.tag].value!
            self.SelectVarientArray.remove(at: collectionView.tag)
            self.SelectVarientArray.insert(data[indexPath.item], at: collectionView.tag)
            self.TVvarient.reloadData()
            let param: [String:Any] = ["product_id": self.productId,
                                       "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                       "theme_id": APP_THEME_ID
            ]
            self.MakeAPICallforVarientStock(param)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
        {
            if collectionView == self.CVproduct
            {
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.productpageindex != self.productlastindex{
                        self.productpageindex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            getProductsGuestData(param)
                        }
                    }
                }
            }
        }else{
            if collectionView == self.CVproduct
            {
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.pageIndex != self.lastIndex {
                        self.pageIndex += 1
                        if self.arrProducts.count != 0 {
                            let proParam: [String:Any] = ["theme_id": APP_THEME_ID]
                            getbestSellersData(proParam)
                        }
                    }
                }
            }
        }
    }
    @objc func onclickBtnWishlistFev(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
extension ProductDescriptionVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == TVvarient{
            return arrVarients.count
        }else if tableView == TVInstaction {
            return arrInstraction.count
        }else{
            return disciptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == TVvarient{
            let varientdata = arrVarients[indexPath.row]
            let varientType = varientdata.type
            if varientType == "dropdown"{
                let cell = TVvarient.dequeueReusableCell(withIdentifier: "DropDownCell") as! DropDownCell
                cell.lbl_varititle.text = arrVarients[indexPath.row].name!
                cell.lbl_colorName.text = arrVarients[indexPath.row].name
                cell.lbl_colorName.text = self.SelectVarientArray[indexPath.row]
                self.varientColor = arrVarients[indexPath.row].value!
                cell.onclickButtonClosure = {
                    cell.txt_colorSelect.textColor = .clear
                    cell.txt_colorSelect.checkMarkEnabled = false
                    cell.txt_colorSelect.resignFirstResponder()
                    cell.txt_colorSelect.optionArray = self.arrVarients[indexPath.row].value!
                    cell.txt_colorSelect.selectedRowColor = hexStringToUIColor(hex: "#B5C547")
                    cell.txt_colorSelect.rowBackgroundColor  = hexStringToUIColor(hex: "#B5C547")
                    cell.txt_colorSelect.didSelect(completion: { selected, index, id in
                        cell.lbl_colorName.text = selected
                        //                    cell.lbl_selectColor.text = self.SelectVarientArray[indexPath.row]
                        self.SelectVarientArray.remove(at: cell.txt_colorSelect.tag)
                        self.SelectVarientArray.insert(selected, at: cell.txt_colorSelect.tag)
                        self.TVvarient.reloadData()
                        let param: [String:Any] = ["product_id": self.productId,
                                                   "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                   "theme_id": APP_THEME_ID
                        ]
                        self.MakeAPICallforVarientStock(param)
                    })
                }
                return cell
            }else{
                let cell = TVvarient.dequeueReusableCell(withIdentifier: "SizeCell") as! SizeCell
                cell.lbl_sizeSelect.text = arrVarients[indexPath.row].name
                self.valueArray = arrVarients[indexPath.row].value!
                cell.sizesCollectionView.tag = indexPath.row
                cell.sizesCollectionView.delegate = self
                cell.sizesCollectionView.dataSource = self
                cell.sizesCollectionView.reloadData()
                return cell
            }
        }else if tableView == TVInstaction {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstractionCell") as! InstractionCell
            cell.configureCell(arrInstraction[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! DescriptionCell
            cell.lbl_destitle.text = disciptions[indexPath.row].title
            cell.lbl_desdiescrioption.setAttributedHtmlText(disciptions[indexPath.row].descrips!)
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == TVvarient{
            return 120
        }else if tableView == TVInstaction{
            return 60
        }else if tableView == TVdescription{
            return 220
        }
        return CGFloat()
    }
}
extension ProductDescriptionVC : FeedbackDelegate
{
    func refreshData(id: Int, rating_no: String, title: String, description: String) {
        let param: [String:Any] = ["id": id,
                                   "user_id": getID(),
                                   "rating_no": rating_no,
                                   "title": title,
                                   "description": description,
                                   "theme_id": APP_THEME_ID]
        self.addrateing(param)
    }
    
}
