//
//  BestsellersVC.swift
//  kiddos
//
//  Created by mac on 13/04/22.
//

import UIKit
import SDWebImage
import ImageSlideshow
import Alamofire

class BestsellersVC: UIViewController, ImageSlideshowDelegate {

    @IBOutlet weak var CVProducts: UICollectionView!
    @IBOutlet weak var img_Header_BG: UIImageView!
    @IBOutlet weak var btn_Header: UIButton!
    @IBOutlet weak var lbl_Header_label: UILabel!
    @IBOutlet weak var lbl_Header_title: UILabel!
    @IBOutlet weak var lbl_Header_description: UILabel!
    
    @IBOutlet weak var img_Banner_bg: UIImageView!
    @IBOutlet weak var lbl_Banner_header: UILabel!
    @IBOutlet weak var lbl_Banner_Title: UILabel!
    @IBOutlet weak var lbl_Banner_subtitle: UILabel!
    @IBOutlet weak var btn_Banner: UIButton!
    
    @IBOutlet weak var lbl_bestseller_title: UILabel!
    @IBOutlet weak var lbl_cartCount: UILabel!
//    @IBOutlet weak var lbl_Slider_heading: UILabel!
//    @IBOutlet weak var lbl_Slider_title: UILabel!
//    @IBOutlet weak var lbl_Slider_subtitle: UILabel!
    @IBOutlet weak var CVBestSeller: UICollectionView!
    @IBOutlet weak var lbl_Newsletter_title: UILabel!
    @IBOutlet weak var lbl_Newsletter_subtitle: UILabel!
    @IBOutlet weak var lbl_Newsletter_description: UILabel!
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var imageSlider2: ImageSlideshow!
    
    var arrCategories : [categoryModel] = []
    var arrProducts:[Products] = []
    var arrFeatured:[Products] = []
    var arrProduts1: [Products] = []
    var firstCatID = Int()
//    var arrFeaturedProducts: [FeaturedProduct] = []
    var arrSliderImage: [SDWebImageSource] = []
    var arrSliderImage2: [SDWebImageSource] = []
//    var arrStaticImages = [Images]()
    var pageIndex = 1
    var lastIndex = 0
    var trendingPageindex = 1
    var trendinglastindex = 0
    var tmpIndex : Any?
    var addwishlist = String()
    var removeWishlist = String()
    var productid = Int()
    var varientId = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        setupInitialUI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        let baseParam: [String:Any] = ["theme_id": APP_THEME_ID]
        MakeAPICallForBaseURL(baseParam)
    }
    //MARK: - Custom Function
    
    func setupInitialUI(){
        CVProducts.register(UINib.init(nibName: "PlantCell", bundle: nil), forCellWithReuseIdentifier: "PlantCell")
        CVBestSeller.register(UINib(nibName: "BestSellerCell", bundle: nil), forCellWithReuseIdentifier: "BestSellerCell")
    }
    func imageSliderData() {
        self.imageSlider.slideshowInterval = 3.0
        self.imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.imageSlider.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.imageSlider.pageIndicator = pageControl
        self.imageSlider.setImageInputs(self.arrSliderImage)
        self.imageSlider.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.imageSlider.addGestureRecognizer(recognizer)
    }
    func imageSliderDatatwo() {
        self.imageSlider2.slideshowInterval = 3.0
        self.imageSlider2.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.imageSlider2.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.imageSlider2.pageIndicator = pageControl
        self.imageSlider2.setImageInputs(self.arrSliderImage2)
        self.imageSlider2.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.imageSlider2.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.imageSlider.presentFullScreenController(from: self)
    }
    
   @objc func scrollToNextCell(){
           //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);

           //get current content Offset of the Collection view
           let contentOffset = CVBestSeller.contentOffset;

           //scroll to next cell
        CVBestSeller.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);

        
       }
    func startTimer() {
         Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true);
       }
    
    //MARK: - IBAction
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.parentVC = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickCart(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onclickSearch(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SpecialGiftsVC") as! SpecialGiftsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - API
    
    func MakeAPICallForBaseURL(_ param: [String:Any]) {
        AIServiceManager.sharedManager.callPostApi(URL_BASE, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let baseURL = data["base_url"] as? String{
                            URL_BASE = baseURL
                        }
                        if let imageURL = data["image_url"] as? String{
                            URL_BASEIMAGE = imageURL
                        }
                        if let paymenturl = data["payment_url"] as? String{
                            URL_PaymentImage = paymenturl
                        }
                        self.MakeAPICalltogetCurrency()
                        if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                            let guestArray = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                            UserDefaults.standard.set(guestArray.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                        }
                        self.MakeAPICallforLandingPage(["theme_id":APP_THEME_ID])
//                        self.startTimer()
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforLandingPage(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_LandingPage, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }
                        if let themeJson = data["them_json"] as? [String:Any]{
                            if let header = themeJson["homepage-header"] as? [String:Any]{
                                if let arrayImage = header["homepage-header-bg-img"] as? [Any]{
                                    self.arrSliderImage.removeAll()
                                    for i in 0..<arrayImage.count {
                                        let endPoint = arrayImage[i] as? String
                                        let sliderImage = getImageFullURL("\(endPoint!)")
                                        let sdwebimage = SDWebImageSource(url: URL(string: "\(sliderImage)")!, placeholder: UIImage(named: ""))
                                        self.arrSliderImage.append(sdwebimage)
                                    }
                                    self.imageSliderData()
                                }
//                                let imgUrl = getImageFullURL("\(endpoint!)")
//                                self.img_Header_BG.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "placeholder"))
                                self.lbl_Header_label.text = header["homepage-header-heading"] as? String
                                self.lbl_Header_title.text = header["homepage-header"] as? String
                                self.lbl_Header_description.text = header["homepage-header-sub-text"] as? String
                                self.btn_Header.setTitle(header["homepage-header-btn-text"] as? String, for: .normal)
                            }
                            
                            if let bestseller = themeJson["homepage-bestseller"] as? [String:Any]{
                                self.lbl_bestseller_title.text = bestseller["homepage-bestseller-heading"] as? String
                            }
                            
                            if let banner = themeJson["homepage-banner"] as? [String:Any]{
                                if let bannerImage = banner["homepage-banner-bg-img"] as? [Any]{
                                    self.arrSliderImage2.removeAll()
                                    for j in 0..<bannerImage.count {
                                        let endPoint = bannerImage[j] as? String
                                        let sliderImage = getImageFullURL("\(endPoint!)")
                                        let sdwebimage = SDWebImageSource(url: URL(string: "\(sliderImage)")!, placeholder: UIImage(named: ""))
                                        self.arrSliderImage2.append(sdwebimage)
                                    }
                                    self.imageSliderDatatwo()
                                }
                                self.lbl_Banner_header.text = banner["homepage-banner-heading"] as? String
                                self.lbl_Banner_Title.text = banner["homepage-banner-title-text"] as? String
                                self.lbl_Banner_subtitle.text = banner["homepage-banner-sub-text"] as? String
                                self.btn_Banner.setTitle(banner["homepage-banner-btn-text"] as? String, for: .normal)
                            }
                            
                            if let newsletter = themeJson["homepage-newsletter"] as? [String:Any]{
                                self.lbl_Newsletter_title.text = newsletter["homepage-newsletter-title-text"] as? String
                                self.lbl_Newsletter_subtitle.text = newsletter["homepage-newsletter-sub-text"] as? String
                                self.lbl_Newsletter_description.text = newsletter["homepage-newsletter-description"] as? String
                            }
                            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                                self.MakeAPICallforBestsellersGuest(["theme_id":APP_THEME_ID])
                                self.MakeAPICallforTrendingProductsGuest(["theme_id":APP_THEME_ID])
                            }else{
                                self.MakeAPICallforBestsellers(["theme_id": APP_THEME_ID])
                                self.MakeAPICallforTrending(["theme_id": APP_THEME_ID])
                            }
                            let param: [String:Any] = ["theme_id": APP_THEME_ID]
                            self.getextraURL(param)
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforBestsellersGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Bestseller_guest + "\(pageIndex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let adata = data["data"] as? [[String:Any]] {
                            if self.pageIndex == 1 {
                                let lastpage = data["last_page"] as! Int
                                self.lastIndex = lastpage
                                self.arrProducts.removeAll()
                            }
                            self.arrProducts.append(contentsOf: ProductsSuperModel.init(adata).product)
                            self.CVBestSeller.reloadData()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforBestsellers(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_Bestseller + "\(pageIndex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let aData = data["data"] as? [[String:Any]]{
                                    if self.pageIndex == 1 {
                                        let lastpage = data["last_page"] as! Int
                                        self.lastIndex = lastpage
                                        self.arrProducts.removeAll()
                                    }
                                    self.arrProducts.append(contentsOf: ProductsSuperModel.init(aData).product)
                                    self.CVBestSeller.reloadData()
                                    let count = json["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                            }
                        }else if status == 9{
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    //                    print(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }

    func MakeAPICalltogetCurrency(){
        AIServiceManager.sharedManager.callPostApi(URL_Currency, params: ["theme_id":APP_THEME_ID], nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let data = json["data"] as? [String:Any]{
                        let currency = data["currency"] as? String
                        UserDefaults.standard.set(currency, forKey: userDefaultsKeys.KEY_CURRENCY)
                        let currencyName = data["currency_name"] as? String
                        UserDefaults.standard.set(currencyName, forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
                    }
                    //                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func MakeAPICallforTrendingProductsGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_TrendingProductsGuest + "\(trendingPageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let aData = data["data"] as? [[String:Any]]{
                            if self.trendingPageindex == 1 {
                                let lastpage = data["last_page"] as! Int
                                self.trendinglastindex = lastpage
                                self.arrProduts1.removeAll()
                            }
                            self.arrProduts1.append(contentsOf: ProductsSuperModel.init(aData).product)
                            self.CVProducts.reloadData()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                
                                    let alertVC = UIAlertController(title: "Garden", message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                        let param: [String:Any] = ["user_id": getID(),
                                                                   "product_id": self.productid,
                                                                   "variant_id": self.varientId,
                                                                   "quantity_type": "increase",
                                                                   "theme_id": APP_THEME_ID]
                                        self.getQtyOfProduct(param)
                                    }
                                    let noAction = UIAlertAction(title: "No", style: .destructive)
                                    alertVC.addAction(noAction)
                                    alertVC.addAction(yesAction)
                                    self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPICallforTrending(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
          AIServiceManager.sharedManager.callPostApi(URL_TrendingProducts + "\(trendingPageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                    if self.trendingPageindex == 1 {
                                        let lastpage = data["last_page"] as! Int
                                        self.trendinglastindex = lastpage
                                        self.arrProduts1.removeAll()
                                    }
                                    self.arrProduts1.append(contentsOf: ProductsSuperModel.init(aData).product)
                                    self.CVProducts.reloadData()
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else {
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String: Any] = ["theme_id": APP_THEME_ID]
                                        self.MakeAPICallforBestsellers(param)
                                        self.MakeAPICallforTrending(param)
                                        self.CVBestSeller.reloadData()
                                        self.CVProducts.reloadData()
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getextraURL(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Extra, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let terms = data["terms"] as? String
                                UserDefaults.standard.set(terms, forKey: userDefaultsKeys.KEY_TERMS)
                                let contectus = data["contact_us"] as? String
                                UserDefaults.standard.set(contectus, forKey: userDefaultsKeys.KEY_CONTECTUS)
                                let returnpolicy = data["return_policy"] as? String
                                UserDefaults.standard.set(returnpolicy, forKey: userDefaultsKeys.KEY_RETURNPOLICY)
                                let insta = data["insta"] as? String
                                UserDefaults.standard.set(insta, forKey: userDefaultsKeys.KEY_INSTA)
                                let youtube = data["youtube"] as? String
                                UserDefaults.standard.set(youtube, forKey: userDefaultsKeys.KEY_YOUTUVBE)
                                let massanger = data["messanger"] as? String
                                UserDefaults.standard.set(massanger, forKey: userDefaultsKeys.KEY_MASSANGER)
                                let twitter = data["twitter"] as? String
                                UserDefaults.standard.set(twitter, forKey: userDefaultsKeys.KEY_TWITTER)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
}


extension BestsellersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
         return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CVBestSeller{
            return arrProducts.count
        }else{
            return arrProduts1.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CVBestSeller{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestSellerCell", for: indexPath) as! BestSellerCell
            cell.congifureCell(arrProducts[indexPath.item])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProducts[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let cartobj = ["product_id": data.id!,
                                           "image": data.covreimageURL!,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProducts[indexPath.row].id!
                    self.varientId = self.arrProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btn_wishlist.isHidden = true
                cell.lbl_addTowishList.isHidden = true
            }else{
                cell.btn_wishlist.isHidden = false
                cell.lbl_addTowishList.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btn_wishlist.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else if arrProducts[indexPath.row].isinwishlist == true{
                cell.btn_wishlist.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btn_wishlist.tag = indexPath.row
            cell.btn_wishlist.addTarget(self, action: #selector(onclickFevBtn), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlantCell", for: indexPath) as! PlantCell
            cell.configureCell(arrProduts1[indexPath.item])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProduts1[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let cartobj = ["product_id": data.id!,
                                           "image": data.covreimageURL!,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProduts1[indexPath.row].id!
                    self.varientId = self.arrProduts1[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProduts1[indexPath.row].id!,
                                               "variant_id": self.arrProduts1[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btnFav.isHidden = true
            }else{
                cell.btnFav.isHidden = false
            }
            if arrProduts1[indexPath.row].isinwishlist == false{
                cell.btnFav.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else{
                cell.btnFav.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btnFav.tag = indexPath.row
            cell.btnFav.addTarget(self, action: #selector(onclickBtnWishlistFev), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == CVBestSeller{
            return CGSize(width: collectionView.frame.width, height: 576)
        }else{
            return CGSize(width: collectionView.frame.width / 2, height: 350)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CVBestSeller{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductDescriptionVC") as! ProductDescriptionVC
            vc.productId = arrProducts[indexPath.item].id!
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductDescriptionVC") as! ProductDescriptionVC
            vc.productId = arrProduts1[indexPath.item].id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            if collectionView == CVBestSeller {
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.pageIndex != self.lastIndex{
                        self.pageIndex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforBestsellersGuest(param)
                        }
                    }
                }
            }else if collectionView == CVProducts {
                if indexPath.item == self.arrProduts1.count - 1 {
                    if self.trendingPageindex != self.trendinglastindex{
                        self.trendingPageindex += 1
                        if arrProduts1.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforTrendingProductsGuest(param)
                        }
                    }
                }
            }
        }else{
            if collectionView == CVBestSeller {
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.pageIndex != self.lastIndex{
                        self.pageIndex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforBestsellers(param)
                        }
                    }
                }
            }else if collectionView == CVProducts {
                if indexPath.item == self.arrProduts1.count - 1 {
                    if self.trendingPageindex != self.trendinglastindex{
                        self.trendingPageindex += 1
                        if arrProduts1.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforTrending(param)
                        }
                    }
                }
            }
        }
    }
    @objc func onclickFevBtn(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else if data.isinwishlist == true{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
    @objc func onclickBtnWishlistFev(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProduts1[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
