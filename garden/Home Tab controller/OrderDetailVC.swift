//
//  OrderDetailVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class OrderDetailVC: UIViewController {
    
    @IBOutlet weak var tableOrderDetail: UITableView!
    @IBOutlet weak var heightTableOrderDetail: NSLayoutConstraint!
    @IBOutlet weak var cvText: UICollectionView!
    @IBOutlet weak var lbl_orderid: UILabel!
    @IBOutlet weak var lbl_Billinfo: UILabel!
    @IBOutlet weak var lbl_dillInfo: UILabel!
    
    @IBOutlet weak var cupensView: ShadowView!
    @IBOutlet weak var image_payment: UIImageView!
    @IBOutlet weak var image_Delivary: UIImageView!
    
    @IBOutlet weak var lbl_subtotal: UILabel!
    @IBOutlet weak var lbl_finalTotal: UILabel!
    @IBOutlet weak var lbl_cupencode: UILabel!
    @IBOutlet weak var lbl_cupenAmmount: UILabel!
    @IBOutlet weak var btn_cancel: UIButton!
    
    @IBOutlet weak var view_cupen: UIView!
    @IBOutlet weak var height_ViewCupen: NSLayoutConstraint!
    
    @IBOutlet weak var view_empty: UIView!
    @IBOutlet weak var lbl_nodateFound: UILabel!
    @IBOutlet weak var lbl_massage: UILabel!
    
    @IBOutlet weak var lbl_TrackOrder: UIButton!
    
    @IBOutlet weak var viewCupen1: UIView!
    @IBOutlet weak var cupenCode_title: UILabel!
    @IBOutlet weak var subTotal_top: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_string2: UILabel!

    var cartListArray: [CartsList] = []
    var textinfoArray: [TextdataList] = []
    var orderStausText = String()
    var order_id = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableOrderDetail.register(UINib.init(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
        cvText.register(UINib(nibName: "TextDetailsCell", bundle: nil), forCellWithReuseIdentifier: "TextDetailsCell")
        tableOrderDetail.tableFooterView = UIView()
        lbl_massage.isHidden = true
        btn_cancel.isHidden = false
        lbl_TrackOrder.isHidden = false
        view_empty.isHidden = true
        lbl_nodateFound.isHidden = true
        view_cupen.isHidden = false
        cupenCode_title.isHidden = true
        cupensView.isHidden = true
        subTotal_top.constant = 8
        viewCupen1.isHidden = true
        height_ViewCupen.constant = 120
//        code.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["order_id": order_id,
                                   "theme_id": APP_THEME_ID]
        getOrderDetails(param)
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickcancelOrder(_ sender: Any){
        if btn_cancel.titleLabel?.text == "Return Order"{
            let param: [String:Any] = ["order_id": order_id,
                                       "order_status": "return",
                                       "theme_id": APP_THEME_ID]
            self.lbl_TrackOrder.isHidden = true
            getOrderStatusChange(param)
        }else{
            let param: [String:Any] = ["order_id": order_id,
                                       "order_status": "cancel",
                                       "theme_id": APP_THEME_ID]
            self.lbl_TrackOrder.isHidden = true
            getOrderStatusChange(param)
        }
    }
    @IBAction func onClickTrackOrder(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
        vc.Status = self.orderStausText
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getOrderDetails(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_OrderDetails, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let orderid = data["order_id"] as? String
                                self.lbl_orderid.text = "Order \(orderid!)"
                                let orderStatustitle = data["order_status_text"] as? String
                                self.orderStausText = orderStatustitle!
                                if self.orderStausText == "Delivered"{
                                    self.btn_cancel.setTitle("Return Order", for: .normal)
                                    self.lbl_massage.text = "Your order has Been Returned"
                                }else{
                                    self.btn_cancel.setTitle("Cancel Order", for: .normal)
                                    self.lbl_massage.text = "Your order has Been Canceled"
                                }
                                if let text = data["tax"] as? [[String:Any]]{
                                    self.textinfoArray = TextData.init(text).textdatalists
                                    self.cvText.reloadData()
                                }
                                if let product = data["product"] as? [[String:Any]]{
                                    self.cartListArray = ProductsList.init(product).cartsList
                                    self.tableOrderDetail.reloadData()
                                    self.heightTableOrderDetail.constant = CGFloat(self.cartListArray.count * 89)
                                }
                                if let cupenifo = data["coupon_info"] as? [String:Any] {
                                    self.viewCupen1.isHidden = false
                                    self.cupenCode_title.isHidden = false
                                    self.view_cupen.isHidden = false
                                    self.cupensView.isHidden = false
                                    self.subTotal_top.constant = 98
                                    self.height_ViewCupen.constant = 203
//                                    self.code.isHidden = false
                                    let cupencode = cupenifo["code"] as? String
                                    let disString = cupenifo["discount_string"] as? String
                                    let disString2 = cupenifo["discount_string2"] as? String
                                    self.lbl_cupencode.text = cupencode
                                    self.lbl_cupenAmmount.text = disString
                                    self.lbl_string2.text = disString2
                                }
                                if let billinginfo = data["billing_informations"] as? [String:Any] {
                                    let name  = billinginfo["name"] as? String
                                    let address = billinginfo["address"] as? String
                                    let email = billinginfo["email"] as? String
                                    let phone = billinginfo["phone"] as? String
                                    let country = billinginfo["country"] as? String
                                    let state = billinginfo["state"] as? String
                                    let city = billinginfo["city"] as? String
                                    let postcode = billinginfo["post_code"] as? String
                                    
                                    self.lbl_Billinfo.text = "\(name!) \n \(address!) \(city!) \(state!) \(country!) - \(postcode!) \n Phone: \(phone!) \n Email: \(email!)"
                                }
                                if let delivaryInfo = data["delivery_informations"] as? [String:Any] {
                                    let name  = delivaryInfo["name"] as! String
                                    let email = delivaryInfo["email"] as! String
                                    let phone = delivaryInfo["phone"] as! String
                                    let country = delivaryInfo["country"] as! String
                                    let state = delivaryInfo["state"] as! String
                                    let city = delivaryInfo["city"] as! String
                                    let postcode = delivaryInfo["post_code"] as! String
                                    let address = delivaryInfo["address"] as! String
                                    if address == "" {
                                        self.lbl_dillInfo.text = "-"
                                    }else{
                                        self.lbl_dillInfo.text = "\(name) \n \(address) \(city) \(state) \(country) - \(postcode) \n Phone: \(phone) \n Email: \(email)"
                                    }
                                }
                                let paymentimage = data["paymnet"] as? String
                                self.image_payment.sd_setImage(with: URL(string: "\(URL_PaymentImage)/\(paymentimage!)"), placeholderImage: UIImage(named: ""))
                                let delivaryImage = data["delivery"] as? String
                                self.image_Delivary.sd_setImage(with: URL(string: getImageFullURL("\(delivaryImage!)")), placeholderImage: UIImage(named: ""))
                                let subtotal = data["sub_total"] as? String
                                self.lbl_subtotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(subtotal!)"
                                let finalTotal = data["final_price"] as? String
                                self.lbl_finalTotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(finalTotal!)"
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func getOrderStatusChange(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_StatusChange, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                self.lbl_massage.isHidden = false
                                self.btn_cancel.isHidden = true
                                let massage = data["message"] as? String
                                print(massage)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func addrateing(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else {
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
}
extension OrderDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.cartListArray.count == 0 {
            self.view_empty.isHidden = false
            self.lbl_nodateFound.isHidden = false
        }else{
            self.view_empty.isHidden = true
            self.lbl_nodateFound.isHidden = true
            return cartListArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        cell.lbl_proName.text = cartListArray[indexPath.row].name
        let productimgURL = getImageFullURL("\(cartListArray[indexPath.row].image!)")
        cell.img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            cell.img_product.image = image
        }
        let price = Double(cartListArray[indexPath.row].finalprice!)! * Double(cartListArray[indexPath.row].qty!)
        cell.lbl_price.text = "1x\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY) ?? "$")\(cartListArray[indexPath.row].orignalprice!)"
        cell.lbl_proPrice.text = "\(price)"
        cell.lbl_qty.text = "\(cartListArray[indexPath.row].qty!)"
        cell.lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension OrderDetailVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textinfoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.cvText.dequeueReusableCell(withReuseIdentifier: "TextDetailsCell", for: indexPath) as! TextDetailsCell
        cell.lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(textinfoArray[indexPath.row].taxAmoountString!)"
        cell.lbl_textTitle.text = textinfoArray[indexPath.row].taxString
        return cell
    }
    
}
extension OrderDetailVC : FeedbackDelegate{
    func refreshData(id: Int, rating_no: String, title: String, description: String) {
        let param: [String:Any] = ["id": id,
                                   "user_id": getID(),
                                   "rating_no": rating_no,
                                   "title": title,
                                   "description": description,
                                   "theme_id": APP_THEME_ID]
        self.addrateing(param)
    }
}
