//
//  MenuItem.swift
//  Taskly
//
//  Created by mac on 10/03/22.
//

import Foundation
import UIKit


class SettingItem:Codable{
    var title: String
    var subtitle: String
    var image: String
    init(title: String,subtit: String, image:String) {
        self.title = title
        self.image = image
        self.subtitle = subtit
    }
}
class CategoryItem: Codable
{
    var title: String
    var image: String
    init(title: String, img: String) {
        self.title = title
        self.image = img
    }

}

//Expandable Object
struct WrapperObject {
    var header : HeaderObject
    var listObject : [ObjectDetail]
}

struct HeaderObject {
    var id : String
    var isOpen : Bool
}

struct ObjectDetail {
    var id : String
    var detailInfo : String
}
