//
//  global.swift
//
//
//  Created by Apple on 08/02/21.
//
import UIKit
import Foundation
import Toast_Swift
//import Localizr_swift
import Alamofire
import StoreKit
import SDWebImage


@IBDesignable class RotatableView: UIImageView {

    @objc @IBInspectable var rotationDegrees: Float = 0 {
        didSet {
//            print("Setting angle to \(rotationDegrees)")
            let angle = NSNumber(value: rotationDegrees / 180.0 * Float.pi)
            layer.setValue(angle, forKeyPath: "transform.rotation.z")
        }
    }
}
//Object keys - Parameters name|| REquest, Response parameter
let K_Theme_Color = "theme_color"
let K_Banner = "banner"
let K_Banner_1 = "banner_1"
let K_Banner_2 = "banner_2"
let K_Title = "title"
let K_Mobile_Description = "mobile_description"
let K_Mobile = "mobile"
let K_OTP = "otp"
let K_Category = "category"
let K_Id = "id"
let K_Code = "code"
let K_Name = "name"
let K_Slug = "slug"
let K_Parent = "parent"
let K_Description = "description"
let K_Short_Description = "short_description"
let K_Display = "display"
let K_Category_Slider_Image = "category_slider_image"
let K_Image = "image"
let K_Date_Created = "date_created"
let K_Date_Created_GMT = "date_created_gmt"
let K_Date_Modified = "date_modified"
let K_Date_Modified_GMT = "date_modified_gmt"
let K_Src = "src"
let K_Alt = "alt"
let K_Menu_Order = "menu_order"
let K_Count = "count"
let K_Yoast_Head = "yoast_head"
let K_Cat_Color = "cat_color"
let K_Details_Image = "details_image"
let K_Long_Description = "long_description"
let K__Links = "_links"
let K_Self = "self"
let K_Href = "href"
let K_Collection = "collection"
let K_OrderNo = "order_no"
let K_Section_Position = "section_position"
let K_Trending_Stories = "tranding_stories"
let K_PermaLink = "permalink"
let K_MetaData = "meta_data"
let K_Key = "key"
let K_Value = "value"
let K_Type = "type"
let K_Status = "status"
let K_Featured = "featured"
let K_Catalog_Visibility = "catalog_visibility"
let K_Sku = "sku"
let K_Price = "price"
let K_Regular_Price = "regular_price"
let K_Sale_Price = "sale_price"
let K_Price_HTML = "price_html"
let K_ON_Sale = "on_sale"
let K_Purchasable = "purchasable"
let K_Total_Sales = "total_sales"
let K_Virtual = "virtual"
let K_Downloadable = "downloadable"
let K_Stock_Quantity = "stock_quantity"
let K_Quantity = "quantity"
let K_Original_Price = "original_price"
let K_Wishlist_Id = "wishlist_id"
let K_Stock_Status = "stock_status"
let K_Weight = "weight"
let K_Dimensions = "dimensions"
let K_Reviews_Allowed = "reviews_allowed"
let K_Length = "length"
let K_Width = "width"
let K_Height = "height"
let K_Average_Ratings = "average_rating"
let K_Rating_Count = "rating_count"
let K_Related_Ids = "related_ids"
let K_Tags = "tags"
let K_Images = "images"
let K_Parent_Id = "parent_id"
let K_Purchase_Note = "purchase_note"
let K_Categories = "categories"
let K_Top_Rated = "top_rated"
let K_Product = "product"
let K_Products = "products"
let K_Min_Price = "min_price"
let K_Max_Price = "max_price"
let K_Search = "search"
let K_Order = "order"
let K_Order_By = "orderby"
let K_Hot_Product = "hot_product"
let K_Username = "username"
let K_Email = "email"
let K_Password = "password"
let K_Phone = "phone"
let K_Avtar = "avatar"
let K_Token = "token"
let K_Data = "data"
let K_User_Display_Name = "user_display_name"
let K_User_Email = "user_email"
let K_User_Id = "user_id"
let K_User_Nicename = "user_nicename"
let K_User_Role = "user_role"
let K_Discount = "discount"
let K_Discount_Tax = "discount_tax"
let K_Review = "review"
let K_Question_Answer = "question_answer"
let K_FirstName = "first_name"
let K_LastName = "last_name"
let K_DisplayName = "display_name"
let K_CartBadge = "cartBadgeValue"
let K_Address1  = "address_1"
let K_Address2 = "address_2"
let K_City = "city"
let K_State = "state"
let K_Postcode = "postcode"
let K_Country = "country"
let K_Billing = "billing"
let K_Shipping = "shipping"
let K_Line_Items = "line_items"
let K_ProductId = "product_id"
let K_Coupon_Apply = "coupon_apply"
let K_Amount = "amount"
let K_Customer_Note = "customer_note"
let K_COD_Charge = "cod_charge"

let CART_BADGE = "cartitems"

var topSafeArea: CGFloat = 0.0
var bottomSafeArea: CGFloat = 0.0


/*public var langArr:[[String:Any]] = [["code":"en","lang":"english".localized()],
           ["code":"ja","lang":"japanese".localized()],
           //["code":"ar","lang":"arabic".localized()],
           ["code":"hi","lang":"hindi".localized()]]*/

var keyWindow = UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .compactMap({$0 as? UIWindowScene})
        .first?.windows
        .filter({$0.isKeyWindow}).first
var MainstoryBoard = UIStoryboard(name: "Main", bundle: nil)

public func setValueToUserDefaults(value: AnyObject,key: String){
    let defaults = UserDefaults.standard
    let MyData = NSKeyedArchiver.archivedData(withRootObject: value)
    defaults.setValue(MyData, forKey: key)
}
func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
}

public func readValueFromUserDefaults(_ key: String) -> AnyObject{
    
    let defaults = UserDefaults.standard
    let data = defaults.object(forKey: key)
    let object = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
    return object as AnyObject
}

public func getToken() -> String{
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil{
        let dic = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as! String
        return dic
    }
    return ""
}
public func getID() -> Int{
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USER_REG_DATA) != nil{
        let dic = readValueFromUserDefaults(userDefaultsKeys.KEY_USER_REG_DATA) as! [String:Any]
        if dic["id"] as? Int != nil{
            return dic["id"] as! Int
        }
    }
    return 0
}
public func isUserLoggedIn() -> Bool{
    if UserDefaults.standard.bool(forKey: userDefaultsKeys.KEY_IS_USER_LOGGED_IN) == true{
        return true
    }
    return false
}
public func getUserData() -> NSObject{
    var userData = NSObject()
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USER_REG_DATA) != nil{
        if let udata = readValueFromUserDefaults(userDefaultsKeys.KEY_USER_REG_DATA) as? [String:Any]{
//            userData = userDataModel.init(udata)
        }
    }
    return userData
}

public func getCurrencySymbol() -> String{
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CURRENCY) != nil{
        return UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CURRENCY) as! String
    }
    return ""
}
public func getCurrencyName() -> String{
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CURRENCY_NAME) != nil{
        return UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_CURRENCY_NAME) as! String
    }
    return ""
}

public func getFCMToken() -> String{
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_FCM_TOKEN) != nil{
        let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_FCM_TOKEN)
        return token as! String
    }
    return ""
}


//public func getUserID() -> Int{
//
//    if let udata = getUserData() as? userDataModel{
//        return udata.user_Id!
//    }
//    return 0
//}

public func getMyAccountData() -> NSObject{
    var userMyAccData = NSObject()
    if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_MY_ACCOUNT_DATA) != nil{
        if let udata = readValueFromUserDefaults(userDefaultsKeys.KEY_MY_ACCOUNT_DATA) as? [String:Any]{
//            userMyAccData = userInfoMyAccountModel.init(udata)
        }
    }
    return userMyAccData
}

func printD(_ items: Any..., separator: String = " ", terminator: String = "\n"){
    print(items)
}

func calcDeductPercentage(_ total: Double, _ perc: Double) ->  Double{
    let perAmt = (total * perc) / 100
    return total - perAmt
}



extension UIColor{
    class func appUnselectedPurpleColor() -> UIColor{
        return hexStringToUIColor(hex: "D8D6E9")
    }
}

class CustomTapGestureRecognizer: UITapGestureRecognizer {
    var params: Dictionary<String, Any> = [:]
}

class PassableUIButton: UIButton{
    var params: Dictionary<String, Any>
    override init(frame: CGRect) {
        self.params = [:]
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        self.params = [:]
        super.init(coder: aDecoder)
    }
}

func utcToLocal(dateStr: String) -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    if let date = dateFormatter.date(from: dateStr) {
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
        return dateFormatter.string(from: date)
    }
    return nil
}
func showAlertMessage(titleStr:String, messageStr:String) -> Void {
    DispatchQueue.main.async {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
        UIApplication.shared.windows[0].rootViewController!.present(alert, animated: true, completion: nil)
    }
}
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
func setView(view: UIView, hidden: Bool) {
    UIView.transition(with: view, duration: 0.4, options: .curveEaseInOut, animations: {
        view.isHidden = hidden
    })
}
//func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
//    DispatchQueue.global().async { //1
//        let asset = AVAsset(url: url) //2
//        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
//        avAssetImageGenerator.appliesPreferredTrackTransform = true //4
//        let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
//        do {
//            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
//            let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
//            DispatchQueue.main.async { //8
//                completion(thumbNailImage) //9
//            }
//        } catch {
//            print(error.localizedDescription) //10
//            DispatchQueue.main.async {
//                completion(nil) //11
//            }
//        }
//    }
//}

func setImageFromUrlOrName(_ url: String, _ Fullname: String, _ sizeWH: CGFloat, _ isBig: Bool = false) -> UIImage{
    let imageview = UIImageView()
    if url != ""{
        imageview.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder"))
        return imageview.image!
    }else{
        let fullname = Fullname.components(separatedBy: " ")
        var nam: String = "", sname:String = ""
        if  let name = fullname[0] as? String {
            nam = name
        }
        if fullname.count > 1{
            if  let snam = fullname[1] as? String {
                sname = snam
            }
        }
        imageview.image = createAvatarWithInitials(name: nam, surname: sname, size: sizeWH, isBig)
        return imageview.image!
    }

}

func estimatedLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {

    let size = CGSize(width: width, height: 1000)

    let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)

    let attributes = [NSAttributedString.Key.font: font]

    let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height

    return rectangleHeight
}
func createAvatarWithInitials(name: String, surname: String, size: CGFloat, _ isBig:Bool = false) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(CGSize(width: size, height: size), false, 0)
    let ctx = UIGraphicsGetCurrentContext()!

    // Draw an opague circle
    UIColor.random.setFill()
    let circle = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: size, height: size))
    circle.fill()

    // Setup text
    
    let font = UIFont.init(name: FONT_ProximaNova_Regular, size: isBig == true ? 25 : 12)
    var text = ""

    if surname != ""{
        text = "\(name.first!)\(surname.first!)".uppercased()
    }else{
        text = "\(name.first!)".uppercased()
    }
    let textSize = (text as NSString).size(withAttributes: [ .font: font! ])

    // Erase text
    ctx.setBlendMode(.clear)
    let textRect = CGRect(x: (size - textSize.width) / 2, y: (size - textSize.height) / 2, width: textSize.width, height: textSize.height)
    (text as NSString).draw(in: textRect, withAttributes: [ .font: font!])

    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    return image
}
extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}

@IBDesignable
class RoundedCornerButton: UIButton {

    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
    }

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor : UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }

}
@IBDesignable
class RoundedCornerButtonWithShadow: UIButton {
    /// The shadow color of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadowColor: UIColor = UIColor.black {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow offset of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0.0, height: 2) {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow radius of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadowRadius: CGFloat = 4.0 {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow opacity of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadowOpacity: Float = 0.5 {
        didSet {
            self.updateProperties()
        }
    }


    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
        
    }

    @IBInspectable var cornerRadius: CGFloat = 8 {
        didSet {
            self.updateProperties()
        }
    }
    fileprivate func updateProperties() {
        self.layer.cornerRadius = self.cornerRadius
        self.layer.shadowColor = self.shadowColor.cgColor
        self.layer.shadowOffset = self.shadowOffset
        self.layer.shadowRadius = self.shadowRadius
        self.layer.shadowOpacity = self.shadowOpacity
    }

}

@IBDesignable
class RoundedCornerView: UIView {

    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
    }

    @IBInspectable var cornerRadius: CGFloat = 8 {
        didSet {
            self.layer.cornerRadius = cornerRadius
//            self.layer.shadowOffset = CGSize.init(width: 0, height: 2)
//            self.layer.shadowColor = UIColor.gray.cgColor
//            self.layer.shadowRadius = 6
//            self.layer.shadowOpacity = 0.7
        }
    }
}

@IBDesignable
class BottomShadowView: UIView {

    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
    }

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.shadowOffset = CGSize.init(width: 0, height: 2)
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowRadius = 5
            self.layer.shadowOpacity = 0.7
        }
    }
}

@IBDesignable
class RoundableView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 0.0{
        didSet{
            self.applyCornerRadius()
        }
    }

    @IBInspectable var borderColor : UIColor = UIColor.clear{
        didSet{
            self.applyCornerRadius()
        }
    }

    @IBInspectable var borderWidth : Double = 0{
        didSet{
            self.applyCornerRadius()
        }
    }

    @IBInspectable var circular : Bool = false{
        didSet{
            self.applyCornerRadius()
        }
    }

    func applyCornerRadius()
    {
        if(self.circular) {
            self.layer.cornerRadius = self.bounds.size.height/2
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = CGFloat(self.borderWidth)
        }else {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = CGFloat(self.borderWidth)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyCornerRadius()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        applyCornerRadius()
    }

}


@IBDesignable
class RoundableImageView: UIImageView {

    @IBInspectable var cornerRadius : CGFloat = 0.0{
        didSet{
            self.applyCornerRadius()
        }
    }

    @IBInspectable var borderColor : UIColor = UIColor.clear{
        didSet{
            self.applyCornerRadius()
        }
    }

    @IBInspectable var borderWidth : Double = 0{
        didSet{
            self.applyCornerRadius()
        }
    }

    @IBInspectable var circular : Bool = false{
        didSet{
            self.applyCornerRadius()
        }
    }

    func applyCornerRadius()
    {
        if(self.circular) {
            self.layer.cornerRadius = self.bounds.size.height/2
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = CGFloat(self.borderWidth)
        }else {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = CGFloat(self.borderWidth)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyCornerRadius()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        applyCornerRadius()
    }

}
@IBDesignable
class CircleImageViewShadow: UIImageView {

    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
    }

    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.shadowOffset = CGSize.init(width: 0, height: 2)
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowRadius = 6
            self.layer.shadowOpacity = 0.7
        }
    }
}

extension NSMutableAttributedString {
    var fontSize:CGFloat { return 14 }
    var boldFont:UIFont { return UIFont(name: FONT_ProximaNova_Bold, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: FONT_ProximaNova_Regular, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    func Boldunderlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  boldFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
open class DGCollectionViewLeftAlignFlowLayout: UICollectionViewFlowLayout {
    var delegate: UICollectionViewDelegateFlowLayout? {
        return self.collectionView?.delegate as? UICollectionViewDelegateFlowLayout
    }

    override open func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributesCollection = super.layoutAttributesForElements(in: rect) else {
            return nil
        }

        var updatedAttributes = [UICollectionViewLayoutAttributes]()
        attributesCollection.forEach({ (originalAttributes) in
            guard originalAttributes.representedElementKind == nil else {
                updatedAttributes.append(originalAttributes)
                return
            }

            if let updatedAttribute = self.layoutAttributesForItem(at: originalAttributes.indexPath) {
                updatedAttributes.append(updatedAttribute)
            }
        })

        return updatedAttributes
    }

    override open func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes else {
            return nil
        }

        guard let collectionView = self.collectionView else {
            return attributes
        }

        let firstInSection: Bool = indexPath.item == 0
        guard !firstInSection else {
            let section = attributes.indexPath.section
            let x = self.delegate?.collectionView?(collectionView, layout: self, insetForSectionAt: section).left ?? self.sectionInset.left
            attributes.frame.origin.x = x
            return attributes
        }

        let previousAttributes = self.layoutAttributesForItem(at: IndexPath(item: indexPath.item - 1, section: indexPath.section))
        let previousFrame: CGRect = previousAttributes?.frame ?? CGRect()
        let firstInRow = previousAttributes?.center.y != attributes.center.y

        guard !firstInRow else {
            let section = attributes.indexPath.section
            let x = self.delegate?.collectionView?(collectionView, layout: self, insetForSectionAt: section).left ?? self.sectionInset.left
            attributes.frame.origin.x = x
            return attributes
        }

        let interItemSpacing: CGFloat = (collectionView.delegate as? UICollectionViewDelegateFlowLayout)?
            .collectionView?(collectionView, layout: self, minimumInteritemSpacingForSectionAt: indexPath.section) ?? self.minimumInteritemSpacing

        let x = previousFrame.origin.x + previousFrame.width + interItemSpacing
        attributes.frame = CGRect(x: x,
                                  y: attributes.frame.origin.y,
                                  width: attributes.frame.width,
                                  height: attributes.frame.height)

        return attributes
    }
}

extension UIView {

    func takeScreenshot() -> UIImage {

        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)

        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)

        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}

let APP_LIST_FONTS = [
       "AlexBrush-Regular",
       "Always  forever",
       "AmaticSC-Regular",
       "Blackout-Sunrise",
       "Blackout-TwoAM",
       "Brain Flower",
       "CanterBold3D",
       "CanterLight",
       "Chunkfive",
       "GeosansLight",
       "GreatVibes-Regular",
       "Haymaker",
       "Intro-Inline",
       "JennaSue",
       "Joyful Juliana",
       "JUICE Bold Italic",
       "knewave",
       "Langdon",
       "Lato-Bold",
       "Lavanderia-Delicate",
       "Lavanderia-Regular",
       "Lavanderia-Sturdy",
       "LeagueGothic-Italic",
       "LeagueGothic-Regular",
       "Learning Curve Pro",
       "Liberator",
       "Lobster 1.4",
       "Lumberjack New",
       "Market Deco",
       "Masana-2Grata",
       "Masana-1Propia",
       "Mathlete-Bulky",
       "Metropolis 1920",
       "Mission-Script",
       "Monoton-Regular",
       "Museo Slab",
       "OstrichSans-Bold",
       "Ostrich Sans Rounded",
       "QuicksandDash-Regular",
       "Quirky Nots",
       "Ribbon",
       "Sequi",
       "Vagabond Revised",
       "Venera900",
       "Vetka",
       "Wisdom Script AI",
       "Mulish-Bold",
       "Mulish-Regular",
       "Mulish-SemiBold",
       "Poppins-Bold",
       "Poppins-Regular",
       "Poppins-SemiBold"
]

extension UITextField{
    //Horizontal gradient color code
        func drawHGradientColor(in rect: CGRect, colors: [CGColor]) -> UIColor? {
           let currentContext = UIGraphicsGetCurrentContext()
           currentContext?.saveGState()
           defer { currentContext?.restoreGState() }

           let size = rect.size
           UIGraphicsBeginImageContextWithOptions(size, false, 0)
           guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                           colors: colors as CFArray,
                                           locations: nil) else { return nil }

           let context = UIGraphicsGetCurrentContext()
           context?.drawLinearGradient(gradient,
                                       start: CGPoint.zero,
                                       end: CGPoint(x: size.width, y: 0),
                                       options: [])
           let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           guard let image = gradientImage else { return nil }
           return UIColor(patternImage: image)
       }
}
extension String {
 
    func index(at position: Int, from start: Index? = nil) -> Index? {
        let startingIndex = start ?? startIndex
        return index(startingIndex, offsetBy: position, limitedBy: endIndex)
    }
 
    func character(at position: Int) -> Character? {
        guard position >= 0, let indexPosition = index(at: position) else {
            return nil
        }
        return self[indexPosition]
    }
}

//collectionflow reverse

class InvertedCollectionViewFlowLayout: UICollectionViewFlowLayout {

    // inverting the transform in the layout, rather than directly on the cell,
    // is the only way I've found to prevent cells from flipping during animated
    // cell updates

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attrs = super.layoutAttributesForItem(at: indexPath)
        attrs?.transform = CGAffineTransform(scaleX: 1, y: -1)
        return attrs
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attrsList = super.layoutAttributesForElements(in: rect)
        if let list = attrsList {
            for i in 0..<list.count {
                list[i].transform = CGAffineTransform(scaleX: 1, y: -1)
            }
        }
        return attrsList
    }
}

class baseVC: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            topSafeArea = window.safeAreaInsets.top
            bottomSafeArea = window.safeAreaInsets.bottom
        }else{
            topSafeArea = view.safeAreaInsets.top
            bottomSafeArea = view.safeAreaInsets.bottom
        }
    }
}
extension UIImage {
    func parseQR() -> [String] {
        guard let image = CIImage(image: self) else {
            return []
        }

        let detector = CIDetector(ofType: CIDetectorTypeQRCode,
                                  context: nil,
                                  options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])

        let features = detector?.features(in: image) ?? []

        return features.compactMap { feature in
            return (feature as? CIQRCodeFeature)?.messageString
        }
    }
}
extension SKProduct {
    fileprivate static var formatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter
    }
    
    var localizedPriceLoacl: String {
        if self.price == 0.00 {
            return "Get"
        } else {
            let formatter = SKProduct.formatter
            formatter.locale = self.priceLocale
            
            guard let formattedPrice = formatter.string(from: self.price) else {
                return "Unknown Price"
            }
            
            return formattedPrice
        }
    }
    var localizedCurnancy: String {
        if self.price == 0.00 {
            return "Get"
        } else {
            let formatter = SKProduct.formatter
            formatter.locale = self.priceLocale
            return formatter.currencyCode!
        }
    }
    
}
extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, targetText: String) -> Bool {
        guard let attributedString = label.attributedText, let lblText = label.text else { return false }
        let targetRange = (lblText as NSString).range(of: targetText)
        //IMPORTANT label correct font for NSTextStorage needed
        let mutableAttribString = NSMutableAttributedString(attributedString: attributedString)
        mutableAttribString.addAttributes(
            [NSAttributedString.Key.font: label.font ?? UIFont.smallSystemFontSize],
            range: NSRange(location: 0, length: attributedString.length)
        )
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: mutableAttribString)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
            locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override public class var layerClass: AnyClass { CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }

    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }

}
