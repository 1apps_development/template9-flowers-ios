//
//  ViewController.swift
//  plants
//
//  Created by mac on 27/04/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
  
    //MARK: - IBActions
    
    @IBAction func onClickLogin(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSignup(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickGuest(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

