//
//  AppDelegate.swift
//  plants
//
//  Created by mac on 27/04/22.
//

import UIKit
import IQKeyboardManagerSwift
import Toast_Swift
import GoogleSignIn
import Firebase
import FirebaseMessaging
import Alamofire
import FacebookCore


@main
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
    var window: UIWindow?
    var firebaseToken: String = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )

        IQKeyboardManager.shared.enable = true
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }


        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
          )
        } else {
          let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        
        UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_CUPENOBJECT)
        UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_BILLINGOBJ)
        
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_IS_USER_LOGGED_IN) != nil{
            self.window =  UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootVC = storyboard.instantiateViewController(identifier: "MainTabbarVC") as! MainTabbarVC
            let rootNC = UINavigationController(rootViewController: rootVC)
            rootNC.navigationBar.isHidden = true
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }else if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_APP_TOKEN) == ""{
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_USERID)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_APP_TOKEN)
            self.window =  UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootVC = storyboard.instantiateViewController(identifier: "MainTabbarVC") as! MainTabbarVC
            let rootNC = UINavigationController(rootViewController: rootVC)
            rootNC.navigationBar.isHidden = true
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }else{
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_USERID)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_APP_TOKEN)
            self.window =  UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootVC = storyboard.instantiateViewController(identifier: "MainTabbarVC") as! MainTabbarVC
            let rootNC = UINavigationController(rootViewController: rootVC)
            rootNC.navigationBar.isHidden = true
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
        if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) == nil
        {
            let Guestcart_Array = [[String:Any]]()
            UserDefaults.standard.set(Guestcart_Array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
        }

        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        FBSDKCoreKit.ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )

        return true
    }
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().token { (result, error) in
            if error == nil{
                //            print(result)
            }
        }
    }
    public func logoutWithoutAPI(_ isLogout: Bool = false){
        if isLogout{
            resetDefaults()
        }
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
    }
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    
    //MessagingDelegate
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let fbtoken = fcmToken{
            self.firebaseToken = fbtoken
            setValueToUserDefaults(value: fbtoken as AnyObject, key: userDefaultsKeys.KEY_APP_FCM_TOKEN)
            print("Firebase token: \(fbtoken)")
        }
    }
    
    
    //UNUserNotificationCenterDelegate
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("APNs received with: \(userInfo)")
        
        
    }

    

    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }


}

