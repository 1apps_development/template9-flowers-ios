//
//  SignupEmailViewController.swift
//  Plants
//
//  Created by Vrushik on 20/04/22.
//

import UIKit

class SignupEmailViewController: UIViewController {
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var btnHideShow: UIButton!
    
    var registerType: String = "email"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - InitialUISetup
    
    func setupInitialUI(){
        btnHideShow.setImage(UIImage.init(named: "password eye close"), for: .normal)
        btnHideShow.setImage(UIImage.init(named: "password eye"), for: .selected)
    }
    
    // MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func onClickRegister(_ sender: Any)
    {
        if txtFullname.text! == ""{
            self.view.makeToast("Fullname Field can not be empty")
        }else if txtFullname.text! != "" && !txtFullname.text!.contains(find: " "){
            self.view.makeToast("Please separate first name and lastname by white space")
        }else if txtPhone.text! == ""{
            self.view.makeToast("Phone number Field can not be empty")
        }else if txtPhone.text! != "" && !txtPhone.text!.isPhone(){
            self.view.makeToast("Please enter valid Phone number")
        }else if txtEmail.text! == ""{
            self.view.makeToast("Email Field can not be empty")
        }else if txtEmail.text! != "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!) == false{
            self.view.makeToast("Please enter valid Email Address")
        }else if txtPass.text! == ""{
            self.view.makeToast("Password Field can not be empty")
        }
//        else if txtPass.text! != "" && !txtPass.text!.isValidPassword(){
//            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
//        }
        else{
            let names = txtFullname.text!.components(separatedBy: " ")
            var fName: String = ""
            var lName: String = ""
            if names.count > 1{
                fName = names[0]
                lName = names[1]
            }
            let param:[String:Any] = ["first_name":fName == "" ? txtFullname.text! : fName,
                                      "last_name":lName == "" ? txtFullname.text! : lName,
                                      "email":txtEmail.text!,
                                      "password":txtPass.text!,
                                      "mobile":txtPhone.text!,
                                      "device_type":App_device_type,
                                      "register_type":registerType,
                                      "google_id":"",
                                      "facebook_id":"",
                                      "apple_id":"",
                                      "token":getFCMToken(),
                                      "theme_id":APP_THEME_ID]
            self.makeAPIcallforSignup(param)
        }
    }
    
    @IBAction func onClickHideShow(_ sender: UIButton){
        if !sender.isSelected{
            sender.isSelected = true
            txtPass.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            txtPass.isSecureTextEntry = true
        }
        
    }
    
    @IBAction func onClickForgotpassword(_ sender: Any)
    {
        
    }
    
    
    // MARK: - API
    
    func makeAPIcallforSignup(_ param:[String:Any])
    {
//        let headers:HTTPHeaders = ["":""]
        AIServiceManager.sharedManager.callPostApi(URL_Signup, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let msg = json["message"] as? String
                    if msg == "fail" {
                        if let datas = json["data"] as? [String:Any]{
                            let massage = datas["message"] as? String
                            self.view.makeToast(massage!)
                        }
                    }else{
                        if let data = json["data"] as? [String:Any]{
                            let userid = data["id"] as? String
                            UserDefaults.standard.set(userid, forKey: userDefaultsKeys.KEY_USERID)
                            let firstname = data["first_name"] as? String
                            UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                            let lastname = data["last_name"] as? String
                            UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                            let fullname = "\(firstname!) \(lastname!)"
                            UserDefaults.standard.set(fullname, forKey: userDefaultsKeys.KEY_FULLNAME)
                            let email = data["email"] as? String
                            UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
                            let phonenumber = data["mobile"] as? String
                            UserDefaults.standard.set(phonenumber, forKey: userDefaultsKeys.KEY_PHONE)
                            if let msg = data["message"] as? String{
                                self.view.makeToast(msg)
                            }
                            let image = data["image"] as? String
                            UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                            if let token = data["token"] as? String{
                                setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        printD(json)
                    }
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }

}
