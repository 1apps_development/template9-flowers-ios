//
//  SetNewPasswordVC.swift
//  Plants
//
//  Created by mac on 14/10/22.
//

import UIKit

class SetNewPasswordVC: UIViewController {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCnfPassword: UITextField!
    @IBOutlet weak var btnHideShow: UIButton!
    @IBOutlet weak var btnHideShow1: UIButton!
    var email: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
 
    func setupInitialUI(){
        self.btnHideShow.setImage(UIImage.init(named: "password eye"), for: .normal)
        self.btnHideShow.setImage(UIImage(named: "password eye close"), for: .selected)
        self.btnHideShow1.setImage(UIImage.init(named: "password eye"), for: .normal)
        self.btnHideShow1.setImage(UIImage(named: "password eye close"), for: .selected)
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSetPassword(_ sender: Any){
        if txtPassword.text! == ""{
            self.view.makeToast("Please Enter Password")
        }else if txtCnfPassword.text == ""{
            self.view.makeToast("Please Enter Confirm Password")
        }else if txtPassword.text! != txtCnfPassword.text!{
            self.view.makeToast("Password does not match")
        }else{
            self.MakeAPICallforSavePassword(["email":self.email!,
                                             "password":txtCnfPassword.text!,
                                             "theme_id":APP_THEME_ID])
        }
    }
    
    @IBAction func onClickHideShow(_ sender: UIButton){
        if !sender.isSelected{
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
        }
        
    }
    @IBAction func onClickHideShow1(_ sender: UIButton){
        if !sender.isSelected{
            sender.isSelected = true
            txtCnfPassword.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            txtCnfPassword.isSecureTextEntry = true
        }
        
    }
    
    //MARK: - API
    
    func MakeAPICallforSavePassword(_ params:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_PasswordSAVE, params: params, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let status = json["status"] as? Int
                    if status == 1{
                        if let data = json["data"] as? [String:Any]{
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                                    let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "PasswordSuccessVC") as! PasswordSuccessVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                })
                            }
                        }
                    }else if status == 9 {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                        nav.navigationBar.isHidden = true
                        keyWindow?.rootViewController = nav
                    }else{
                        let msg = json["data"] as! [String:Any]
                        let massage = msg["message"] as! String
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                    }
                    
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
