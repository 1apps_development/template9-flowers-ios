//
//  ResetPasswordViewController.swift
//  Plants
//
//  Created by Vrushik on 20/04/22.
//

import UIKit

class ResetPasswordViewController: UIViewController , OTPFieldViewDelegate{
    
    @IBOutlet weak var txtOTP: OTPFieldView!
    var otp:String = ""
    var email: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtOTP.fieldSize = 35
        txtOTP.cursorColor = .white
        txtOTP.otpInputType = .numeric
        txtOTP.displayType = .underlinedBottom
        txtOTP.fieldsCount = 4
        txtOTP.delegate = self
        txtOTP.initializeUI()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickVerifyOTP(_ sender: Any){
        if otp.count == 4{
            self.MakeAPICallforVerifyOTP(["email":self.email!,
                                          "otp":self.otp,
                                          "theme_id":APP_THEME_ID])
        }
    }
    
    @IBAction func onClickContactUS(_ sender: Any){
        
    }
    
    //MARK: - API
    
    func MakeAPICallforVerifyOTP(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_VerifyOTP, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let status = json["status"] as? Int
                    if status == 1{
                        if let data = json["data"] as? [String:Any]{
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                                    let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SetNewPasswordVC") as! SetNewPasswordVC
                                    vc.email = self.email
                                    self.navigationController?.pushViewController(vc, animated: true)
//                                })
                            }
                        }
                    }else if status == 9 {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                        nav.navigationBar.isHidden = true
                        keyWindow?.rootViewController = nav
                    }else{
                        let msg = json["data"] as! [String:Any]
                        let massage = msg["message"] as! String
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                    }
                    
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }

//OtpDelegate
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        self.otp = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return hasEnteredAll
    }
}
