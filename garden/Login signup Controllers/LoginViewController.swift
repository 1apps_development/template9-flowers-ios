//
//  LoginViewController.swift
//  Plants
//
//  Created by Vrushik on 20/04/22.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import FacebookLogin
import FacebookCore


class LoginViewController: UIViewController {
    @IBOutlet weak var viewGoogleSignin: UIView!
    var regType: String = "email"
    
    let signInConfig = GIDConfiguration(clientID: "338614670794-e377seou1vger4fhhs7qb3923vbbhne3.apps.googleusercontent.com")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Functions
   
    func showAlertMessage(titleStr: String, messageStr: String){
        let alert = UIAlertController.init(title: titleStr, message: messageStr, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: "ok", style: .default) { action in
            
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickLoginwithEmail(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "LoginEmailViewController") as! LoginEmailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFacebook(_ sender: Any)
    {
        self.regType = "facebook"
        self.FacebookLogin()
    }
    @IBAction func onClickGoogle(_ sender: Any)
    {
        GIDSignIn.sharedInstance.signIn(withPresenting: self) { signInResult, error in
            guard error == nil else { return }
            guard let signInResult = signInResult else { return }

            let user = signInResult.user
            let googleId = user.userID
            let emailAddress = user.profile?.email

            let fullName = user.profile?.name
            let givenName = user.profile?.givenName
            let familyName = user.profile?.familyName

            let profilePicUrl = user.profile?.imageURL(withDimension: 320)


//            let urlString = API_URL + "login"
            let params: [String:Any] = ["first_name":givenName!,
                                        "last_name":familyName!,
                                        "email":emailAddress!,
                                        "password":"",
                                        "device_type":App_device_type,
                                        "register_type":self.regType,
                                        "google_id":googleId,
                                        "facebook_id":"",
                                        "apple_id":"",
                                        "token":getFCMToken(),
                                        "theme_id":APP_THEME_ID]
            self.MakeAPICallforLogin(params)
        }

    }
    @IBAction func onClickApple(_ sender: Any)
    {
        self.regType = "apple"
        self.AppleLogin()
    }
    @IBAction func onClickSignup(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - API
    
    func MakeAPICallforLogin(_ params:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Signup, params: params, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let msg = json["message"] as? String
                    if msg == "fail" {
                        if let datas = json["data"] as? [String:Any]{
                            let massage = datas["message"] as? String
                            self.view.makeToast(massage!)
                        }
                    }else{
                        if let data = json["data"] as? [String:Any]{
                            let userid = data["id"] as? String
                            UserDefaults.standard.set(userid, forKey: userDefaultsKeys.KEY_USERID)
                            let firstname = data["first_name"] as? String
                            UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                            let lastname = data["last_name"] as? String
                            UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                            UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FULLNAME)
                            let email = data["email"] as? String
                            UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
                            let phonenumber = data["mobile"] as? String
                            UserDefaults.standard.set(phonenumber, forKey: userDefaultsKeys.KEY_PHONE)
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
                            }
                            let image = data["image"] as? String
                            UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                            if let token = data["token"] as? String{
                                setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        printD(json)
                    }
                }
            case let .failure(error):
                printD("errors:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController
{

    func FacebookLogin()
    {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile"], from: nil) { loginResult, error in
            
            if let error = error {
                print("Encountered Erorr: \(error)")
                self.showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Something went wrong. Try again later")

            } else if let result = loginResult, result.isCancelled {
                print("Cancelled")
                self.showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Something went wrong. Try again later")

            } else {
                print("Logged In")
                let dictParamaters = ["fields":"id, name, email"]
                let request: GraphRequest = GraphRequest.init(graphPath: "me", parameters: dictParamaters)
                request.start { (connection, result, error) in
                    let responseData = result as! NSDictionary
                    let facebookId = responseData["id"] as! String
                    let name = responseData["name"] as! String
                    var fName: String = ""
                    var lName: String = ""
                    let namesArr = name.components(separatedBy: " ")
                    if namesArr.count > 1{
                        fName = namesArr[0]
                        lName = namesArr[1]
                    }
                    var email = ""
                    if responseData["email"] != nil {
                        email = responseData["email"] as! String
                    }
                    loginManager.logOut()
                    let params: [String:Any] = ["first_name":fName,
                                                "last_name":lName,
                                                "email":email,
                                                "password":"",
                                                "device_type":App_device_type,
                                                "register_type":self.regType,
                                                "google_id":"",
                                                "facebook_id":facebookId,
                                                "apple_id":"",
                                                "token":getFCMToken(),
                                                "theme_id":APP_THEME_ID]
                    self.MakeAPICallforLogin(params)
                }
            }
        }
    }



    func AppleLogin(){
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
    }

}
//MARK: - Extensions
extension LoginViewController: ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            let userIdentifier = appleIDCredential.user
            var Email = String()
            var Fullname = String()
            if appleIDCredential.email != nil
            {
                Email = appleIDCredential.email!
                UserDefaults.standard.set(Email, forKey: userDefaultsKeys.KEY_EMAIL)
            }
            else
            {
                Email = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_EMAIL) ?? "N/A"
            }
            //            print(appleIDCredential.fullName)
            if appleIDCredential.fullName?.givenName != nil
            {
                Fullname = (appleIDCredential.fullName?.givenName)! + (appleIDCredential.fullName?.familyName)!
                UserDefaults.standard.set(Fullname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
            }
            else
            {
                Fullname = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_FIRSTNAME) ?? "N/A"
            }
            
            let params: [String:Any] = ["first_name":Fullname,
                                        "last_name":Fullname,
                                        "email":Email,
                                        "password":"",
                                        "mobile":"",
                                        "device_type":"ios",
                                        "register_type":"apple",
                                        "google_id":"",
                                        "facebook_id":"",
                                        "apple_id":userIdentifier,
                                        "token":getFCMToken(),
                                        "theme_id":APP_THEME_ID]
            self.MakeAPICallforLogin(params)
            break
        default:
            break
        }
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
