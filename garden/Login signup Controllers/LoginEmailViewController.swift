//
//  LoginEmailViewController.swift
//  Plants
//
//  Created by Vrushik on 07/03/22.
//

import UIKit
import Alamofire

class LoginEmailViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var btnHideShow: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        if IS_DEMO_MODE == true {
            self.txtEmail.isUserInteractionEnabled = false
            self.txtEmail.isUserInteractionEnabled = false
            self.txtPass.text = "123456"
            self.txtEmail.text = "garden@example.com"
        }
        else {
            self.txtPass.text = ""
            self.txtEmail.text = ""
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - InitialUISetup
    
    func setupInitialUI(){
        self.btnHideShow.setImage(UIImage.init(named: "password eye"), for: .normal)
        self.btnHideShow.setImage(UIImage(named: "password eye close"), for: .selected)
    }
    
    // MARK: - IBActions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func onClickHideShow(_ sender: UIButton )
    {
        if sender.isSelected{
            btnHideShow.isSelected = false
            txtPass.isSecureTextEntry = true
        }else{
            btnHideShow.isSelected = true
            txtPass.isSecureTextEntry = false
        }
    }
    
    @IBAction func onClickLogin(_ sender: Any)
    {
        if txtEmail.text! == ""{
            self.view.makeToast("Email Field can not be empty")
        }else if txtEmail.text! != "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!) == false{
            self.view.makeToast("Please enter valid Email Address")
        }else if txtPass.text! == ""{
            self.view.makeToast("Password Field can not be empty")
        }
//        else if txtPass.text! != "" && !txtPass.text!.isValidPassword(){
//            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
//        }
        else{
            let param:[String:Any] = ["email":txtEmail.text!,
                                      "password":txtPass.text!,
                                      "device_type":App_device_type,
                                      "token":getFCMToken(),
                                      "theme_id":APP_THEME_ID]
            self.makeAPIcallforLogin(param)
        }
    }
    
    @IBAction func onClickSignup(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
     }
    
    @IBAction func onClickForgotpassword(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
            
    }
    
    
    // MARK: - API
    
    func makeAPIcallforLogin(_ param:[String:Any], bypass:Bool = false)
    {
//        let headers:HTTPHeaders = ["":""]
        if bypass{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            AIServiceManager.sharedManager.callPostApi(URL_Login, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        let msg = json["message"] as? String
                        if msg == "fail" {
                            if let datas = json["data"] as? [String:Any]{
                                let massage = datas["message"] as? String
                                self.view.makeToast(massage!)
                            }
                        }else{
                            if let data = json["data"] as? [String:Any]{
                                let userid = data["id"] as? String
                                UserDefaults.standard.set(userid, forKey: userDefaultsKeys.KEY_USERID)
                                let firstname = data["first_name"] as? String
                                UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                                let lastname = data["last_name"] as? String
                                UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                                let fullname = "\(firstname!) \(lastname!)"
                                UserDefaults.standard.set(fullname, forKey: userDefaultsKeys.KEY_FULLNAME)
                                let email = data["email"] as? String
                                UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
                                let phonenumber = data["mobile"] as? String
                                UserDefaults.standard.set(phonenumber, forKey: userDefaultsKeys.KEY_PHONE)
                                if let msg = data["message"] as? String
                                {
                                    self.view.makeToast(msg)
                                }
                                
                                let image = data["image"] as? String
                                UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                                if let token = data["token"] as? String{
                                    setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                    setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                    setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                    let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            printD(json)
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
