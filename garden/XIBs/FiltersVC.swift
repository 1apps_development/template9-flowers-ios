//
//  FiltersVC.swift
//  kiddos
//
//  Created by mac on 15/04/22.
//

import UIKit
import TagListView
import RangeSeekSlider

protocol FilterDelegate {
    func filterData(tag:String,min_price:String,max_price:String,rating:String,isfilter:String)
}


class FiltersVC: UIViewController {
    
    @IBOutlet weak var taglistView: TagListView!
    @IBOutlet weak var lbl_maximumPrice: UILabel!
    @IBOutlet weak var lbl_minimumPrice: UILabel!
    @IBOutlet weak var slider: RangeSeekSlider!
    @IBOutlet weak var btn_one: UIButton!
    @IBOutlet weak var btn_Two: UIButton!
    @IBOutlet weak var btn_three: UIButton!
    @IBOutlet weak var btn_four: UIButton!
    @IBOutlet weak var btn_five: UIButton!
    
//    @IBOutlet weak var rangeslider:
    
    private var selected = [String]()
    private var titles = [String]()

    var categoryArray: [categoryModel] = []
    var catdata: categoryModel?
    var maxmumPrice = String()
    var minimumPrice = String()
    var rateing = String()
    var isFilered = String()
    var selectedID = [String]()
    var selectedName = [String]()
    var delegate: FilterDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.slider.delegate = self
        self.btn_one.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_Two.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_three.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_four.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_five.setImage(UIImage.init(named: "checkbox"), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getfilterData(param)
        let catlistParam: [String:Any] = ["theme_id": APP_THEME_ID]
        getCatgoryList(catlistParam)
    }
    @IBAction func onClickClose(_ sende: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onclickDltFilter(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onclickFilter(_ sender: UIButton) {
        self.dismiss(animated: true) {
            if self.rateing == ""{
                self.rateing = "0"
            }
            self.isFilered = "1"
            print("\(self.selectedID)")
            self.delegate.filterData(tag: self.selectedID.joined(separator: ","), min_price: self.minimumPrice, max_price: self.maxmumPrice, rating: self.rateing, isfilter: self.isFilered)
        }
    }
    
    @IBAction func onclickBtn1(_ sender: UIButton) {
        if sender.tag == 11{
            self.rateing = "1"
            self.btn_one.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.btn_Two.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_three.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_four.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_five.setImage(UIImage.init(named: "checkbox"), for: .normal)
        }else if sender.tag == 12{
            self.rateing = "2"
            self.btn_Two.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.btn_one.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_three.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_four.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_five.setImage(UIImage.init(named: "checkbox"), for: .normal)
        }else if sender.tag == 13 {
            self.rateing = "3"
            self.btn_three.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.btn_Two.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_four.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_five.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_one.setImage(UIImage.init(named: "checkbox"), for: .normal)
        }else if sender.tag == 14 {
            self.rateing = "4"
            self.btn_four.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.btn_three.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_Two.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_five.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_one.setImage(UIImage.init(named: "checkbox"), for: .normal)
        }else if sender.tag == 15 {
            self.rateing = "5"
            self.btn_five.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.btn_four.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_three.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_Two.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.btn_one.setImage(UIImage.init(named: "checkbox"), for: .normal)
        }
    }
    func getCatgoryList(_ param: [String:Any]) {
        AIServiceManager.sharedManager.callPostApi(URL_CATEGORYLIST, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            let maxiMumPrice = json["max_price"] as? Int
                            self.slider.maxValue = CGFloat(maxiMumPrice!)
                            self.lbl_maximumPrice.text = "\(maxiMumPrice!) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)!)"
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                   
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
    }
    func getfilterData(_ param: [String:Any]){
            AIServiceManager.sharedManager.callPostApi(URL_HomeCategories, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String: Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let categories = data["data"] as? [[String:Any]]{
                                        self.categoryArray = HomeCatSuperModel.init(categories).categories
                                        var arrCategories = [String]()
                                        for datafilter in categories {
                                            if let tags = datafilter["name"] as? String{
                                                arrCategories.append(tags)
                                            }
                                        }
                                        self.taglistView.delegate = self
                                        self.taglistView.addTags(arrCategories)
                                        self.taglistView.alignment = .left
                                        self.taglistView.textFont = UIFont(name: "Outfit-Medium", size: 14)!
                                        self.taglistView.borderWidth = 1
                                        self.taglistView.layer.borderWidth = 0.0
                                    }
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
    //                    print(json)
                    }
                case let .failure(error):
                    print(error.localizedDescription)
                    HIDE_CUSTOM_LOADER()
                    break
                }
        }
    }
    
}

extension FiltersVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubtaskCell", for: indexPath) as! SubtaskCell
        cell.configureCell(titles[indexPath.row], isSelected: selected.contains(titles[indexPath.row]) ? true : false)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? SubtaskCell, let text = cell.lblSubtask.text else {return}
        
        if selected.contains(text) {
            selected = selected.filter{$0 != text}
        } else {
            selected.append(text)
        }
        collectionView.reloadData()

    }
}
extension FiltersVC: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === slider {
            let MinPrice = Int(minValue)
            self.minimumPrice = "\(MinPrice)"
            self.lbl_minimumPrice.text = "\(MinPrice) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)"
            
            let MaxPrice = Int(maxValue)
            self.maxmumPrice = "\(MaxPrice)"
            self.lbl_maximumPrice.text = "\(MaxPrice) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)"
            
            print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
extension FiltersVC: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        //        print("Tag pressed: \(title), \(tagView.tag)")
        if tagView.isSelected == true
        {
            if selectedName.contains(title) == true
            {
                let index = selectedName.firstIndex(of: title)
                self.selectedID.remove(at: index!)
                self.selectedName.remove(at: index!)
            }
        }
        else{
            for data in self.categoryArray
            {
                if data.name == title
                {
                    self.selectedID.append("\(data.id!)")
                    self.selectedName.append(data.name!)
                }
            }
        }
        tagView.isSelected = !tagView.isSelected
    }
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        //        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
