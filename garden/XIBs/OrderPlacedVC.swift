//
//  OrderPlacedVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit

class OrderPlacedVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var lbl_orderTitle: UILabel!
    @IBOutlet weak var lbl_orderDiscription: UILabel!
    
    //MARK: - Variables
    
    var ordercomTitle = String()
    var ordercomDescription = String()
    var parentVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_orderTitle.text = ordercomTitle
        lbl_orderDiscription.text = ordercomDescription
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onclickbackToshop(_ sender: UIButton) {
        dismiss(animated: true) {
            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSERPRODUCTARRAY)
            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)
            UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)
            UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_CUPENOBJECT)
            UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_BILLINGOBJ)
            
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_DELIVARYID)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_SAVED_CART)
            
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
            self.parentVC.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
}
