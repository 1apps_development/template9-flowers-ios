//
//  MenuViewController.swift
//  Taskly
//
//  Created by mac on 09/03/22.
//

import UIKit
import SOTabBar
import Alamofire
protocol MenuOptionSelectDelegate: NSObject{
    func menuOptionSelected(_ title: String)
}

class MenuViewController: UIViewController {
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var mDelegate: MenuOptionSelectDelegate?
    var parentVC: UIViewController!
    var selectedItem: String!
    var arrMenuItems : [MenuModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
//        lblTitle.text = selectedItem != nil ? selectedItem : "Dashboard"
        tableItems.register(UINib.init(nibName: "MenuTVCell", bundle: nil), forCellReuseIdentifier: "MenuTVCell")
        tableItems.tableFooterView = UIView()
        self.MakeAPICallforSideMenu(["theme_id":APP_THEME_ID])
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            self.btnLogin.isHidden = false
        } else {
            self.btnLogin.isHidden = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let transition:CATransition = CATransition()
//        transition.duration = 0.3
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromBottom
//        view.layer.add(transition, forKey: kCATransition)
        self.view.alpha = 1
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
//        self.navigationController?.popViewController(animated: true)
//        let transition:CATransition = CATransition()
//        transition.duration = 0.3
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromTop
//        view.layer.add(transition, forKey: kCATransition)
//        self.view.alpha = 0

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickLogin(_ sender: Any){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
    }
    
    //MARK: - API
  
    func MakeAPICallforSideMenu(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_SideMenu, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [[String:Any]]{
                        self.arrMenuItems = MenuSuperModel.init(data).menuitems
                        self.tableItems.reloadData()
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    //MARK: - Actions
    
    @IBAction func onclickSharebutton(_ sender: UIButton) {
        if sender.tag == 1 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_YOUTUVBE)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if sender.tag == 2 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_MASSANGER)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if sender.tag == 3 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_INSTA)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if sender.tag == 4 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_TWITTER)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"MenuTVCell", for: indexPath) as! MenuTVCell
        cell.selectionStyle = .none
        cell.configureCell(arrMenuItems[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
            vc.catid = self.arrMenuItems[indexPath.row].id!
            self.parentVC.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
