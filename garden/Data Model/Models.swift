//
//  Models.swift
//  Jewellery
//
//  Created by mac on 02/11/22.
//

import Foundation
import UIKit

class HomeCatSuperModel: NSObject{
    var categories:[categoryModel] = []
    init(_ dataArr:[[String:Any]]){
        for i in 0..<dataArr.count{
            categories.append(categoryModel.init(dataArr[i]))
        }
    }
}

class categoryModel: NSObject{
    
    static var SharedInstance = categoryModel()
    override init() {
        super.init()
    }

    
    var id: Int? = 0
    var name: String? = ""
    var imagePath: String? = ""
    var iconPath: String? = ""
    var trending: Int? = 0
    var status: Int? = 0
    var themeId: String? = ""
    var createdAt: String? = ""
    var updatedAt: String? = ""
    var categoryId: Int? = 0
    var image: String? = ""
    var demoField: String? = ""
    var categoryItem: Int? = 0
    var mainCategoryId: Int? = 0
   
    init(_ dict:[String:Any]){
        id = dict["id"] as? Int
        name = dict["name"] as? String
        imagePath = dict["image_path"] as? String
        iconPath = dict["icon_path"] as? String
        trending = dict["trending"] as? Int
        status = dict["status"] as? Int
        themeId = dict["theme_id"] as? String
        createdAt = dict["created_at"] as? String
        updatedAt = dict["updated_at"] as? String
        categoryId = dict["category_id"] as? Int
        image = dict["image"] as? String
        demoField = dict["demo_field"] as? String
        categoryItem = dict["category_item"] as? Int
        mainCategoryId = dict["maincategory_id"] as? Int
    }
}
class ProductsSuperModel: NSObject{
    var product:[Products] = []
    init(_ dataArr:[[String:Any]]){
        for i in 0..<dataArr.count{
            product.append(Products.init(dataArr[i]))
        }
    }
}

class Products: NSObject{
    static var SharedInstance = Products()
    override init() {
        super.init()
    }
    var id: Int? = 0
    var name: String? = ""
    var proDiscription: String? = ""
    var proOtherDiscription: [String:Any]? = ["":""]
    var proOtherDiscriptionAPI: String? = ""
    var tag: [String:Any]? = ["":""]
    var tagapi: String? = ""
    var catid: Int? = 0
    var subcatid: Int? = 0
    var coverimagepath: String? = ""
    var covreimageURL: String? = ""
    var price: Int? = 0
    var discountType: String? = ""
    var discountAmmount: String? = ""
    var productStock: String? = ""
    var variantProduct: String? = ""
    var defaultVariantID: Int? = 0
    var trending: Bool? = false
    var avragerate: Int? = 0
    var slug: String? = ""
    var temeid: String? = ""
    var status: Bool? = false
    var demofield: String? = ""
    var mainCatID: Int? = 0
    var incart: Bool? = false
    var inwishlist: Bool? = false
    var varientPrice: Float? = 0.0
    var varientID: String? = ""
    var varientName: String? = ""
    var orignalPrice: String? = ""
    var discountPrice: String? = ""
    var isincart: Bool? = false
    var isinwishlist: Bool? = false
    var finalPrice: String? = ""
    var isreview: Bool? = false
    var returnText: String? = ""
    
    init(_ produtcsDic: [String:Any]){
        id = produtcsDic["id"] as? Int
        name = produtcsDic["name"] as? String
        proDiscription = produtcsDic["description"] as? String
        proOtherDiscription = produtcsDic["other_description"] as? [String:Any]
        proOtherDiscriptionAPI = produtcsDic["other_description_api"] as? String
        tag = produtcsDic["tag"] as? [String:Any]
        tagapi = produtcsDic["tag_api"] as? String
        catid = produtcsDic["category_id"] as? Int
        subcatid = produtcsDic["subcategory_id"] as? Int
        coverimagepath = produtcsDic["cover_image_path"] as? String
        covreimageURL = produtcsDic["cover_image_url"] as? String
        price = produtcsDic["price"] as? Int
        discountType = produtcsDic["discount_type"] as? String
        discountAmmount = produtcsDic["discount_amount"] as? String
        productStock = produtcsDic["product_stock"] as? String
        variantProduct = produtcsDic["variant_product"] as? String
        defaultVariantID = produtcsDic["default_variant_id"] as? Int
        trending = produtcsDic["trending"] as? Bool
        avragerate = produtcsDic["average_rating"] as? Int
        slug = produtcsDic["slug"] as? String
        temeid = produtcsDic["theme_id"] as? String
        status = produtcsDic["status"] as? Bool
        demofield = produtcsDic["demo_field"] as? String
        mainCatID = produtcsDic["maincategory_id"] as? Int
        incart = produtcsDic["in_cart"] as? Bool
        inwishlist = produtcsDic["in_whishlist"] as? Bool
        varientPrice = produtcsDic["default_variant_price"] as? Float
        varientID = produtcsDic["variant_id"] as? String
        varientName = produtcsDic["default_variant_name"] as? String
        orignalPrice = produtcsDic["original_price"] as? String
        discountPrice = produtcsDic["discount_price"] as? String
        isincart = produtcsDic["in_cart"] as? Bool
        isinwishlist = produtcsDic["in_whishlist"] as? Bool
        finalPrice = produtcsDic["final_price"] as? String
        isreview = produtcsDic["is_review"] as? Bool
        returnText = produtcsDic["retuen_text"] as? String
    }
}
class MenuSuperModel: NSObject{
    var menuitems:[MenuModel] = []
    init(_ arrItme: [[String:Any]]){
        for i in 0..<arrItme.count{
            menuitems.append(MenuModel.init(arrItme[i]))
        }
    }
}
class MenuModel: NSObject{
    var image: String? = ""
    var iconImgPath: String? = ""
    var name: String? = ""
    var id: Int? = 0
    var themeId: String? = ""
    var subCategory: [Any]? = []
    init(_ dict:[String:Any]){
        image = dict["image"] as? String
        iconImgPath = dict["icon_img_path"] as? String
        name = dict["name"] as? String
        id = dict["id"] as? Int
        themeId = dict["theme_id"] as? String
        subCategory = dict["sub_category"] as? [Any]
    }
}
class ProductSubImage: NSObject{
    var subproductdata: [ProductImages] = []
    init(_ arrCate:[[String:Any]]){
        for i in 0..<arrCate.count{
            let dic = arrCate[i]
            subproductdata.append(ProductImages.init(dic))
        }
    }
}
class ProductImages: NSObject{
    static var shareInstance = ProductImages()
    override init() {
        super.init()
    }
    var id: Int? = 0
    var productid: Int? = 0
    var imagePath: String? = ""
    var themeid: String? = ""
    var demofield: String? = ""
    
    init(_ productImages: [String:Any]) {
        id = productImages["id"] as? Int
        productid = productImages["product_id"] as? Int
        imagePath = productImages["image_path"] as? String
        themeid = productImages["theme_id"] as? String
        demofield = productImages["demo_field"] as? String
    }
}
class ProductVarient: NSObject{
    var varients: [Varient] = []
    init(_ arrVarients: [[String:Any]]) {
        for i in 0..<arrVarients.count{
            let dic = arrVarients[i]
            varients.append(Varient.init(dic))
        }
    }
}
class Varient: NSObject{
    static var shareinstance = Varient()
    override init() {
        super.init()
    }
    var name: String? = ""
    var type: String? = ""
    var value: [String]? = [""]
    
    init(_ varients: [String:Any]){
        name = varients["name"] as? String
        type = varients["type"] as? String
        value = varients["value"] as? [String]
    }
}
class Discription: NSObject{
    var discription: [OtherDis] = []
    init(_ arrDiscript:[[String:Any]]){
        for i in 0..<arrDiscript.count{
            let dic = arrDiscript[i]
            discription.append(OtherDis.init(dic))
        }
    }
}
class OtherDis: NSObject{
    var title: String? = ""
    var descrips: String? = ""
    var isSelected: Bool = false
    
    init(_ otherdic: [String:Any]){
        title = otherdic["title"] as? String
        descrips = otherdic["description"] as? String
    }
}
class Instruction: NSObject{
    var instraction: [ProductInstraction] = []
    init(_ arrDiscript:[[String:Any]]){
        for i in 0..<arrDiscript.count{
            let dic = arrDiscript[i]
            instraction.append(ProductInstraction.init(dic))
        }
    }
}
class ProductInstraction: NSObject{
    
    static var shareInstance = ProductInstraction()
    
    override init() {
        super.init()
    }
    var discription: String? = ""
    var img_path: String? = ""
    
    init(_ arrProductInstruct: [String:Any]) {
        discription = arrProductInstruct["description"] as? String
        img_path = arrProductInstruct["img_path"] as? String
    }
}
class ProductReview: NSObject{
    var productreview: [Review] = []
    init(_ arrReview: [[String:Any]]){
        for i in 0..<arrReview.count{
            let dic = arrReview[i]
            productreview.append(Review.init(dic))
        }
    }
}
class Review: NSObject{
    static var shareInstance = Review()
    
    override init() {
        super.init()
    }
    var title: String? = ""
    var subtitle: String? = ""
    var reteing: Double? = 0
    var reviewProduct: String? = ""
    var username: String? = ""
    var userEmail: String? = ""
    
    init(_ reviews: [String:Any]){
        title = reviews["title"] as? String
        subtitle = reviews["sub_title"] as? String
        reteing = reviews["rating"] as? Double
        reviewProduct = reviews["review"] as? String
        username = reviews["user_name"] as? String
        userEmail = reviews["user_email"] as? String
    }
}
class ProductsList: NSObject{
    var cartsList: [CartsList] = []
    init(_ arrCart: [[String:Any]]){
        for i in 0..<arrCart.count{
            let dic = arrCart[i]
            cartsList.append(CartsList.init(dic))
        }
    }
}
class CartsList: NSObject, NSCoding, Encodable{
    
    func encode(with coder: NSCoder) {
          coder.encode(productid, forKey: "product_id")
          coder.encode(name, forKey: "name")
          coder.encode(image, forKey: "image")
          coder.encode(orignalprice, forKey: "orignal_price")
          coder.encode(totalproductOrignalPricde, forKey: "total_orignal_price")
          coder.encode(perproDisPrice, forKey: "per_product_discount_price")
          coder.encode(discountPrice, forKey: "discount_price")
          coder.encode(finalprice, forKey: "final_price")
          coder.encode(qty, forKey: "qty")
          coder.encode(varientId, forKey: "variant_id")
          coder.encode(varientName, forKey: "variant_name")
          coder.encode(retun, forKey: "return")
      }
      
      required convenience init?(coder: NSCoder) {
          let cid = coder.decodeObject(forKey: "product_id") as! String
          let cname = coder.decodeObject(forKey: "name") as! String
          let slug = coder.decodeObject(forKey: "image") as! String
          let cprice = coder.decodeObject(forKey: "orignal_price") as! String
          let corignalPrice = coder.decodeObject(forKey: "total_orignal_price") as! String
          let totalPrice = coder.decodeObject(forKey: "per_product_discount_price") as! String
          let cproductDis = coder.decodeObject(forKey: "discount_price") as! String
          let finalPrice = coder.decodeObject(forKey: "final_price") as! String
          let qty = coder.decodeObject(forKey: "qty") as! String
          let varientid = coder.decodeObject(forKey: "variant_id") as! String
          let varientname = coder.decodeObject(forKey: "variant_name") as! String
          let creturn = coder.decodeObject(forKey: "return") as! String
          self.init(["product_id":cid,"name":cname,"image":slug,"orignal_price":cprice,"total_orignal_price":corignalPrice,"per_product_discount_price":totalPrice,"discount_price":cproductDis,"final_price":finalPrice,"qty":qty,"variant_id":varientid,"variant_name":varientname,"return":creturn])
      }
    
    static var shareInstance = CartsList()
    
    override init() {
        super.init()
    }
    
    var productid: Int? = 0
    var image: String? = ""
    var name: String? = ""
    var orignalprice: String? = ""
    var totalproductOrignalPricde: String? = ""
    var perproDisPrice: String? = ""
    var discountPrice: String? = ""
    var finalprice: String? = ""
    var qty: Int? = 0
    var varientId: Int? = 0
    var varientName: String? = ""
    var retun: Int? = 0
    
    init(_ carts: [String:Any]) {
        productid = carts["product_id"] as? Int
        image = carts["image"] as? String
        name = carts["name"] as? String
        orignalprice = carts["orignal_price"] as? String
        totalproductOrignalPricde = carts["total_orignal_price"] as? String
        perproDisPrice = carts["per_product_discount_price"] as? String
        discountPrice = carts["discount_price"] as? String
        finalprice = carts["final_price"] as? String
        qty = carts["qty"] as? Int
        varientId = carts["variant_id"] as? Int
        varientName = carts["variant_name"] as? String
        retun = carts["return"] as? Int
    }
}
class TextData: NSObject{
    var textdatalists: [TextdataList] = []
    init(_ arrCart: [[String:Any]]){
        for i in 0..<arrCart.count{
            let dic = arrCart[i]
            textdatalists.append(.init(dic))
        }
    }
}
class TextdataList: NSObject{
    static var shareinstance = TextdataList()
    
    override init() {
        super.init()
    }
    
    var taxname: String? = ""
    var textype: String? = ""
    var taxString: String? = ""
    var taxPrice: String? = ""
    var taxAmmount: Int? = 0
    var taxid: Int? = 0
    var taxAmoountString: String? = ""
    
    init(_ arrTexts: [String:Any]){
        taxname = arrTexts["tax_name"] as? String
        textype = arrTexts["tax_type"] as? String
        taxString = arrTexts["tax_string"] as? String
        taxPrice = arrTexts["tax_price"] as? String
        taxAmmount = arrTexts["tax_amount"] as? Int
        taxid = arrTexts["id"] as? Int
        taxAmoountString = arrTexts["amountstring"] as? String
    }
}
class Address: NSObject{
    var addresslist: [AddressLists] = []
    init(_ arrlists: [[String:Any]]) {
        for i in 0..<arrlists.count{
            let dic = arrlists[i]
            addresslist.append(.init(dic))
        }
    }
}
class AddressLists: NSObject{
    static var shareinstance = AddressLists()
    
    override init() {
        super.init()
    }
    
    var id: Int? = 0
    var countryid: Int? = 0
    var stateid: Int? = 0
    var city: String? = ""
    var userid: Int? = 0
    var title: String? = ""
    var address: String? = ""
    var postcode: Int? = 0
    var defaultaddress: Int? = 0
    var countryname: String? = ""
    var cityname: String? = ""
    var statename: String? = ""
    var cityid: String? = ""
    
    init(_ arrCountry: [String:Any]) {
        id = arrCountry["id"] as? Int
        countryid = arrCountry["country_id"] as? Int
        stateid = arrCountry["state_id"] as? Int
        city = arrCountry["city"] as? String
        userid = arrCountry["user_id"] as? Int
        title = arrCountry["title"] as? String
        address = arrCountry["address"] as? String
        postcode = arrCountry["postcode"] as? Int
        defaultaddress = arrCountry["default_address"] as? Int
        countryname = arrCountry["country_name"] as? String
        cityname = arrCountry["state_name"] as? String
        statename = arrCountry["city_name"] as? String
        cityid = arrCountry["city_id"] as? String
    }
}
class Country: NSObject{
    var countrylists: [CountryLists] = []
    init(_ arrlits: [[String:Any]]){
        for i in 0..<arrlits.count{
            let dic = arrlits[i]
            countrylists.append(.init(dic))
        }
    }
}
class CountryLists: NSObject{
    static var shareinstance = CountryLists()
    
    override init() {
        super.init()
    }
    
    var id: Int? = 0
    var name: String? = ""
    
    init(_ arrCountry: [String:Any]) {
        id = arrCountry["id"] as? Int
        name = arrCountry["name"] as? String
    }
}
class State: NSObject{
    var statelists: [StateLists] = []
    init(_ arrlits: [[String:Any]]){
        for i in 0..<arrlits.count{
            let dic = arrlits[i]
            statelists.append(.init(dic))
        }
    }
}
class StateLists: NSObject{
    static var shareinstance = StateLists()
    
    override init() {
        super.init()
    }
    
    var name: String? = ""
    var id: Int? = 0
    var countryid: Int? = 0
    
    init(_ arrCountry: [String:Any]) {
        name = arrCountry["name"] as? String
        id = arrCountry["id"] as? Int
        countryid = arrCountry["country_id"] as? Int
    }
}
class City: NSObject{
    var citylists: [CityLists] = []
    init(_ arrlits: [[String:Any]]){
        for i in 0..<arrlits.count{
            let dic = arrlits[i]
            citylists.append(.init(dic))
        }
    }
}
class CityLists: NSObject{
    static var shareinstance = CityLists()
    
    override init() {
        super.init()
    }
    
    var name: String? = ""
    var id: Int? = 0
    var stateid: Int? = 0
    var countryid: Int? = 0
    
    init(_ arrCountry: [String:Any]) {
        name = arrCountry["name"] as? String
        id = arrCountry["id"] as? Int
        id = arrCountry["state_id"] as? Int
        countryid = arrCountry["country_id"] as? Int
    }
}
class Delivary: NSObject{
    var delivarylist: [DelivaryList] = []
    init(_ arrlists: [[String:Any]]) {
        for i in 0..<arrlists.count{
            let dic = arrlists[i]
            delivarylist.append(.init(dic))
        }
    }
}
class DelivaryList: NSObject{
    
    static var shareinstance = DelivaryList()
    
    override init() {
        super.init()
    }
    
    var id: Int? = 0
    var name: String? = ""
    var descript: String? = ""
    var imagepath: String? = ""
    var chage_type: String? = ""
    var retunorders: Int? = 0
    var amount: String? = ""
    var status: Int? = 0
    var theme_id: String? = ""
    var expected_delivary: String? = ""
    
    init(_ arrDelivary: [String:Any]){
        id = arrDelivary["id"] as? Int
        name = arrDelivary["name"] as? String
        descript = arrDelivary["description"] as? String
        imagepath = arrDelivary["image_path"] as? String
        chage_type = arrDelivary["charges_type"] as? String
        retunorders = arrDelivary["return_order_dutation"] as? Int
        amount = arrDelivary["amount"] as? String
        status = arrDelivary["status"] as? Int
        theme_id = arrDelivary["theme_id"] as? String
        expected_delivary = arrDelivary["expeted_delivery"] as? String
    }
}
class Payment: NSObject{
    var paymentlist: [PaymentList] = []
    init(_ arrlists: [[String:Any]]) {
        for i in 0..<arrlists.count{
            let dic = arrlists[i]
            paymentlist.append(.init(dic))
        }
    }
}
class PaymentList: NSObject{
    static var shareinstance = PaymentList()
    
    override init() {
        super.init()
    }
    var status: String? = ""
    var nameString: String? = ""
    var name: String? = ""
    var image: String? = ""
    var detail: String? = ""
    
    init(_ arrPayment: [String:Any]){
        status = arrPayment["status"] as? String
        nameString = arrPayment["name_string"] as? String
        name = arrPayment["name"] as? String
        image = arrPayment["image"] as? String
        detail = arrPayment["detail"] as? String
    }
}
class WishList: NSObject{
    var wishlists: [WishlistData] = []
    init(_ arrCart: [[String:Any]]){
        for i in 0..<arrCart.count{
            let dic = arrCart[i]
            wishlists.append(WishlistData .init(dic))
        }
    }
}
class WishlistData: NSObject{
    static var shareInstance = WishlistData()
    
    override init() {
        super.init()
    }
    
    var id: Int? = 0
    var userid: Int? = 0
    var productid: Int? = 0
    var varientid: Int? = 0
    var status: Int? = 0
    var themeid: String? = ""
    var createdAt: String? = ""
    var updatedAt: String? = ""
    var demofield: String? = ""
    var productname: String? = ""
    var productimage: String? = ""
    var varientname: String? = ""
    var orignalprice: String? = ""
    var finalprice: String? = ""
    
    init(_ lists: [String:Any]){
        id = lists["id"] as? Int
        userid = lists["user_id"] as? Int
        productid = lists["product_id"] as? Int
        varientid = lists["variant_id"] as? Int
        status = lists["status"] as? Int
        themeid = lists["theme_id"] as? String
        createdAt = lists["created_at"] as? String
        updatedAt = lists["updated_at"] as? String
        demofield = lists["demo_field"] as? String
        productname = lists["product_name"] as? String
        productimage = lists["product_image"] as? String
        varientname = lists["variant_name"] as? String
        orignalprice = lists["original_price"] as? String
        finalprice = lists["final_price"] as? String
    }
}
class Order: NSObject{
    var orderList: [OrderList] = []
    init(_ arrlists: [[String:Any]]) {
        for i in 0..<arrlists.count{
            let dic = arrlists[i]
            orderList.append(.init(dic))
        }
    }
}
class OrderList: NSObject{
    static var shareinstance = OrderList()
    
    override init() {
        super.init()
    }
    var id: Int? = 0
    var amount: Double? = 0.0
    var delivery_id: Int? = 0
    var date: String? = ""
    var reward_points: Float? = 0
    var theme_id: String? = ""
    var demo_field: String? = ""
    var delivered_status_string: String? = ""
    var delivered_image: String? = ""
    var order_id_string: String? = ""
    var return_date: String? = ""
    var delivery_date: String? = ""
    
    init(_ arrOrderList: [String:Any]){
        id = arrOrderList["id"] as? Int
        amount = arrOrderList["amount"] as? Double
        delivery_id = arrOrderList["delivery_id"] as? Int
        reward_points = arrOrderList["reward_points"] as? Float
        theme_id = arrOrderList["theme_id"] as? String
        demo_field = arrOrderList["demo_field"] as? String
        date = arrOrderList["date"] as? String
        delivered_status_string = arrOrderList["delivered_status_string"] as? String
        delivered_image = arrOrderList["delivered_image"] as? String
        order_id_string = arrOrderList["order_id_string"] as? String
        return_date = arrOrderList["return_date"] as? String
        delivery_date = arrOrderList["delivery_date"] as? String
    }
}
