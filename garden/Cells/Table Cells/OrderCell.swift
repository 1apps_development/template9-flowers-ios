//
//  OrderCell.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit

class OrderCell: UITableViewCell {
    
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var order_date: UILabel!
    @IBOutlet weak var order_id: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    var isReturn:Bool = false
    var isLoyalty: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        if isReturn{
            lblTitle.text = "Return:"
        }else{
            lblTitle.text = "Order:"
        }
        if isLoyalty{
            lblTitle.isHidden = true
            lblStatusTitle.text = "Friend:"
            lblStatus.text = "Jonathon Doe"
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
