//
//  InstractionCell.swift
//  plants
//
//  Created by mac on 01/12/22.
//

import UIKit
import SDWebImage

class InstractionCell: UITableViewCell {
    
    @IBOutlet weak var img_instract: UIImageView!
    @IBOutlet weak var instract: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ subproducts: ProductInstraction){
        let subURL = getImageFullURL("\(subproducts.img_path!)")
        img_instract.sd_setImage(with: URL(string: subURL)) { image, error, type, url in
            if image != nil{
                self.img_instract.image = image
            }else
            {
                self.img_instract.image = UIImage.init(named: "ic_instruction")
            }
        }
        instract.text = subproducts.discription
    }
}
