//
//  WishlistCell.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit

class WishlistCell: UITableViewCell {

    @IBOutlet weak var img_peoduct: UIImageView!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_varient: UILabel!
    @IBOutlet weak var lbl_productName: UILabel!
    
    var isWishlist:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(_ wishlists: WishlistData) {
        lbl_price.text = wishlists.finalprice
        let productimgURL = getImageFullURL("\(wishlists.productimage!)")
        img_peoduct.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.img_peoduct.image = image
        }
        lbl_varient.text = wishlists.varientname
        lbl_productName.text = wishlists.productname
        lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
