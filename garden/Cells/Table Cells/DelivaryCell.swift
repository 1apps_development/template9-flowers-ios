//
//  DelivaryCell.swift
//  plants
//
//  Created by mac on 05/12/22.
//

import UIKit

class DelivaryCell: UITableViewCell {

    @IBOutlet weak var img_delevary: UIImageView!
    @IBOutlet weak var viewCell: ShadowView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_aditionalPrice: UILabel!
    @IBOutlet weak var lbl_dillName: UILabel!
    @IBOutlet weak var lbl_dillDis: UILabel!
    @IBOutlet weak var img_selected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(_ delivary: DelivaryList) {
        if delivary.chage_type == "percentage"{
            lbl_price.text = "\(delivary.amount!) %"
        }else{
            lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(delivary.amount!)"
        }
        let imageURL = getImageFullURL("\(delivary.imagepath!)")
        img_delevary.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
            self.img_delevary.image = image
        }
        lbl_dillDis.text = delivary.descript
        lbl_dillName.text = delivary.name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
