//
//  AddressCell.swift
//  plants
//
//  Created by mac on 05/12/22.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var lbl_saveaddress: UILabel!
    @IBOutlet weak var img_selected: UIImageView!
    @IBOutlet weak var cellView: ShadowView!
    @IBOutlet weak var lbl_fulladdress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(_ address: AddressLists) {
        lbl_saveaddress.text = address.title
        lbl_fulladdress.text = "\(address.address!), \(address.cityname!), \(address.statename!), \(address.countryname!) -\(address.postcode!)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
