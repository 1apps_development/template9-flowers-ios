//
//  CategoriesCell.swift
//  plants
//
//  Created by mac on 02/05/22.
//

import UIKit
import SDWebImage

class CategoriesCell: UITableViewCell {
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var btnShowmore: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnShowmore.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnShowmore.layer.shadowColor = hexStringToUIColor(hex: "B5C547").cgColor
        btnShowmore.layer.shadowRadius = 8
        btnShowmore.layer.shadowOpacity = 0.3
        // Initialization code
    }
    
    func configureCell(_ category: categoryModel){
        lblCategory.text = category.name
        let endPoint = category.imagePath
        let imgUrl = getImageFullURL("\(endPoint!)")
        imgCategory.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage.init(named: "placeholder") )
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
