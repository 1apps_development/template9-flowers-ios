//
//  AddressBookCell.swift
//  plants
//
//  Created by mac on 06/12/22.
//

import UIKit

class AddressBookCell: UITableViewCell {

    @IBOutlet weak var lbl_addressTitle: UILabel!
    @IBOutlet weak var lbl_addressDis: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
