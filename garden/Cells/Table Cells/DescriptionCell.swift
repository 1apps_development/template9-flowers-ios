//
//  DescriptionCell.swift
//  plants
//
//  Created by mac on 29/11/22.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet weak var lbl_destitle: UILabel!
    @IBOutlet weak var lbl_desdiescrioption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
