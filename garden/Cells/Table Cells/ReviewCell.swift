//
//  ReviewCell.swift
//  plants
//
//  Created by mac on 29/11/22.
//

import UIKit

class ReviewCell: UICollectionViewCell {

    @IBOutlet weak var lbl_reviewText: UILabel!
    @IBOutlet weak var lbl_clientName: UILabel!
    @IBOutlet weak var lbl_rate: UILabel!
    @IBOutlet weak var rateing: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
