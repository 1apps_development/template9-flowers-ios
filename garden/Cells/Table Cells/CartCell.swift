//
//  CartCell.swift
//  kiddos
//
//  Created by mac on 18/04/22.
//

import UIKit
import SDWebImage

class CartCell: UITableViewCell {

    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_proPrice: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_proName: UILabel!
    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var qtyProView: UIView!
    @IBOutlet weak var btn_minus: UIButton!
    @IBOutlet weak var btn_pluse: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(_ products: CartsList){
        lbl_proName.text = products.name
        let productimgURL = "\(products.image!)"
        img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.img_product.image = image
        }
        let price = Double(products.finalprice!)! * Double(products.qty!)
        lbl_price.text = products.varientName
        lbl_proPrice.text = "\(price)"
        lbl_qty.text = "\(products.qty!)"
        lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
