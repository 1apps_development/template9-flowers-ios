//
//  MenuTVCell.swift
//  kiddos
//
//  Created by mac on 14/04/22.
//

import UIKit

class MenuTVCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(_ menu: MenuModel){
        lblTitle.text = menu.name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
