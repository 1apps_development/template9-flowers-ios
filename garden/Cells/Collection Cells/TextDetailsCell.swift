//
//  TextDetailsCell.swift
//  plants
//
//  Created by mac on 05/12/22.
//

import UIKit

class TextDetailsCell: UICollectionViewCell {

    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_textTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ textdetails: TextdataList){
        lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(textdetails.taxPrice!)"
        lbl_textTitle.text = textdetails.taxString
    }
}
