//
//  categoryCVCell.swift
//  jewellery
//
//  Created by mac on 04/11/22.
//

import UIKit
import SDWebImage

class categoryCVCell: UICollectionViewCell {
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var isSelected: Bool {
        didSet {
            self.viewOuter.backgroundColor = self.isSelected ? hexStringToUIColor(hex: "#5C8973") : .clear
            self.lblCategoryName.textColor = self.isSelected ? hexStringToUIColor(hex: "#010F09") : .white
        }
    }
    
    func configureCell(_ category: categoryModel){
//        viewOuter.backgroundColor = category.
        let endpoint = category.iconPath
        let iconUrl = getImageFullURL("\(endpoint!)")
        imgCategory.sd_setImage(with: URL(string: iconUrl), placeholderImage: UIImage(named: "placeholder"))
        lblCategoryName.text = category.name
            
    }
    

}
