//
//  SubtaskCell.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class SubtaskCell: UICollectionViewCell {
    @IBOutlet weak var lblSubtask: UILabel!
    @IBOutlet weak var viewOuter: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(_ title: String,isSelected: Bool){
        lblSubtask.text = title
        if isSelected{
            self.lblSubtask.textColor = hexStringToUIColor(hex: "00160C")
            self.viewOuter.backgroundColor = hexStringToUIColor(hex: "E6E6E6")
            self.viewOuter.layer.borderColor = UIColor.clear.cgColor
            
        }else{
            self.lblSubtask.textColor = hexStringToUIColor(hex: "E6E6E6")
            self.viewOuter.backgroundColor = .clear
            self.viewOuter.layer.borderColor = hexStringToUIColor(hex: "E6E6E6").cgColor

        }
    }

}
