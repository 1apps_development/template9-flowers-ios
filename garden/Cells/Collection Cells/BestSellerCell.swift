//
//  BestSellerCell.swift
//  plants
//
//  Created by mac on 28/11/22.
//

import UIKit
import SDWebImage

class BestSellerCell: UICollectionViewCell {

    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var lbl_addTowishList: UILabel!
    @IBOutlet weak var btn_wishlist: UIButton!
    @IBOutlet weak var lbl_height: UILabel!
    @IBOutlet weak var lbl_productName: UILabel!
    @IBOutlet weak var varient: UILabel!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var btn_addtocart: UIButton!
    
    var onclickAddCartClosure: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func congifureCell(_ Producs: Products) {
        let endpoint = Producs.coverimagepath
        let imgUrl = getImageFullURL("\(endpoint!)")
        img_product.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage.init(named: "placeholder"))
        lbl_productName.text = Producs.name
        lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY_NAME)
        lbl_price.text = Producs.finalPrice
     }
    
    @IBAction func onclickAddTocart(_ sender: UIButton) {
        self.onclickAddCartClosure?()
    }
}
