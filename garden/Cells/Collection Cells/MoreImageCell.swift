//
//  MoreImageCell.swift
//  plants
//
//  Created by mac on 29/11/22.
//

import UIKit
import SDWebImage

class MoreImageCell: UICollectionViewCell {
    
    @IBOutlet weak var img_product: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ subproducts: ProductImages){
        let subURL = getImageFullURL("\(subproducts.imagePath!)")
        img_product.sd_setImage(with: URL(string: subURL)) { image, error, type, url in
            self.img_product.image = image
        }
    }

}
