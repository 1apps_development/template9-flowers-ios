//
//  PlantCell.swift
//  kiddos
//
//  Created by mac on 13/04/22.
//

import UIKit
import SDWebImage

class PlantCell: UICollectionViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnAddtocart: UIButton!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var btnFav: UIButton!

    var onclickAddCartClosure: (()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ produc :Products){
        lblProductName.text = produc.name
        let endpoint = produc.coverimagepath
        let imgUrl = getImageFullURL("\(endpoint!)")
        imgProduct.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage.init(named: "placeholder"))
        lblCurrency.text = getCurrencyName()
        lblProductPrice.text = produc.finalPrice
    }

    @IBAction func onclickAddToCart(_ sender: UIButton) {
        self.onclickAddCartClosure?()
    }
    
}
